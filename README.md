# Foundry VTT application fork
This project is a fork of the original foundry VTT nodejs applucation. but it has been mosified to woek like a serverless application. 

## Architecture
The application is made up of 2 parts, one part is the nodejs application, and the other is the app world data. <br>
My objective is to run the application on a serverless platform, and allow each instance to communicate with a generic cloud storage location. This should distribute the computation and enable better performance and scalability for the Foundry VTT Platform.

### Process
1. Deploy app to serverless platform
2. Connect to Private Cloud Storage
