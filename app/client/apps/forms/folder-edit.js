/**
 * The Application responsible for configuring a single Folder document.
 * @extends {DocumentSheet}
 * @param {Folder} object                   The {@link Folder} object to configure.
 * @param {DocumentSheetOptions} [options]  Application configuration options.
 */
class FolderConfig extends DocumentSheet {

  /** @inheritdoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ["sheet", "folder-edit"],
      template: "templates/sidebar/folder-edit.html",
      width: 360
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get id() {
    return this.object.id ? `folder-edit-${this.object.id}` : "folder-create";
  }

  /* -------------------------------------------- */

  /** @override */
  get title() {
    if ( this.object.id ) return `${game.i18n.localize("FOLDER.Update")}: ${this.object.name}`;
    return game.i18n.localize("FOLDER.Create");
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async close(options={}) {
    if ( !this.options.submitOnClose ) this.options.resolve?.(null);
    return super.close(options);
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    return {
      name: this.object.id ? this.object.name : "",
      newName: game.i18n.format("DOCUMENT.New", {type: game.i18n.localize(Folder.metadata.label)}),
      folder: this.object.data,
      safeColor: this.object.data.color ?? "#000000",
      sortingModes: {"a": "FOLDER.SortAlphabetical", "m": "FOLDER.SortManual"},
      submitText: game.i18n.localize(this.object.id ? "FOLDER.Update" : "FOLDER.Create")
    }
  }

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    if ( !formData.parent ) formData.parent = null;
    let doc = this.object;
    if ( this.object.id ) await this.object.update(formData);
    else {
      this.object.data.update(formData);
      doc = await Folder.create(this.object.data);
    }
    this.options.resolve?.(doc);
    return doc;
  }
}
