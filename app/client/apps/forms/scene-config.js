/**
 * The Application responsible for configuring a single Scene document.
 * @extends {DocumentSheet}
 * @param {Scene} object                    The Scene Document which is being configured
 * @param {DocumentSheetOptions} [options]  Application configuration options.
 */
class SceneConfig extends DocumentSheet {

  /** @inheritdoc */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "scene-config",
      classes: ["sheet", "scene-sheet"],
      template: "templates/scene/config.html",
      width: 560,
      height: "auto",
      tabs: [{navSelector: ".tabs", contentSelector: "form", initial: "basic"}]
    });
  }

	/* -------------------------------------------- */

  /** @override */
  get title() {
    return `${game.i18n.localize("SCENES.ConfigTitle")}: ${this.object.name}`;
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  async close(options={}) {
    this._resetScenePreview();
    return super.close(options);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  render(force, options={}) {
    if ( options.renderContext && (options.renderContext !== "updateScene" ) ) return this;
    return super.render(force, options);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    const data = super.getData(options);

    // Selectable types
    data.gridTypes = this.constructor._getGridTypes();
    data.weatherTypes = this._getWeatherTypes();

    // Referenced documents
    data.playlists = this._getDocuments(game.playlists);
    data.sounds = this._getDocuments(this.object.playlist?.sounds ?? []);
    data.journals = this._getDocuments(game.journal);

    // Global illumination threshold
    data.hasGlobalThreshold = data.data.globalLightThreshold !== null;
    data.data.globalLightThreshold = data.data.globalLightThreshold ?? 0;
    return data;
  }

	/* -------------------------------------------- */

  /**
   * Get an enumeration of the available grid types which can be applied to this Scene
   * @return {Object}
   * @private
   */
	static _getGridTypes() {
	  const labels = {
	    "GRIDLESS": "SCENES.GridGridless",
      "SQUARE": "SCENES.GridSquare",
      "HEXODDR": "SCENES.GridHexOddR",
      "HEXEVENR": "SCENES.GridHexEvenR",
      "HEXODDQ": "SCENES.GridHexOddQ",
      "HEXEVENQ": "SCENES.GridHexEvenQ"
    };
    return Object.keys(CONST.GRID_TYPES).reduce((obj, t) => {
	    obj[CONST.GRID_TYPES[t]] = labels[t];
	    return obj;
    }, {});
  }

	/* -------------------------------------------- */

  /**
   * Get the available weather effect types which can be applied to this Scene
   * @return {Object}
   * @private
   */
  _getWeatherTypes() {
    const types = {};
    for ( let [k, v] of Object.entries(CONFIG.weatherEffects) ) {
      types[k] = v.label;
    }
    return types;
  }

	/* -------------------------------------------- */

  /**
   * Get the alphabetized Documents which can be chosen as a configuration for the Scene
   * @param {WorldCollection} collection
   * @return {object[]}
   * @private
   */
  _getDocuments(collection) {
    const documents = collection.map(doc => {
      return {id: doc.id, name: doc.name};
    });
    documents.sort((a, b) => a.name.localeCompare(b.name));
    return documents;
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    super.activateListeners(html);
    html.find("button.capture-position").click(this._onCapturePosition.bind(this));
    html.find("button.grid-config").click(this._onGridConfig.bind(this));
    html.find("select[name='playlist']").change(this._onChangePlaylist.bind(this));
  }

	/* -------------------------------------------- */

  /**
   * Capture the current Scene position and zoom level as the initial view in the Scene config
   * @param {Event} event   The originating click event
   * @private
   */
  _onCapturePosition(event) {
    event.preventDefault();
    if ( !canvas.ready ) return;
    const btn = event.currentTarget;
    const form = btn.form;
    form["initial.x"].value = parseInt(canvas.stage.pivot.x);
    form["initial.y"].value = parseInt(canvas.stage.pivot.y);
    form["initial.scale"].value = canvas.stage.scale.x;
    ui.notifications.info("Captured canvas position as initial view in the Scene configuration form.")
  }

  /* -------------------------------------------- */

  /** @override */
  async _onChangeInput(event) {
    this._previewScene(event.target.name);
    return super._onChangeInput(event);
  }

  /* -------------------------------------------- */

  /** @override */
  _onChangeColorPicker(event) {
    super._onChangeColorPicker(event);
    this._previewScene(event.target.dataset.edit);
  }

  /* -------------------------------------------- */

  /** @override */
  _onChangeRange(event) {
    super._onChangeRange(event);
    this._previewScene(event.target.name);
  }

  /* -------------------------------------------- */

  /**
   * Live update the scene as certain properties are changed.
   * @param {string} changed  The changed property.
   * @private
   */
  _previewScene(changed) {
    if ( !this.object.isView || !canvas.ready ) return;
    if ( ["gridColor", "gridAlpha"].includes(changed) ) canvas.grid.grid.draw({
      gridColor: this.form.gridColor.value.replace("#", "0x"),
      gridAlpha: Number(this.form.gridAlpha.value)
    });
    if ( ["darkness", "backgroundColor"].includes(changed) ) canvas.lighting.refresh({
      backgroundColor: this.form.backgroundColor.value,
      darkness: Number(this.form.darkness.value)
    });
  }

  /* -------------------------------------------- */

  /**
   * Reset the previewed darkness level, background color, grid alpha, and grid color back to their true values.
   * @private
   */
  _resetScenePreview() {
    if ( !this.object.isView || !canvas.ready ) return;
    const sceneData = canvas.scene.data;
    let gridChanged = this.form.gridColor.value !== sceneData.gridColor;
    gridChanged ||= this.form.gridAlpha.value !== sceneData.gridAlpha;
    canvas.lighting.refresh(this.object.data);
    if ( gridChanged ) canvas.grid.grid.draw();
  }

	/* -------------------------------------------- */

  /**
   * Handle updating the select menu of PlaylistSound options when the Playlist is changed
   * @param {Event} event   The initiating select change event
   * @private
   */
  _onChangePlaylist(event) {
    event.preventDefault();
    const playlist = game.playlists.get(event.target.value);
    const sounds = this._getDocuments(playlist?.sounds || []);
    const options = ['<option value=""></option>'].concat(sounds.map(s => {
      return `<option value="${s.id}">${s.name}</option>`;
    }));
    const select = this.form.querySelector(`select[name="playlistSound"]`);
    select.innerHTML = options.join("");
  }

	/* -------------------------------------------- */

  /**
   * Handle click events to open the grid configuration application
   * @param {Event} event   The originating click event
   * @private
   */
  async _onGridConfig(event) {
    event.preventDefault();
    if ( !this.object.isView ) await this.object.view();
    new GridConfig(this.object, this).render(true);
    return this.minimize();
  }

	/* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    const scene = this.document;

    // Capture initial camera position
    const initialViewAttrs = ["initial.x", "initial.y", "initial.scale"];
    if ( initialViewAttrs.every(a => !formData[a]) ) {
      for ( let a of initialViewAttrs ) {
        delete formData[a];
      }
      formData.initial = null;
    }

    // Toggle global illumination threshold
    if ( formData.hasGlobalThreshold === false ) formData.globalLightThreshold = null;
    delete formData.hasGlobalThreshold;
    // SceneData.img is nullable in the schema, causing an empty string to be initialised to null. We need to match that
    // logic here to ensure that comparisons to the existing scene image are accurate.
    if ( formData.img === "" ) formData.img = null;
    if ( formData.foreground === "" ) formData.foreground = null;

	  // Determine what type of change has occurred
    const hasDefaultDims = (scene.data.width === 4000 ) && ( scene.data.height === 3000 );
    const hasImage = formData.img || scene.data.img;
    const changedBackground = (formData.img !== undefined) && (formData.img !== scene.data.img);
    const clearedDims = (formData.width === null) || (formData.height === null);
    const needsThumb = changedBackground || !scene.data.thumb;
    const needsDims = formData.img && (clearedDims || hasDefaultDims);
    const createThumbnail = hasImage && (needsThumb || needsDims);

    // Update thumbnail and image dimensions
    if ( createThumbnail && game.settings.get("core", "noCanvas") ) {
      ui.notifications.warn("SCENES.GenerateThumbNoCanvas", {localize: true});
      formData.thumb = null;
    } else if ( createThumbnail ) {
      let td = {};
      try {
        td = await scene.createThumbnail({img: formData.img ?? scene.data.img});
      } catch(err) {
        Hooks.onError("SceneConfig#_updateObject", err, {
          msg: "Thumbnail generation for Scene failed",
          notify: "error",
          log: "error",
          scene: scene.id
        });
      }
      if ( needsThumb ) formData.thumb = td.thumb || null;
      if ( needsDims ) {
        formData.width = td.width;
        formData.height = td.height;
      }
    }

    // Warn the user if Scene dimensions are changing
    const delta = foundry.utils.diffObject(scene.data, formData);
    if ( ["width", "height", "padding", "shiftX", "shiftY", "size"].some(k => k in delta) ) {
      const confirm = await Dialog.confirm({
        title: game.i18n.localize("SCENES.DimensionChangeTitle"),
        content: `<p>${game.i18n.localize("SCENES.DimensionChangeWarning")}</p>`
      });
      if ( !confirm ) return;
    }

    // Perform the update
    return scene.update(formData);
  }
}
