/**
 * The global action bar displayed at the bottom of the game view.
 * The Hotbar is a UI element at the bottom of the screen which contains Macros as interactive buttons.
 * The Hotbar supports 5 pages of global macros which can be dragged and dropped to organize as you wish.
 *
 * Left clicking a Macro button triggers its effect.
 * Right clicking the button displays a context menu of Macro options.
 * The number keys 1 through 0 activate numbered hotbar slots.
 * Pressing the delete key while hovering over a Macro will remove it from the bar.
 *
 * @extends {Application}
 *
 * @see {@link Macros}
 * @see {@link Macro}
 */
class Hotbar extends Application {
	constructor(options) {
	  super(options);
	  game.macros.apps.push(this);

    /**
     * The currently viewed macro page
     * @type {number}
     */
    this.page = 1;

    /**
     * The currently displayed set of macros
     * @type {Macro[]}
     */
    this.macros = [];

    /**
     * Track collapsed state
     * @type {boolean}
     */
    this._collapsed = false;

    /**
     * Track which hotbar slot is the current hover target, if any
     * @type {number|null}
     */
    this._hover = null;
  }

  /* -------------------------------------------- */

  /** @override */
	static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: "hotbar",
      template: "templates/hud/hotbar.html",
      popOut: false,
      dragDrop: [{ dragSelector: ".macro-icon", dropSelector: "#macro-list" }]
    });
  }

	/* -------------------------------------------- */

  /** @override */
	getData(options) {
	  this.macros = this._getMacrosByPage(this.page);
    return {
      page: this.page,
      macros: this.macros,
      barClass: this._collapsed ? "collapsed" : ""
    };
  }

	/* -------------------------------------------- */

  /**
   * Get the Array of Macro (or null) values that should be displayed on a numbered page of the bar
   * @param {number} page
   * @returns {Macro[]}
   * @private
   */
  _getMacrosByPage(page) {
    const macros = game.user.getHotbarMacros(page);
    for ( let [i, m] of macros.entries() ) {
      m.key = i<9 ? i+1 : 0;
      m.cssClass = m.macro ? "active" : "inactive";
      m.icon = m.macro ? m.macro.data.img : null;
    }
    return macros;
  }

	/* -------------------------------------------- */

  /**
   * Collapse the Hotbar, minimizing its display.
   * @return {Promise}    A promise which resolves once the collapse animation completes
   */
  async collapse() {
    if ( this._collapsed ) return true;
    const toggle = this.element.find("#bar-toggle");
    const icon = toggle.children("i");
    const bar = this.element.find("#action-bar");
    return new Promise(resolve => {
      bar.slideUp(200, () => {
        bar.addClass("collapsed");
        icon.removeClass("fa-caret-down").addClass("fa-caret-up");
        this._collapsed = true;
        resolve(true);
      });
    });
  }

	/* -------------------------------------------- */

  /**
   * Expand the Hotbar, displaying it normally.
   * @return {Promise}    A promise which resolves once the expand animation completes
   */
  async expand() {
    if ( !this._collapsed ) return true;
    const toggle = this.element.find("#bar-toggle");
    const icon = toggle.children("i");
    const bar = this.element.find("#action-bar");
    return new Promise(resolve => {
      bar.slideDown(200, () => {
        bar.css("display", "");
        bar.removeClass("collapsed");
        icon.removeClass("fa-caret-up").addClass("fa-caret-down");
        this._collapsed = false;
        resolve(true);
      });
    });
  }

	/* -------------------------------------------- */

  /**
   * Change to a specific numbered page from 1 to 5
   * @param {number} page     The page number to change to.
   */
  changePage(page) {
    this.page = Math.clamped(page ?? 1, 1, 5);
    this.render();
  }

	/* -------------------------------------------- */

  /**
   * Change the page of the hotbar by cycling up (positive) or down (negative)
   * @param {number} direction    The direction to cycle
   */
  cyclePage(direction) {
    direction = Number.isNumeric(direction) ? Math.sign(direction) : 1;
    if ( direction > 0 ) {
      this.page = this.page < 5 ? this.page+1 : 1;
    } else {
      this.page = this.page > 1 ? this.page-1 : 5;
    }
    this.render();
  }

	/* -------------------------------------------- */
  /*  Event Listeners and Handlers
	/* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Macro actions
    html.find('#bar-toggle').click(this._onToggleBar.bind(this));
    html.find("#macro-directory").click(ev => ui.macros.renderPopout(true));
    html.find(".macro").click(this._onClickMacro.bind(this)).hover(this._onHoverMacro.bind(this));
    html.find(".page-control").click(this._onClickPageControl.bind(this));

    // Activate context menu
    this._contextMenu(html);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _contextMenu(html) {
    ContextMenu.create(this, html, ".macro", this._getEntryContextOptions());
  }

  /* -------------------------------------------- */

  /**
   * Get the Macro entry context options
   * @returns {object[]}  The Macro entry context options
   * @private
   */
  _getEntryContextOptions() {
    return [
      {
        name: "MACRO.Edit",
        icon: '<i class="fas fa-edit"></i>',
        condition: li => {
          const macro = game.macros.get(li.data("macro-id"));
          return macro ? macro.isOwner : false;
        },
        callback: li => {
          const macro = game.macros.get(li.data("macro-id"));
          macro.sheet.render(true);
        }
      },
      {
        name: "MACRO.Remove",
        icon: '<i class="fas fa-times"></i>',
        condition: li => !!li.data("macro-id"),
        callback: li => game.user.assignHotbarMacro(null, Number(li.data("slot")))
      },
      {
        name: "MACRO.Delete",
        icon: '<i class="fas fa-trash"></i>',
        condition: li => {
          const macro = game.macros.get(li.data("macro-id"));
          return macro ? macro.isOwner : false;
        },
        callback: li => {
          const macro = game.macros.get(li.data("macro-id"));
          return Dialog.confirm({
            title: `${game.i18n.localize("MACRO.Delete")} ${macro.name}`,
            content: `<h4>${game.i18n.localize("AreYouSure")}</h4><p>${game.i18n.localize("MACRO.DeleteWarning")}</p>`,
            yes: macro.delete.bind(macro)
          });
        }
      },
    ];
  }

  /* -------------------------------------------- */

  /**
   * Handle left-click events to
   * @param {MouseEvent} event    The originating click event
   * @protected
   */
  async _onClickMacro(event) {
    event.preventDefault();
    const li = event.currentTarget;

    // Case 1 - create a new Macro
    if ( li.classList.contains("inactive") ) {
      const macro = await Macro.create({name: "New Macro", type: "chat", scope: "global"});
      await game.user.assignHotbarMacro(macro, Number(li.dataset.slot));
      macro.sheet.render(true);
    }

    // Case 2 - trigger a Macro
    else {
      const macro = game.macros.get(li.dataset.macroId);
      return macro.execute();
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle hover events on a macro button to track which slot is the hover target
   * @param {Event} event   The originating mouseover or mouseleave event
   * @private
   */
  _onHoverMacro(event) {
    event.preventDefault();
    const li = event.currentTarget;
    const hasAction = !li.classList.contains("inactive");

    // Remove any existing tooltip
    const tooltip = li.querySelector(".tooltip");
    if ( tooltip ) li.removeChild(tooltip);

    // Handle hover-in
    if ( event.type === "mouseenter" ) {
      this._hover = li.dataset.slot;
      if ( hasAction ) {
        const macro = game.macros.get(li.dataset.macroId);
        const tooltip = document.createElement("SPAN");
        tooltip.classList.add("tooltip");
        tooltip.textContent = macro.name;
        li.appendChild(tooltip);
      }
    }

    // Handle hover-out
    else {
      this._hover = null;
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle pagination controls
   * @param {Event} event   The originating click event
   * @private
   */
  _onClickPageControl(event) {
    this.cyclePage(event.currentTarget.dataset.action === "page-up" ? 1 : -1);
  }

  /* -------------------------------------------- */

  /** @override */
  _canDragStart(selector) {
    return true;
  }

  /* -------------------------------------------- */

  /** @override */
  _onDragStart(event) {
    const li = event.currentTarget.closest(".macro");
    if ( !li.dataset.macroId ) return false;
    const dragData = { type: "Macro", id: li.dataset.macroId, slot: li.dataset.slot };
    event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  /* -------------------------------------------- */

  /** @override */
  _canDragDrop(selector) {
    return true;
  }

  /* -------------------------------------------- */

  /** @override */
  async _onDrop(event) {
    event.preventDefault();
    const li = event.target.closest(".macro");
    const slot = Number(li.dataset.slot);
    const data = TextEditor.getDragEventData(event);

    /**
     * A hook event that fires whenever data is dropped into a Hotbar slot.
     * The hook provides a reference to the Hotbar application, the dropped data, and the target slot.
     * Default handling of the drop event can be prevented by returning false within the hooked function.
     *
     * @function hotbarDrop
     * @memberof hookEvents
     * @param {Hotbar} hotbar       The Hotbar application instance
     * @param {object} data         The dropped data object
     * @param {number} slot         The target hotbar slot
     */
    if ( Hooks.call("hotbarDrop", this, data, slot) === false ) return;

    // Only handle Macro drops
    if ( data.type !== "Macro" ) return;
    let macro = await Macro.fromDropData(data);
    if ( !macro ) return;
    if ( !game.macros.has(macro.id) ) {
      macro = await Macro.create(macro.toJSON());
    }
    return game.user.assignHotbarMacro(macro, slot, {fromSlot: data.slot});
  }

  /* -------------------------------------------- */

  /**
   * Handle click events to toggle display of the macro bar
   * @param {Event} event
   * @private
   */
  _onToggleBar(event) {
    event.preventDefault();
    if ( this._collapsed ) return this.expand();
    else return this.collapse();
  }
}
