/**
 * The Application responsible for configuring a single Note document within a parent Scene.
 * @extends {DocumentSheet}
 * @param {NoteDocument} note               The Note object for which settings are being configured
 * @param {DocumentSheetOptions} [options]  Additional Application configuration options
 */
class NoteConfig extends DocumentSheet {

  /** @override */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
      title: game.i18n.localize("NOTE.ConfigTitle"),
      template: "templates/scene/note-config.html",
      width: 400
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    const data = super.getData(options);
    return foundry.utils.mergeObject(data, {
      entry: game.journal.get(this.object.data.entryId) || {},
      entries: game.journal.contents.sort((a, b) => a.data.name.localeCompare(b.data.name)),
      icons: CONFIG.JournalEntry.noteIcons,
      fontFamilies: CONFIG.fontFamilies.reduce((obj, f) => {
        obj[f] = f;
        return obj;
      }, {}),
      textAnchors: Object.entries(CONST.TEXT_ANCHOR_POINTS).reduce((obj, e) => {
        obj[e[1]] = game.i18n.localize(`JOURNAL.Anchor${e[0].titleCase()}`);
        return obj;
      }, {}),
      submitText: game.i18n.localize(this.id ? "NOTE.Update" : "NOTE.Create")
    });
  }

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {
    if ( this.object.id ) return this.object.update(formData);
    else return this.object.constructor.create(formData, {parent: canvas.scene});
  }

  /* -------------------------------------------- */

  /** @override */
  async close(options) {
    if ( !this.object.id ) canvas.notes.preview.removeChildren();
    return super.close(options);
  }
}
