/**
 * Keyboard Controls Reference Sheet
 * @type {Application}
 * @deprecated since v9
 * @ignore
 */
class ControlsReference extends Application {
  /** @inheritdoc */
  static get defaultOptions() {
    const options = super.defaultOptions;
    options.title = game.i18n.localize("CONTROLS.Title");
    options.id = "controls-reference";
    options.template = "templates/sidebar/apps/controls-reference.html";
    options.width = 600;
    return options;
  }

  /* -------------------------------------------- */
  
  /** @inheritDoc */
  getData(options = {}) {
    let data = super.getData(options);
    data.controlKey = KeyboardManager.CONTROL_KEY_STRING;
    return data;
  }
}
