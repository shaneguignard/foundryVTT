/**
 * Allows for viewing and editing of Keybinding Actions
 * @extends {FormApplication}
 */
class KeybindingsConfig extends FormApplication {

  /**
   * A cached copy of the Categories
   * @type {{categories: object[], totalActions: number} | null}
   * @private
   */
  _cachedData = null;

  /**
   * The category being filtered for
   * @type {string}
   */
  _category = "all";

  /**
   * A Map of pending Edits. The Keys are bindingIds
   * @type {Map<string, KeybindingActionBinding[]>}
   * @private
   */
  _pendingEdits = new Map();

  /* -------------------------------------------- */

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      title: game.i18n.localize("SETTINGS.Keybindings"),
      id: "keybindings",
      template: "templates/sidebar/apps/keybindings-config.html",
      width: 750,
      height: 600,
      resizable: true,
      scrollY: [".filters", ".category-list"],
      filters: [{inputSelector: 'input[name="filter"]', contentSelector: ".category-list"}]
    })
  }

  /* -------------------------------------------- */

  /** @override */
  getData(options) {
    let data = this._getCategoryData();

    // Filter and set active
    for ( let category of data.categories ) {
      category.active = category.id === this._category;
      category.hidden = this._category !== "all" && this._category !== category.id;
    }

    // If none of the categories are active, then "All" is active
    data.allActive = !data.categories.some(x => x.active);

    // Return data
    return data;
  }
  /* -------------------------------------------- */

  /**
   * Builds the set of Bindings into a form usable for display and configuration
   * @return {Object}
   * @private
   */
  _getCategoryData() {

    // Return from Cache if possible
    if ( this._cachedData ) return this._cachedData;

    // Classify all Actions
    let categories = new Map();
    let totalActions = 0;
    for ( let [ actionId, action ] of game.keybindings.actions ) {
      if ( action.restricted && !game.user.isGM ) continue;
      totalActions++;

      // Determine what category the action belongs to
      let category = this._categorizeAction(action);

      // Carry over bindings for future rendering
      const actionData = foundry.utils.deepClone(action);
      actionData.category = category.title;
      actionData.id = actionId;
      actionData.name = game.i18n.localize(action.name);
      actionData.hint = game.i18n.localize(action.hint);
      actionData.cssClass = action.restricted ? "gm" : "";
      actionData.notes = [
        action.restricted ? game.i18n.localize("KEYBINDINGS.Restricted") : "",
        action.reservedModifiers.length > 0 ? game.i18n.format("KEYBINDINGS.ReservedModifiers",
          { modifiers: action.reservedModifiers.map(m => m.titleCase()).join(", ") }) : "",
        game.i18n.localize(action.hint)
      ].filterJoin("<br>");
      actionData.uneditable = action.uneditable;

      // Prepare binding-level data
      actionData.bindings = (game.keybindings.bindings.get(actionId) ?? []).map((b, i) => {
        const uneditable = action.uneditable.includes(b);
        const binding = foundry.utils.deepClone(b);
        binding.id = `${actionId}.binding.${i}`;
        binding.display = KeybindingsConfig._humanizeBinding(binding);
        binding.cssClasses = uneditable ? "uneditable" : "";
        binding.isEditable = !uneditable;
        binding.isFirst = i === 0;
        const conflicts = this._detectConflictingActions(actionId, action, binding);
        binding.conflicts = game.i18n.format("KEYBINDINGS.Conflict", {
          conflicts: conflicts.map((action) => game.i18n.localize(action.name)).join(", ")
        });
        binding.hasConflicts = conflicts.length > 0;
        return binding;
      });
      actionData.noBindings = actionData.bindings.length === 0;

      // Register a category the first time it is seen, otherwise add to it
      if ( !categories.has(category.id) ) {
        categories.set(category.id, {
          id: category.id,
          title: category.title,
          actions: [ actionData ],
          count: 0
        });

      } else categories.get(category.id).actions.push(actionData);
    }

    // Add Mouse Controls
    totalActions += this._addMouseControlsReference(categories);

    // Sort Actions by priority and assign Counts
    for ( let category of categories.values() ) {
      category.actions = category.actions.sort(ClientKeybindings._compareActions);
      category.count = category.actions.length;
    }

    // Sort by name
    categories = Array.from(categories.values()).sort(KeybindingsConfig._sortCategories);

    // Update cache and return data
    this._cachedData = {
      categories: categories,
      totalActions: totalActions
    };
    return this._cachedData;
  }

  /* -------------------------------------------- */

  /**
   * Add faux-keybind actions that represent the possible Mouse Controls
   * @param {Map} categories    The current Map of Categories to add to
   * @return {Number}           The number of Actions added
   * @private
   */
  _addMouseControlsReference(categories) {
    let coreMouseCategory = game.i18n.localize("KEYBINDINGS.CoreMouse");

    const defineMouseAction = (id, name, keys, gmOnly=false) => {
      return {
        category: coreMouseCategory,
        id: id,
        name: game.i18n.localize(name),
        notes: gmOnly ? game.i18n.localize("KEYBINDINGS.Restricted") : "",
        bindings: [
          {
            display: keys.map(k => game.i18n.localize(k)).join(" + "),
            cssClasses: "uneditable",
            isEditable: false,
            hasConflicts: false,
            isFirst: false
          }
        ]
      }
    };

    const actions = [
      ["canvas-select", "CONTROLS.CanvasSelect", [ "CONTROLS.LeftClick" ]],
      ["canvas-select-many", "CONTROLS.CanvasSelectMany", [ "Shift", "CONTROLS.LeftClick" ]],
      ["canvas-drag", "CONTROLS.CanvasLeftDrag", [ "CONTROLS.LeftClick", "CONTROLS.Drag" ]],
      ["canvas-select-cancel", "CONTROLS.CanvasSelectCancel", [ "CONTROLS.RightClick" ]],
      ["canvas-pan-mouse", "CONTROLS.CanvasPan", [ "CONTROLS.RightClick", "CONTROLS.Drag" ]],
      ["canvas-zoom", "CONTROLS.CanvasSelectCancel", [ "CONTROLS.MouseWheel" ]],
      ["ruler-measure", "CONTROLS.RulerMeasure", [ KeyboardManager.CONTROL_KEY_STRING, "CONTROLS.LeftDrag" ]],
      ["ruler-measure-waypoint", "CONTROLS.RulerWaypoint", [ KeyboardManager.CONTROL_KEY_STRING, "CONTROLS.LeftClick" ]],
      ["object-sheet", "CONTROLS.ObjectSheet", [ `${game.i18n.localize("CONTROLS.Double")} ${game.i18n.localize("CONTROLS.LeftClick")}` ]],
      ["object-hud", "CONTROLS.ObjectHUD", [ "CONTROLS.RightClick" ]],
      ["object-config", "CONTROLS.ObjectConfig", [ `${game.i18n.localize("CONTROLS.Double")} ${game.i18n.localize("CONTROLS.RightClick")}` ]],
      ["object-drag", "CONTROLS.ObjectDrag", [ "CONTROLS.LeftClick", "CONTROLS.Drag" ]],
      ["object-no-snap", "CONTROLS.ObjectNoSnap", [ "CONTROLS.Drag", "Shift", "CONTROLS.Drop" ]],
      ["object-drag-cancel", "CONTROLS.ObjectDragCancel", [ `${game.i18n.localize("CONTROLS.RightClick")} ${game.i18n.localize("CONTROLS.During")} ${game.i18n.localize("CONTROLS.Drag")}` ]],
      ["object-rotate-slow", "CONTROLS.ObjectRotateSlow", [ KeyboardManager.CONTROL_KEY_STRING, "CONTROLS.MouseWheel" ]],
      ["object-rotate-fast", "CONTROLS.ObjectRotateFast", [ "Shift", "CONTROLS.MouseWheel" ]],
      ["place-hidden-token", "CONTROLS.TokenPlaceHidden", [ "Alt", "CONTROLS.Drop" ], true],
      ["token-target-mouse", "CONTROLS.TokenTarget", [ `${game.i18n.localize("CONTROLS.Double")} ${game.i18n.localize("CONTROLS.RightClick")}` ]]
    ];

    let coreMouseCategoryData = {
      id: "core-mouse",
      title: coreMouseCategory,
      actions: actions.map(a => defineMouseAction(...a)),
      count: 0
    };
    coreMouseCategoryData.count = coreMouseCategoryData.actions.length;
    categories.set("core-mouse", coreMouseCategoryData);
    return coreMouseCategoryData.count;
  }

  /* -------------------------------------------- */

  /**
   * Given an Binding and its parent Action, detects other Actions that might conflict with that binding
   * @param {string} actionId                   The namespaced Action ID the Binding belongs to
   * @param {KeybindingActionConfig} action     The Action config
   * @param {KeybindingActionBinding} binding   The Binding
   * @return {KeybindingAction[]}
   * @private
   */
  _detectConflictingActions(actionId, action, binding) {

    // Uneditable Core bindings are never wrong, they can never conflict with something
    if ( actionId.startsWith("core.") && action.uneditable.includes(binding) ) return [];

    // Build fake context
    /** @type KeyboardEventContext */
    const context = KeyboardManager.getKeyboardEventContext({
      code: binding.key,
      shiftKey: binding.modifiers.includes(KeyboardManager.MODIFIER_KEYS.SHIFT),
      ctrlKey: binding.modifiers.includes(KeyboardManager.MODIFIER_KEYS.CONTROL),
      altKey: binding.modifiers.includes(KeyboardManager.MODIFIER_KEYS.ALT),
      repeat: false
    });

    // Return matching keybinding actions (excluding this one)
    let matching = KeyboardManager._getMatchingActions(context);
    return matching.filter(a => a.action !== actionId);
  }

  /* -------------------------------------------- */

  /**
   * Transforms a Binding into a human readable string representation
   * @param {KeybindingActionBinding} binding   The Binding
   * @return {string}                           A human readable string
   * @private
   */
  static _humanizeBinding(binding) {
    const stringParts = binding.modifiers.reduce((parts, part) => {
      if ( KeyboardManager.MODIFIER_CODES[part]?.includes(binding.key) ) return parts;
      parts.unshift(KeyboardManager.getKeycodeDisplayString(part));
      return parts;
    }, [KeyboardManager.getKeycodeDisplayString(binding.key)]);
    return stringParts.join(" + ");

  }

  /* -------------------------------------------- */

  /**
   * Compares two Category Filters for rendering
   * This method ignores cases of equality because we know our categories are unique
   * @param a           The first Category
   * @param b           The second Category
   * @return {number}   A number for usage in the Sort method
   * @private
   */
  static _sortCategories(a, b) {
    // Stick All Actions at the top
    if ( a.id === "all" ) return -1;
    if ( b.id === "all"  ) return 1;

    // Followed by Core Keybindings
    if ( a.id === "core-keybindings" ) return -1;
    if ( b.id === "core-keybindings"  ) return 1;

    // Followed by Core Mouse Controls
    if ( a.id === "core-mouse" ) return -1;
    if ( b.id === "core-mouse" ) return 1;

    // Followed by System
    if ( a.title === game.system.data.title ) return -1;
    if ( b.title === game.system.data.title ) return 1;

    // Finally, modules
    return a.title.localeCompare(b.title);
  }

  /* -------------------------------------------- */

  /**
   * Classify what Category an Action belongs to
   * @param {KeybindingActionConfig} action   The Action to classify
   * @return {Object}                         The category the Action belongs to
   * @private
   */
  _categorizeAction(action) {
    if ( action.namespace === "core" )
      // If we want to further categorize Core Actions, we can do so here
      return {
        id: "core-keybindings",
        title: game.i18n.localize("KEYBINDINGS.CoreKeybindings")
      };
    else if ( action.namespace === game.system.data.name )
      return {
        id: action.namespace,
        title: game.system.data.title
      };
    else {
      const module = game.modules.get(action.namespace);
      if ( module )
        return {
          id: action.namespace,
          title: module.data.title
        };
      return {
        id: "general",
        title: game.i18n.localize("WEBRTC.GeneralTab")
      }
    }
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    html.find(".category-filter .filter").click(this._onClickCategoryFilter.bind(this));
    html.find(".category-header").click(this._onClickCategoryCollapse.bind(this));
    html.find(".reset-all").click(this._onClickResetActions.bind(this));
    const actionBindings = html.find(".action-bindings");
    actionBindings.on("dblclick", ".editable-binding", this._onDoubleClickKey.bind(this));
    actionBindings.on("click", ".control", this._onClickBindingControl.bind(this));
    actionBindings.on("keydown", ".binding-input", this._onKeydownBindingInput.bind(this));
    html.find("input[name=filter]").focus();
  }

  /* -------------------------------------------- */

  /**
   * Handle left-click events to filter to a certain category
   * @param {MouseEvent} event
   * @private
   */
  _onClickCategoryFilter(event) {
    event.preventDefault();
    this._category = event.target.dataset.category || "all";
    this.render();
  }

  /* -------------------------------------------- */

  /**
   * Handle left-click events to show / hide a certain category
   * @param {MouseEvent} event
   * @private
   */
  _onClickCategoryCollapse(event) {
    $(event.currentTarget).children(".category-collapse").toggleClass("hidden");
    $(event.currentTarget).siblings(".action-list").toggleClass("hidden");
  }

  /* -------------------------------------------- */

  /**
   * Handle left-click events to reset all Actions to Default
   * @param {MouseEvent} event
   * @private
   */
  async _onClickResetActions(event) {
    return Dialog.confirm({
      title: game.i18n.localize("KEYBINDINGS.ResetTitle"),
      content: `<h4>${game.i18n.localize("AreYouSure")}</h4><p>${game.i18n.localize("KEYBINDINGS.ResetWarning")}</p>`,
      yes: async () => {
        await game.keybindings.resetDefaults();
        this._cachedData = null;
        this._pendingEdits.clear();
        this.render();
      },
      no: () => {},
      defaultYes: false
    })
  }

  /* -------------------------------------------- */

  /**
   * Handle Control clicks
   * @param {MouseEvent} event
   * @private
   */
  _onClickBindingControl(event) {
    const button = event.currentTarget;
    switch ( button.dataset.action ) {
      case "add":
        this._onClickAdd(event); break;
      case "delete":
        this._onClickDelete(event); break;
      case "edit":
        return this._onClickEditableBinding(event);
      case "save":
        return this._onClickSaveBinding(event);
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle left-click events to show / hide a certain category
   * @param {MouseEvent} event
   * @private
   */
  async _onClickAdd(event) {
    const {actionHtml, namespace, action} = this._getParentAction(event);
    const {bindingHtml, bindingId} = this._getParentBinding(event);
    let totalBindings = actionHtml.getElementsByClassName("binding-input").length;
    const newBindingId = `${namespace}.${action}.binding.${totalBindings++}`;

    const toInsert =
      `<li class="binding flexrow inserted" data-binding-id="${newBindingId}">
          <div class="editable-binding">
              <div class="form-fields binding-fields">
                  <input type="text" class="binding-input" name="${newBindingId}" id="${newBindingId}" placeholder="Control + 1">
                  <i class="far fa-keyboard binding-input-icon"></i>
              </div>
          </div>
          <div class="binding-controls flexrow">
            <a class="control save-edit" title="${game.i18n.localize('KEYBINDINGS.SaveBinding')}" data-action="save"><i class="fas fa-save"></i></a>
            <a class="control" title="${game.i18n.localize('KEYBINDINGS.DeleteBinding')}" data-action="delete"><i class="fas fa-trash-alt"></i></a>
          </div>
      </li>`;

    bindingHtml.closest(".action-bindings").insertAdjacentHTML("beforeend", toInsert);
    document.getElementById(newBindingId).focus();

    // If this is an empty binding, delete it
    if ( bindingId === "empty" ) {
      bindingHtml.remove();
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle left-click events to show / hide a certain category
   * @param {MouseEvent} event
   * @private
   */
  async _onClickDelete(event) {
    const {namespace, action} = this._getParentAction(event);
    const {bindingId} = this._getParentBinding(event);
    const bindingIndex = Number.parseInt(bindingId.split(".")[3]);
    /** @typedef {KeybindingActionBinding} **/
    let binding = {
      index: bindingIndex,
      key: null
    };
    this._addPendingEdit(namespace, action, bindingIndex, binding);
    await this._savePendingEdits();
  }

  /* -------------------------------------------- */

  /**
   * Inserts a Binding into the Pending Edits object, creating a new Map entry as needed
   * @param {string} namespace
   * @param {string} action
   * @param {number} bindingIndex
   * @param {KeybindingActionBinding} binding
   * @private
   */
  _addPendingEdit(namespace, action, bindingIndex, binding) {
    // Save pending edits
    const pendingEditKey = `${namespace}.${action}`;
    if ( this._pendingEdits.has(pendingEditKey) ) {
      // Filter out any existing pending edits for this Binding so we don't add each Key in "Shift + A"
      let currentBindings = this._pendingEdits.get(pendingEditKey).filter(x => x.index !== bindingIndex);
      currentBindings.push(binding);
      this._pendingEdits.set(pendingEditKey, currentBindings);
    } else {
      this._pendingEdits.set(pendingEditKey, [ binding ]);
    }
  }

  /* -------------------------------------------- */

  /**
   * Toggle visibility of the Edit / Save UI
   * @param {MouseEvent} event
   * @private
   */
  _onClickEditableBinding(event) {
    const target = event.currentTarget;
    const bindingRow = target.closest("li.binding");
    target.classList.toggle("hidden");
    bindingRow.querySelector(".save-edit").classList.toggle("hidden");
    for ( let binding of bindingRow.querySelectorAll(".editable-binding") ) {
      binding.classList.toggle("hidden");
      binding.getElementsByClassName("binding-input")[0]?.focus();
    }
  }

  /* -------------------------------------------- */

  /**
   * Toggle visibility of the Edit UI
   * @param {MouseEvent} event
   * @private
   */
  _onDoubleClickKey(event) {
    const target = event.currentTarget;

    // If this is an inserted binding, don't try to swap to a non-edit mode
    if ( target.parentNode.parentNode.classList.includes("inserted") ) return;
    for ( let child of target.parentNode.getElementsByClassName("editable-binding") ) {
      child.classList.toggle("hidden");
      child.getElementsByClassName("binding-input")[0]?.focus();
    }
    const bindingRow = target.closest(".binding");
    for ( let child of bindingRow.getElementsByClassName("save-edit") ) {
      child.classList.toggle("hidden");
    }
  }

  /* -------------------------------------------- */

  /**
   * Save the new Binding value and update the display of the UI
   * @param {MouseEvent} event
   * @private
   */
  async _onClickSaveBinding(event) {
    // Set new value and reset the UI
    await this._savePendingEdits();
  }

  /* -------------------------------------------- */

  /**
   * Given a clicked Action element, finds the parent Action
   * @param {MouseEvent|KeyboardEvent} event
   * @return {{namespace: string, action: string, actionHtml: *}}
   * @private
   */
  _getParentAction(event) {
    // Get the Action target
    const actionHtml = event.currentTarget.closest(".action");
    const actionId = actionHtml.dataset.actionId;
    let [namespace, ...action] = actionId.split(".");
    action = action.join(".");
    return {actionHtml, namespace, action};
  }

  /* -------------------------------------------- */

  /**
   * Given a Clicked binding control element, finds the parent Binding
   * @param {MouseEvent|KeyboardEvent} event
   * @return {{bindingHtml: *, bindingId: string}}
   * @private
   */
  _getParentBinding(event) {
    // Get the Binding target
    const bindingHtml = event.currentTarget.closest(".binding");
    const bindingId = bindingHtml.dataset.bindingId;
    return {bindingHtml, bindingId};
  }

  /* -------------------------------------------- */

  /**
   * Iterates over all Pending edits, merging them in with unedited Bindings and then saving and resetting the UI
   * @return {Promise<void>}
   * @private
   */
  async _savePendingEdits() {
    // Keys are of the form <namespace>.<action>
    for ( let [id, pendingBindings] of this._pendingEdits ) {
      let [namespace, ...action] = id.split('.');
      action = action.join(".");
      const bindingsData = game.keybindings.bindings.get(id);
      const actionData = game.keybindings.actions.get(id);
      try {
        // Iterate over existing bindings, replacing them with the matching pending one if it exists
        let bindingsToSet = new Set();
        for ( let [index, bindingData] of bindingsData.entries() ) {
          let pending = pendingBindings.find(x => x.index === index);
          if ( pending ) bindingsToSet.add(pending);
          else if ( !actionData.uneditable.includes(bindingData) ) bindingsToSet.add(bindingData);
        }

        // Add any additional new ones
        for ( let x = bindingsData.length; x < bindingsData.length + pendingBindings.length; x++ ) {
          let additionalBinding = pendingBindings.find(b => b.index === x);
          if ( additionalBinding ) bindingsToSet.add(additionalBinding);
        }
        await game.keybindings.set(namespace, action, Array.from(bindingsToSet).filter(x => x.key !== null));
      }
      catch (e) {
        ui.notifications.error(e);
      }
    }

    // Reset and rerender
    this._cachedData = null;
    this._pendingEdits.clear();
    this.render();
  }

  /* -------------------------------------------- */

  /**
   * Processes input from the keyboard to form a list of pending Binding edits
   * @param {KeyboardEvent} event   The keyboard event
   * @private
   */
  _onKeydownBindingInput(event) {
    const context = KeyboardManager.getKeyboardEventContext(event);

    // Stop propagation
    event.preventDefault();
    event.stopPropagation();

    const {bindingHtml, bindingId} = this._getParentBinding(event);
    const {namespace, action} = this._getParentAction(event);

    // Build pending Binding
    const bindingIdParts = bindingId.split(".");
    const bindingIndex = Number.parseInt(bindingIdParts[bindingIdParts.length - 1]);
    const {MODIFIER_KEYS, MODIFIER_CODES} = KeyboardManager;
    /** @typedef {KeybindingActionBinding} **/
    let binding = {
      index: bindingIndex,
      key: context.key,
      modifiers: []
    };
    if ( context.isAlt && !MODIFIER_CODES[MODIFIER_KEYS.ALT].includes(context.key) ) {
      binding.modifiers.push(MODIFIER_KEYS.ALT);
    }
    if ( context.isShift && !MODIFIER_CODES[MODIFIER_KEYS.SHIFT].includes(context.key) ) {
      binding.modifiers.push(MODIFIER_KEYS.SHIFT);
    }
    if ( context.isControl && !MODIFIER_CODES[MODIFIER_KEYS.CONTROL].includes(context.key) ) {
      binding.modifiers.push(MODIFIER_KEYS.CONTROL);
    }

    // Save pending edits
    this._addPendingEdit(namespace, action, bindingIndex, binding);

    // Predetect potential conflicts
    const conflicts = this._detectConflictingActions(`${namespace}.${action}`, game.keybindings.actions.get(`${namespace}.${action}`), binding);
    const conflictString = game.i18n.format("KEYBINDINGS.Conflict", {
      conflicts: conflicts.map((action) => game.i18n.localize(action.name)).join(", ")
    });

    // Remove existing conflicts and add a new one
    for ( const conflict of bindingHtml.getElementsByClassName("conflicts") ) {
      conflict.remove();
    }
    if ( conflicts.length > 0 ) {
      const conflictHtml = `<div class="control conflicts" title="${conflictString}"><i class="fas fa-exclamation-triangle"></i></div>`
      bindingHtml.getElementsByClassName("binding-controls")[0].insertAdjacentHTML("afterbegin", conflictHtml);
    }

    // Set value
    event.currentTarget.value = this.constructor._humanizeBinding(binding);
  }

  /* -------------------------------------------- */

  /** @override */
  _onSearchFilter(event, query, rgx, html) {
    for ( let action of html.querySelectorAll(".action") ) {
      if ( !query ) {
        action.classList.remove("hidden");
        continue;
      }
      const title = action.querySelector(".action-title")?.textContent;
      const match = rgx.test(SearchFilter.cleanQuery(title));
      action.classList.toggle("hidden", !match);
    }
  }
}
