/**
 * Render the Sidebar container, and after rendering insert Sidebar tabs.
 * @extends {Application}
 */
class Sidebar extends Application {

  /**
   * Singleton application instances for each sidebar tab
   * @type {Object<SidebarTab>}
   */
  tabs = {};

  /**
   * Track whether the sidebar container is currently collapsed
   * @type {boolean}
   */
  _collapsed = false;

  /* -------------------------------------------- */

  /** @inheritdoc */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    id: "sidebar",
      template: "templates/sidebar/sidebar.html",
      popOut: false,
      width: 300,
      tabs: [{navSelector: ".tabs", contentSelector: "#sidebar", initial: "chat"}]
    });
  }

  /* -------------------------------------------- */

  /**
   * Return the name of the active Sidebar tab
   * @type {string}
   */
  get activeTab() {
    return this._tabs[0].active;
  }

  /* -------------------------------------------- */

  /**
   * Singleton application instances for each popout tab
   * @type {Object<SidebarTab>}
   */
  get popouts() {
    const popouts = {};
    for ( let [name, app] of Object.entries(this.tabs) ) {
      if ( app._popout ) popouts[name] = app._popout;
    }
    return popouts;
  }

	/* -------------------------------------------- */
  /*  Rendering
	/* -------------------------------------------- */

  /** @override */
  getData(options) {
    return {
      coreUpdate: game.user.isGM && game.data.coreUpdate.hasUpdate ? game.i18n.format("SETUP.UpdateAvailable", {
        type: game.i18n.localize("Software"),
        channel: game.data.coreUpdate.channel,
        version: game.data.coreUpdate.version
      }) : false,
      systemUpdate: game.user.isGM && game.data.systemUpdate ? game.i18n.format("SETUP.UpdateAvailable", {
        type: game.i18n.localize("System"),
        channel: game.data.system.data.title,
        version: game.data.systemUpdate.version
      }) : false,
      user: game.user
    };
  }

 	/* -------------------------------------------- */

  /** @inheritdoc */
	async _render(force, options) {

	  // Render the Sidebar container only once
    if ( !this.rendered ) await super._render(force, options);

    // Define the sidebar tab names to render
	  const tabs = ["chat", "combat", "actors", "items", "journal", "tables", "cards", "playlists", "compendium", "settings"];
	  if ( game.user.isGM ) tabs.push("scenes");

    // Render sidebar Applications
    for ( let [name, app] of Object.entries(this.tabs) ) {
      app._render(true).catch(err => {
        Hooks.onError("Sidebar#_render", err, {
          msg: `Failed to render Sidebar tab ${name}`,
          log: "error",
          name
        });
      });
    }
  }

	/* -------------------------------------------- */
  /*  Methods
	/* -------------------------------------------- */

  /**
   * Activate a Sidebar tab by it's name
   * @param {string} tabName      The tab name corresponding to it's "data-tab" attribute
   */
  activateTab(tabName) {
    this._tabs[0].activate(tabName, {triggerCallback: true});
  }

	/* -------------------------------------------- */

  /**
   * Expand the Sidebar container from a collapsed state.
   * Take no action if the sidebar is already expanded.
   */
  expand() {
    if ( !this._collapsed ) return;
    const sidebar = this.element;
    const tab = sidebar.find(".sidebar-tab.active");
    const icon = sidebar.find("#sidebar-tabs a.collapse i");

    // Animate the sidebar expansion
    tab.hide();
    sidebar.animate({width: this.options.width, height: this.position.height}, 150, () => {
      sidebar.css({width: "", height: ""}); // Revert to default styling
      sidebar.removeClass("collapsed");
      tab.fadeIn(250, () => {
        tab.css({
          display: "",
          height: ""
        });
      });
      icon.removeClass("fa-caret-left").addClass("fa-caret-right");
      this._collapsed = false;
      /**
       * A hook event that fires when the Sidebar is collapsed or expanded.
       * @function collapseSidebar
       * @memberof hookEvents
       * @param {Sidebar} sidebar   The Sidebar application
       * @param {boolean} collapsed Whether the Sidebar is now collapsed or not
       */
      Hooks.callAll("collapseSidebar", this, this._collapsed);
    })
  }

	/* -------------------------------------------- */

  /**
   * Collapse the sidebar to a minimized state.
   * Take no action if the sidebar is already collapsed.
   */
  collapse() {
    if ( this._collapsed ) return;
    const sidebar = this.element;
    const tab = sidebar.find(".sidebar-tab.active");
    const icon = sidebar.find("#sidebar-tabs a.collapse i");

    // Animate the sidebar collapse
    tab.fadeOut(250, () => {
      sidebar.animate({width: 32, height: (32 + 4) * (Object.values(this.tabs).length + 1)}, 150, () => {
        sidebar.css("height", ""); // Revert to default styling
        sidebar.addClass("collapsed");
        tab.css("display", "");
        icon.removeClass("fa-caret-right").addClass("fa-caret-left");
        this._collapsed = true;
        Hooks.callAll("collapseSidebar", this, this._collapsed);
      })
    })
  }

	/* -------------------------------------------- */
  /*  Event Listeners and Handlers
	/* -------------------------------------------- */

  /** @inheritdoc */
	activateListeners(html) {
	  super.activateListeners(html);

    // Right click pop-out
    const nav = this._tabs[0]._nav;
    nav.addEventListener('contextmenu', this._onRightClickTab.bind(this));

    // Toggle Collapse
    const collapse = nav.querySelector(".collapse");
    collapse.addEventListener("click", this._onToggleCollapse.bind(this));

    // Left click a tab
    const tabs = nav.querySelectorAll(".item");
    tabs.forEach(tab => tab.addEventListener("click", this._onLeftClickTab.bind(this)));
  }

	/* -------------------------------------------- */

  /** @override */
  _onChangeTab(event, tabs, active) {
    const app = ui[active];
    if ( (active === "chat") && app ) app.scrollBottom();

    /**
     * A hook event that fires when the Sidebar tab is changed.
     * @function changeSidebarTab
     * @memberof hookEvents
     * @param {SidebarTab} app    The SidebarTab application which is now active
     */
    Hooks.callAll("changeSidebarTab", app);
  }

  /* -------------------------------------------- */

  /**
   * Handle the special case of left-clicking a tab when the sidebar is collapsed.
   * @param {MouseEvent} event  The originating click event
   * @private
   */
  _onLeftClickTab(event) {
    const app = ui[event.currentTarget.dataset.tab];
    if ( app && this._collapsed ) app.renderPopout(app);
  }

  /* -------------------------------------------- */

  /**
   * Handle right-click events on tab controls to trigger pop-out containers for each tab
   * @param {Event} event     The originating contextmenu event
   * @private
   */
  _onRightClickTab(event) {
    const li = event.target.closest(".item");
    if ( !li ) return;
    event.preventDefault();
    const tabApp = ui[li.dataset.tab];
    tabApp.renderPopout(tabApp);
  }

  /* -------------------------------------------- */

  /**
   * Handle toggling of the Sidebar container's collapsed or expanded state
   * @param {Event} event
   * @private
   */
  _onToggleCollapse(event) {
    event.preventDefault();
    if ( this._collapsed ) this.expand();
    else this.collapse();
  }
}
