

/* -------------------------------------------- */
/*  HTML Template Loading                       */
/* -------------------------------------------- */

// Global template cache
_templateCache = {};

/**
 * Get a template from the server by fetch request and caching the retrieved result
 * @param {string} path           The web-accessible HTML template URL
 * @returns {Promise<Function>}	  A Promise which resolves to the compiled Handlebars template
 */
async function getTemplate(path) {
	if ( !_templateCache.hasOwnProperty(path) ) {
    await new Promise((resolve, reject) => {
    	game.socket.emit('template', path, resp => {
    	  if ( resp.error ) return reject(new Error(resp.error));
	      const compiled = Handlebars.compile(resp.html);
	      Handlebars.registerPartial(path, compiled);
	      _templateCache[path] = compiled;
	      console.log(`Foundry VTT | Retrieved and compiled template ${path}`);
	      resolve(compiled);
	    });
    });
	}
	return _templateCache[path];
}

/* -------------------------------------------- */

/**
 * Load and cache a set of templates by providing an Array of paths
 * @param {string[]} paths    An array of template file paths to load
 * @return {Promise<Function[]>}
 */
async function loadTemplates(paths) {
  return Promise.all(paths.map(p => getTemplate(p)));
}

/* -------------------------------------------- */


/**
 * Get and render a template using provided data and handle the returned HTML
 * Support asynchronous file template file loading with a client-side caching layer
 *
 * Allow resolution of prototype methods and properties since this all occurs within the safety of the client.
 * @see {@link https://handlebarsjs.com/api-reference/runtime-options.html#options-to-control-prototype-access}
 *
 * @param {string} path             The file path to the target HTML template
 * @param {Object} data             A data object against which to compile the template
 *
 * @return {Promise<string>}        Returns the compiled and rendered template as a string
 */
async function renderTemplate(path, data) {
  const template = await getTemplate(path);
  return template(data || {}, {
    allowProtoMethodsByDefault: true,
    allowProtoPropertiesByDefault: true
  });
}


/* -------------------------------------------- */
/*  Handlebars Template Helpers                 */
/* -------------------------------------------- */

// Register Handlebars Extensions
HandlebarsIntl.registerWith(Handlebars);

/**
 * A collection of Handlebars template helpers which can be used within HTML templates.
 */
class HandlebarsHelpers {

  /**
   * For checkboxes, if the value of the checkbox is true, add the "checked" property, otherwise add nothing.
   * @return {string}
   */
  static checked(value) {
    return Boolean(value) ? "checked" : "";
  }

  /* -------------------------------------------- */

  /**
   * For use in form inputs. If the supplied value is truthy, add the "disabled" property, otherwise add nothing.
   * @returns {string}
   */
  static disabled(value) {
    return value ? "disabled" : "";
  }

  /* -------------------------------------------- */

  /**
   * Concatenate a number of string terms into a single string.
   * This is useful for passing arguments with variable names.
   * @param {string[]} values             The values to concatenate
   * @return {Handlebars.SafeString}
   *
   * @example <caption>Concatenate several string parts to create a dynamic variable</caption>
   * {{filePicker target=(concat "faces." i ".img") type="image"}}
   */
  static concat(...values) {
    const options = values.pop();
    const join = options.hash?.join || "";
    return new Handlebars.SafeString(values.join(join));
  }

  /* -------------------------------------------- */

  /**
   * Render a pair of inputs for selecting a color.
   * @param {object} options              Helper options
   * @param {string} [options.name]       The name of the field to create
   * @param {string} [options.value]      The current color value
   * @param {string} [options.default]    A default color string if a value is not provided
   * @return {Handlebars.SafeString}
   */
  static colorPicker(options) {
    let {name, value} = options.hash;
    name = name || color;
    value = value || "";
    const safeValue = value || options.hash["default"] || "#000000";
    const html =
    `<input class="color" type="text" name="${name}" value="${value}"/>
    <input type="color" value="${safeValue}" data-edit="${name}"/>`;
    return new Handlebars.SafeString(html);
  }

  /* -------------------------------------------- */

  /**
   * Construct an editor element for rich text editing with TinyMCE
   * @param {object} options              Helper options
   * @param {string} [options.target]     The named target data element
   * @param {boolean} [options.owner]     Is the current user an owner of the data?
   * @param {boolean} [options.button]    Include a button used to activate the editor later?
   * @param {boolean} [options.editable]  Is the text editor area currently editable?
   * @param {boolean} [options.documents=true] Replace dynamic document links?
   * @param {Object|Function} [options.rollData] The data object providing context for inline rolls
   * @param {string} [options.content=""]  The original HTML content as a string
   * @return {Handlebars.SafeString}
   */
  static editor(options) {
    const target = options.hash['target'];
    if ( !target ) throw new Error("You must define the name of a target field.");

    // Enrich the content
    let documents = options.hash.documents !== false;
    if ( options.hash.entities !== undefined ) {
      console.warn("The 'entities' argument for the editor helper is deprecated. Please use 'documents' instead.");
      documents = options.hash.entities !== false;
    }
    const owner = Boolean(options.hash['owner']);
    const rollData = options.hash["rollData"];
    const content = TextEditor.enrichHTML(options.hash['content'] || "", {secrets: owner, documents, rollData});

    // Construct the HTML
    let editor = $(`<div class="editor"><div class="editor-content" data-edit="${target}">${content}</div></div>`);

    // Append edit button
    const button = Boolean(options.hash['button']);
    const editable = Boolean(options.hash['editable']);
    if ( button && editable ) editor.append($('<a class="editor-edit"><i class="fas fa-edit"></i></a>'));
    return new Handlebars.SafeString(editor[0].outerHTML);
  }

  /* -------------------------------------------- */

  /**
   * Render a file-picker button linked to an <input> field
   * @param {object} options              Helper options
   * @param {string} [options.type]       The type of FilePicker instance to display
   * @param {string} [options.target]     The field name in the target data
   * @return {Handlebars.SafeString|string}
   */
  static filePicker(options) {
    const type = options.hash['type'];
    const target = options.hash['target'];
    if ( !target ) throw new Error("You must define the name of the target field.");

    // Do not display the button for users who do not have browse permission
    if ( game.world && !game.user.can("FILES_BROWSE" ) ) return "";

    // Construct the HTML
    const tooltip = game.i18n.localize("FILES.BrowseTooltip");
    return new Handlebars.SafeString(`
    <button type="button" class="file-picker" data-type="${type}" data-target="${target}" title="${tooltip}" tabindex="-1">
        <i class="fas fa-file-import fa-fw"></i>
    </button>`);
  }

  /* -------------------------------------------- */

  /**
   * Translate a provided string key by using the loaded dictionary of localization strings.
   * @return {string}
   *
   * @example <caption>Translate a provided localization string, optionally including formatting parameters</caption>
   * <label>{{localize "ACTOR.Create"}}</label> <!-- "Create Actor" -->
   * <label>{{localize "CHAT.InvalidCommand" command=foo}}</label> <!-- "foo is not a valid chat message command." -->
   */
  static localize(value, options) {
    const data = options.hash;
    return isObjectEmpty(data) ? game.i18n.localize(value) : game.i18n.format(value, data);
  }

  /* -------------------------------------------- */

  /**
   * A string formatting helper to display a number with a certain fixed number of decimals and an explicit sign.
   * @param {number} value              A numeric value to format
   * @param {object} options            Additional options which customize the resulting format
   * @param {number} [options.decimals=0]   The number of decimal places to include in the resulting string
   * @param {boolean} [options.sign=false]  Whether to include an explicit "+" sign for positive numbers   *
   * @returns {Handlebars.SafeString}   The formatted string to be included in a template
   */
  static numberFormat(value, options) {
    const dec = options.hash['decimals'] ?? 0;
    const sign = options.hash['sign'] || false;
    value = parseFloat(value).toFixed(dec);
    if (sign ) return ( value >= 0 ) ? "+"+value : value;
    return value;
  }

  /* --------------------------------------------- */

  /**
   * Render a form input field of type number with value appropriately rounded to step size.
   * @return {Handlebars.SafeString}
   */
  static numberInput(value, options) {
    const properties = [];
    for ( let k of ["class", "name", "placeholder", "min", "max"] ) {
      if ( k in options.hash ) properties.push(`${k}="${options.hash[k]}"`);
    }
    const step = options.hash["step"] ?? "any";
    properties.unshift(`step="${step}"`)
    if ( options.hash["disabled"] === true ) properties.push("disabled");
    let safe = Number.isNumeric(value) ? Number(value) : "";
    if ( Number.isNumeric(step) && (typeof safe === "number") ) safe = safe.toNearest(Number(step));
    return new Handlebars.SafeString(`<input type="number" value="${safe}" ${properties.join(" ")}>`);
  }

  /* -------------------------------------------- */

  /**
   * A helper to create a set of radio checkbox input elements in a named set.
   * The provided keys are the possible radio values while the provided values are human readable labels.
   *
   * @param {string} name         The radio checkbox field name
   * @param {object} choices      A mapping of radio checkbox values to human readable labels
   * @param {object} options      Options which customize the radio boxes creation
   * @param {string} options.checked    Which key is currently checked?
   * @param {boolean} options.localize  Pass each label through string localization?
   * @return {Handlebars.SafeString}
   *
   * @example <caption>The provided input data</caption>
   * let groupName = "importantChoice";
   * let choices = {a: "Choice A", b: "Choice B"};
   * let chosen = "a";
   *
   * @example <caption>The template HTML structure</caption>
   * <div class="form-group">
   *   <label>Radio Group Label</label>
   *   <div class="form-fields">
   *     {{radioBoxes groupName choices checked=chosen localize=true}}
   *   </div>
   * </div>
   */
  static radioBoxes(name, choices, options) {
    const checked = options.hash['checked'] || null;
    const localize = options.hash['localize'] || false;
    let html = "";
    for ( let [key, label] of Object.entries(choices) ) {
      if ( localize ) label = game.i18n.localize(label);
      const isChecked = checked === key;
      html += `<label class="checkbox"><input type="radio" name="${name}" value="${key}" ${isChecked ? "checked" : ""}> ${label}</label>`;
    }
    return new Handlebars.SafeString(html);
  }

  /* -------------------------------------------- */

  /**
   * Render a pair of inputs for selecting a value in a range.
   * @param {object} options            Helper options
   * @param {string} [options.name]     The name of the field to create
   * @param {number} [options.value]    The current range value
   * @param {number} [options.min]      The minimum allowed value
   * @param {number} [options.max]      The maximum allowed value
   * @param {number} [options.step]     The allowed step size
   * @return {Handlebars.SafeString}
   */
  static rangePicker(options) {
    let {name, value, min, max, step} = options.hash;
    name = name || "range";
    value = value ?? "";
    if ( Number.isNaN(value) ) value = "";
    const html =
    `<input type="range" name="${name}" value="${value}" min="${min}" max="${max}" step="${step}"/>
     <span class="range-value">${value}</span>`;
    return new Handlebars.SafeString(html);
  }

  /* -------------------------------------------- */

  /**
  * A helper to assign an &lt;option> within a &lt;select> block as selected based on its value
  * Escape the string as handlebars would, then escape any regexp characters in it
  * @return {Handlebars.SafeString}
  */
  static select(selected, options) {
    const escapedValue = RegExp.escape(Handlebars.escapeExpression(selected));
    const rgx = new RegExp(' value=[\"\']' + escapedValue + '[\"\']');
    const html = options.fn(this);
    return html.replace(rgx, "$& selected");
  }

  /* -------------------------------------------- */

  /**
   * A helper to create a set of &lt;option> elements in a &lt;select> block based on a provided dictionary.
   * The provided keys are the option values while the provided values are human readable labels.
   * This helper supports both single-select as well as multi-select input fields.
   *
   * @param {object} choices                     A mapping of radio checkbox values to human readable labels
   * @param {object} options                     Helper options
   * @param {string|string[]} [options.selected] Which key or array of keys that are currently selected?
   * @param {boolean} [options.localize=false]   Pass each label through string localization?
   * @param {string} [options.blank]             Add a blank option as the first option with this label
   * @param {string} [options.nameAttr]          Look up a property in the choice object values to use as the option value
   * @param {string} [options.labelAttr]         Look up a property in the choice object values to use as the option label
   * @param {boolean} [options.inverted=false]   Use the choice object value as the option value, and the key as the label
   *                                             instead of vice-versa
   * @return {Handlebars.SafeString}
   *
   * @example <caption>The provided input data</caption>
   * let choices = {a: "Choice A", b: "Choice B"};
   * let value = "a";
   *
   * @example <caption>The template HTML structure</caption>
   * <select name="importantChoice">
   *   {{selectOptions choices selected=value localize=true}}
   * </select>
   *
   * @example <caption>The resulting HTML</caption>
   * <select name="importantChoice">
   *   <option value="a" selected>Choice A</option>
   *   <option value="b">Choice B</option>
   * </select>
   *
   * @example <caption>Using inverted</caption>
   * let choices = {"Choice A": "a", "Choice B": "b"};
   * let value = "a";
   *
   * @example <caption>The template HTML structure</caption>
   * <select name="importantChoice">
   *   {{selectOptions choices selected=value inverted=true}}
   * </select>
   *
   * @example <caption>Using nameAttr and labelAttr with objects</caption>
   * let choices = {foo: {key: "a", label: "Choice A"}, bar: {key: "b", label: "Choice B"}};
   * let value = "b";
   *
   * @example <caption>The template HTML structure</caption>
   * <select name="importantChoice">
   *   {{selectOptions choices selected=value nameAttr="key" labelAttr="label"}}
   * </select>
   *
   * @example <caption>Using nameAttr and labelAttr with arrays</caption>
   * let choices = [{key: "a", label: "Choice A"}, {key: "b", label: "Choice B"}];
   * let value = "b";
   *
   * @example <caption>The template HTML structure</caption>
   * <select name="importantChoice">
   *   {{selectOptions choices selected=value nameAttr="key" labelAttr="label"}}
   * </select>
   */
  static selectOptions(choices, options) {
    const localize = options.hash['localize'] ?? false;
    let selected = options.hash['selected'] ?? null;
    let blank = options.hash['blank'] ?? null;
    let nameAttr = options.hash["nameAttr"];
    let labelAttr = options.hash["labelAttr"];
    let inverted = !!options.hash["inverted"];
    selected = selected instanceof Array ? selected.map(String) : [String(selected)];

    // Create an option
    const option = (name, label) => {
      if ( localize ) label = game.i18n.localize(label);
      let isSelected = selected.includes(String(name));
      html += `<option value="${name}" ${isSelected ? "selected" : ""}>${label}</option>`
    };

    // Create the options
    let html = "";
    if ( blank !== null ) option("", blank);

    // Options as an Array
    if ( choices instanceof Array ) {
      for ( let choice of choices ) {
        option(choice[nameAttr], choice[labelAttr]);
      }
    }

    // Choices as an Object
    else {
      for ( let choice of Object.entries(choices) ) {
        let [key, value] = inverted ? choice.reverse() : choice;
        if ( nameAttr ) key = value[nameAttr];
        if ( labelAttr ) value = value[labelAttr];
        option(key, value);
      }
    }
    return new Handlebars.SafeString(html);
  }
}

// Register all handlebars helpers
Handlebars.registerHelper({
  checked: HandlebarsHelpers.checked,
  disabled: HandlebarsHelpers.disabled,
  colorPicker: HandlebarsHelpers.colorPicker,
  concat: HandlebarsHelpers.concat,
  editor: HandlebarsHelpers.editor,
  filePicker: HandlebarsHelpers.filePicker,
  numberFormat: HandlebarsHelpers.numberFormat,
  numberInput: HandlebarsHelpers.numberInput,
  localize: HandlebarsHelpers.localize,
  radioBoxes: HandlebarsHelpers.radioBoxes,
  rangePicker: HandlebarsHelpers.rangePicker,
  select: HandlebarsHelpers.select,
  selectOptions: HandlebarsHelpers.selectOptions,
  timeSince: timeSince,
  eq: (v1, v2) => v1 === v2,
  ne: (v1, v2) => v1 !== v2,
  lt: (v1, v2) => v1 < v2,
  gt: (v1, v2) => v1 > v2,
  lte: (v1, v2) => v1 <= v2,
  gte: (v1, v2) => v1 >= v2,
  not: (pred) => !pred,
  and() { return Array.prototype.every.call(arguments, Boolean) },
  or() { return Array.prototype.slice.call(arguments, 0, -1).some(Boolean) }
});
