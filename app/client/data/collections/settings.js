/**
 * The Collection of Setting documents which exist within the active World.
 * This collection is accessible as game.settings.storage.get("world")
 * @extends {WorldCollection}
 *
 * @see {@link Setting} The Setting document
 */
class WorldSettings extends WorldCollection {

  /** @override */
  static documentName = "Setting";

	/* -------------------------------------------- */

  /** @override */
  get directory() {}

	/* -------------------------------------------- */
  /* World Settings Methods                       */
  /* -------------------------------------------- */

  /**
   * Return the Setting or Keybind document with the given key.
   * @param {string} key                 The key
   * @return {Setting|Keybind|undefined} The Setting or Keybind.
   */
  getSetting(key) {
    return this.find(s => s.key === key);
  }

  /**
   * Return the serialized value of the world setting as a string
   * @param {string} key    The setting key
   * @return {string|null}  The serialized setting string
   */
  getItem(key) {
    const setting = this.getSetting(key);
    return setting?.data.value ?? null;
  }
}
