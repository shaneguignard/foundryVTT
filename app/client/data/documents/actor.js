/**
 * The client-side Actor document which extends the common BaseActor model.
 * Each Actor document contains ActorData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseActor
 * @extends ClientDocumentMixin
 *
 * @see {@link data.ActorData}              The Actor data schema
 * @see {@link documents.Actors}            The world-level collection of Actor documents
 * @see {@link applications.ActorSheet}     The Actor configuration application
 *
 * @param {data.ActorData} [data={}]        Initial data provided to construct the Actor document
 * @param {object} [context={}]             The document context, see {@link foundry.abstract.Document}
 *
 * @example <caption>Create a new Actor</caption>
 * let actor = await Actor.create({
 *   name: "New Test Actor",
 *   type: "character",
 *   img: "artwork/character-profile.jpg"
 * });
 *
 * @example <caption>Retrieve an existing Actor</caption>
 * let actor = game.actors.get(actorId);
 */
class Actor extends ClientDocumentMixin(foundry.documents.BaseActor) {
  constructor(data, context) {
    super(data, context);

    /**
     * An object that tracks which tracks the changes to the data model which were applied by active effects
     * @type {object}
     */
    this.overrides = this.overrides || {};

    /**
     * A cached array of image paths which can be used for this Actor's token.
     * Null if the list has not yet been populated.
     * @type {string[]|null}
     * @private
     */
    this._tokenImages = null;

    /**
     * Cache the last drawn wildcard token to avoid repeat draws
     * @type {string|null}
     */
    this._lastWildcard = null;
  }

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * A convenient reference to the file path of the Actor's profile image
   * @type {string}
   */
  get img() {
    return this.data.img;
  }

  /* -------------------------------------------- */

  /**
   * Provide a thumbnail image path used to represent this document.
   * @type {string}
   */
  get thumbnail() {
    return this.data.img;
  }

  /* -------------------------------------------- */

  /**
   * Provide an object which organizes all embedded Item instances by their type
   * @type {Object<documents.Item[]>}
   */
  get itemTypes() {
    const types = Object.fromEntries(game.system.documentTypes.Item.map(t => [t, []]));
    for ( let i of this.items.values() ) {
      types[i.data.type].push(i);
    }
    return types;
  }

  /* -------------------------------------------- */

  /**
   * Test whether an Actor document is a synthetic representation of a Token (if true) or a full Document (if false)
   * @type {boolean}
   */
  get isToken() {
    if ( !this.parent ) return false;
    return this.parent instanceof TokenDocument;
  }

  /* -------------------------------------------- */

  /**
   * An array of ActiveEffect instances which are present on the Actor which have a limited duration.
   * @return {ActiveEffect[]}
   */
  get temporaryEffects() {
    return this.effects.filter(e => e.isTemporary && !e.data.disabled);
  }

  /* -------------------------------------------- */

  /**
   * Return a reference to the TokenDocument which owns this Actor as a synthetic override
   * @returns {TokenDocument|null}
   */
  get token() {
    return this.parent instanceof TokenDocument ? this.parent : null;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get uuid() {
    if ( this.isToken ) return this.token.uuid;
    return super.uuid;
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Apply any transformations to the Actor data which are caused by ActiveEffects.
   */
  applyActiveEffects() {
    const overrides = {};

    // Organize non-disabled effects by their application priority
    const changes = this.effects.reduce((changes, e) => {
      if ( e.data.disabled || e.isSuppressed ) return changes;
      return changes.concat(e.data.changes.map(c => {
        c = foundry.utils.duplicate(c);
        c.effect = e;
        c.priority = c.priority ?? (c.mode * 10);
        return c;
      }));
    }, []);
    changes.sort((a, b) => a.priority - b.priority);

    // Apply all changes
    for ( let change of changes ) {
      const result = change.effect.apply(this, change);
      if ( result !== null ) overrides[change.key] = result;
    }

    // Expand the set of final overrides
    this.overrides = foundry.utils.expandObject(overrides);
  }

  /* -------------------------------------------- */

  /**
   * Retrieve an Array of active tokens which represent this Actor in the current canvas Scene.
   * If the canvas is not currently active, or there are no linked actors, the returned Array will be empty.
   * If the Actor is a synthetic token actor, only the exact Token which it represents will be returned.
   *
   * @param {boolean} [linked=false]  Limit results to Tokens which are linked to the Actor. Otherwise return all Tokens even those which are not linked.   *
   * @param {boolean} [document=false]  Return the Document instance rather than the PlaceableObject
   * @return Token[]}       An array of Token instances in the current Scene which reference this Actor.
   */
  getActiveTokens(linked=false, document=false) {
    if ( !canvas.ready ) return [];

    // Synthetic token actors are, themselves, active tokens
    if ( this.isToken ) {
      if ( this.token.parent !== canvas.scene ) return [];
      return document ? [this.token] : [this.token.object];
    }

    // Otherwise find tokens within the current scene
    const tokens = [];
    for ( let t of canvas.scene.tokens ) {
      if ( t.data.actorId !== this.id ) continue;
      if ( !linked || t.data.actorLink ) tokens.push(document ? t : t.object);
    }
    return tokens;
  }

  /* -------------------------------------------- */

  /**
   * Prepare a data object which defines the data schema used by dice roll commands against this Actor
   * @return {object}
   */
  getRollData() {
    return this.data.data;
  }

  /* -------------------------------------------- */

  /**
   * Create a new TokenData object which can be used to create a Token representation of the Actor.
   * @param {object} [data={}]            Additional data, such as x, y, rotation, etc. for the created token data
   * @return {Promise<data.TokenData>}    The created TokenData instance
   */
  async getTokenData(data={}) {
    const tokenData = this.data.token.toObject();
    tokenData.actorId = this.id;

    if ( tokenData.randomImg && !data.img ) {
      let images = await this.getTokenImages();
      if ( (images.length > 1) && this._lastWildcard ) {
        images = images.filter(i => i !== this._lastWildcard);
      }
      const image = images[Math.floor(Math.random() * images.length)];
      tokenData.img = this._lastWildcard = image;
    }
    foundry.utils.mergeObject(tokenData, data);
    return new foundry.data.TokenData(tokenData);
  }

  /* -------------------------------------------- */

  /**
   * Get an Array of Token images which could represent this Actor
   * @return {Promise<string[]>}
   */
  async getTokenImages() {
    if (!this.data.token.randomImg) return [this.data.token.img];
    if (this._tokenImages) return this._tokenImages;
    let source = "data";
    let pattern = this.data.token.img;
    const browseOptions = { wildcard: true };

    // Support non-user sources
    if ( /\.s3\./.test(pattern) ) {
      source = "s3";
      const {bucket, keyPrefix} = FilePicker.parseS3URL(pattern);
      if ( bucket ) {
        browseOptions.bucket = bucket;
        pattern = keyPrefix;
      }
    }
    else if ( pattern.startsWith("icons/") ) source = "public";

    // Retrieve wildcard content
    try {
      const content = await FilePicker.browse(source, pattern, browseOptions);
      this._tokenImages = content.files;
    } catch(err) {
      this._tokenImages = [];
      Hooks.onError("Actor#getTokenImages", err, {
        msg: "Error retrieving wildcard tokens",
        log: "error",
        notify: "error"
      });
    }
    return this._tokenImages;
  }

  /* -------------------------------------------- */

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * This allows for game systems to override this behavior and deploy special logic.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise<documents.Actor>}  The updated Actor document
   */
  async modifyTokenAttribute(attribute, value, isDelta=false, isBar=true) {
    const current = foundry.utils.getProperty(this.data.data, attribute);

    // Determine the updates to make to the actor data
    let updates;
    if ( isBar ) {
      if (isDelta) value = Math.clamped(0, Number(current.value) + value, current.max);
      updates = {[`data.${attribute}.value`]: value};
    } else {
      if ( isDelta ) value = Number(current) + value;
      updates = {[`data.${attribute}`]: value};
    }

    /**
     * A hook event that fires when a token's resource bar attribute has been modified.
     * @function modifyTokenAttribute
     * @memberof hookEvents
     * @param {object} data           An object describing the modification
     * @param {string} data.attribute The attribute path
     * @param {number} data.value     The target attribute value
     * @param {boolean} data.isDelta  Whether the number represents a relative change (true) or an absolute change (false)
     * @param {boolean} data.isBar    Whether the new value is part of an attribute bar, or just a direct value
     * @param {objects} updates       The update delta that will be applied to the Token's actor
     */
    const allowed = Hooks.call("modifyTokenAttribute", {attribute, value, isDelta, isBar}, updates);
    return allowed !== false ? this.update(updates) : this;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  prepareEmbeddedDocuments() {
    super.prepareEmbeddedDocuments();
    this.applyActiveEffects();
  }

  /* -------------------------------------------- */

  /**
   * Roll initiative for all Combatants in the currently active Combat encounter which are associated with this Actor.
   * If viewing a full Actor document, all Tokens which map to that actor will be targeted for initiative rolls.
   * If viewing a synthetic Token actor, only that particular Token will be targeted for an initiative roll.
   *
   * @param {object} options                          Configuration for how initiative for this Actor is rolled.
   * @param {boolean} [options.createCombatants=false]    Create new Combatant entries for Tokens associated with this actor.
   * @param {boolean} [options.rerollInitiative=false]    Re-roll the initiative for this Actor if it has already been rolled.
   * @param {object} [options.initiativeOptions={}]       Additional options passed to the Combat#rollInitiative method.
   * @return {Promise<documents.Combat|null>}         A promise which resolves to the Combat document once rolls are complete.
   */
  async rollInitiative({createCombatants=false, rerollInitiative=false, initiativeOptions={}}={}) {

    // Obtain (or create) a combat encounter
    let combat = game.combat;
    if ( !combat ) {
      if ( game.user.isGM && canvas.scene ) {
        const cls = getDocumentClass("Combat")
        combat = await cls.create({scene: canvas.scene.id, active: true});
      }
      else {
        ui.notifications.warn("COMBAT.NoneActive", {localize: true});
        return null;
      }
    }

    // Create new combatants
    if ( createCombatants ) {
      const tokens = this.getActiveTokens();
      const toCreate = [];
      if ( tokens.length ) {
        for ( let t of tokens ) {
          if ( t.inCombat ) continue;
          toCreate.push({tokenId: t.id, sceneId: t.scene.id, actorId: this.id, hidden: t.data.hidden});
        }
      } else toCreate.push({actorId: this.id, hidden: false})
      await combat.createEmbeddedDocuments("Combatant", toCreate);
    }

    // Roll initiative for combatants
    const combatants = combat.combatants.reduce((arr, c) => {
      if ( c.actor.id !== this.id ) return arr;
      if ( !rerollInitiative && (c.initiative !== null) ) return arr;
      arr.push(c.id);
      return arr;
    }, []);

    await combat.rollInitiative(combatants, initiativeOptions);
    return combat;
  }

  /* -------------------------------------------- */
  /*  Database Operations                         */
  /* -------------------------------------------- */

  /** @inheritdoc */
  getEmbeddedCollection(embeddedName) {
    if ( embeddedName === "OwnedItem" ) {
      console.warn('You are using "OwnedItem" as an embedded Document name within the Actor document. This has changed due to the symmetric Document standardization in 0.8.0 and you must now use "Item" as the embedded document name.')
      embeddedName = "Item";
    }
    return super.getEmbeddedCollection(embeddedName);
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  async _preCreate(data, options, userId) {
    await super._preCreate(data, options, userId);
    // Apply default token preferences.
    const token = foundry.utils.deepClone(game.settings.get("core", DefaultTokenConfig.SETTING));
    this.data.token.update(foundry.utils.mergeObject(token, data.token || {}));
  }

  /* -------------------------------------------- */

  /** @override */
  _onUpdate(data, options, userId) {
    super._onUpdate(data, options, userId);

    // Get the changed attributes
    const keys = Object.keys(data).filter(k => k !== "_id");
    const changed = new Set(keys);

    // Additional options only apply to Actors which are not synthetic Tokens
    if (this.isToken) return;

    // If the prototype token was changed, expire any cached token images
    if (changed.has("token")) this._tokenImages = null;

    // Update the active TokenDocument instances which represent this Actor
    const tokens = this.getActiveTokens(false, true);
    for ( let t of tokens ) {
      t._onUpdateBaseActor(data, options);
    }

    // If ownership changed for the actor reset token control
    if (changed.has("permission") && tokens.length) {
      canvas.tokens.releaseAll();
      canvas.tokens.cycleTokens(true, true);
    }
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _onCreateEmbeddedDocuments(embeddedName, ...args) {
    super._onCreateEmbeddedDocuments(embeddedName, ...args);
    this._onEmbeddedDocumentChange(embeddedName);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _onUpdateEmbeddedDocuments(embeddedName, ...args) {
    super._onUpdateEmbeddedDocuments(embeddedName, ...args);
    this._onEmbeddedDocumentChange(embeddedName);
  }

	/* -------------------------------------------- */

  /** @inheritdoc */
  _onDeleteEmbeddedDocuments(embeddedName, ...args) {
    super._onDeleteEmbeddedDocuments(embeddedName, ...args);
    this._onEmbeddedDocumentChange(embeddedName);
  }

	/* -------------------------------------------- */

  /**
   * Perform various actions on active tokens if embedded documents were changed.
   * @param {string} embeddedName  The type of embedded document that was modified.
   * @private
   */
  _onEmbeddedDocumentChange(embeddedName) {
    const tokens = this.getActiveTokens();
    let refreshCombat = false;
    if ( this.isToken ) refreshCombat = this.token.inCombat;
    else if ( game.combat?.getCombatantByActor(this.id) ) refreshCombat = true;
    for ( let token of tokens ) {
      if ( token.hasActiveHUD ) canvas.tokens.hud.render();
      const linked = token.data.actorLink;
      if ( linked && token.document.parent.isView ) {
        token.drawEffects();
        token.drawBars();
      } else if ( !linked ) {
        // Special case handling for unlinked token actors that have not overridden their base actors' embedded
        // collections yet.
        const collectionName = this.constructor.metadata.embedded[embeddedName].collectionName;
        if ( !token.data.actorData?.[collectionName] ) token.document._actor = null;
      }
    }
    if ( refreshCombat ) ui.combat.render();
  }
}
