/**
 * The client-side AmbientLight document which extends the common BaseAmbientLight model.
 * Each AmbientLight document contains AmbientLightData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseAmbientLight
 * @extends ClientDocumentMixin
 *
 * @see {@link data.AmbientLightData}             The AmbientLight data schema
 * @see {@link documents.Scene}                   The Scene document type which contains AmbientLight embedded documents
 * @see {@link applications.AmbientLightConfig}   The AmbientLight configuration application
 *
 * @param {data.AmbientLightData} [data={}]       Initial data provided to construct the AmbientLight document
 * @param {Scene} parent                The parent Scene document to which this AmbientLight belongs
 */
class AmbientLightDocument extends CanvasDocumentMixin(foundry.documents.BaseAmbientLight) {

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * Is this ambient light source global in nature?
   * @type {boolean}
   */
  get isGlobal() {
    return !this.data.walls;
  }
}
