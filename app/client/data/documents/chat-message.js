/**
 * The client-side ChatMessage document which extends the common BaseChatMessage model.
 * Each ChatMessage document contains ChatMessageData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.Document
 * @extends abstract.BaseChatMessage
 * @extends ClientDocumentMixin
 *
 * @see {@link data.ChatMessageData}              The ChatMessage data schema
 * @see {@link documents.Messages}                The world-level collection of ChatMessage documents
 *
 * @param {data.ChatMessageData} [data={}]        Initial data provided to construct the ChatMessage document
 */
class ChatMessage extends ClientDocumentMixin(foundry.documents.BaseChatMessage) {

  /**
   * The cached Roll instance that this message contains, if any
   * @type {Roll|null}
   * @private
   */
  _roll = null;

  /**
   * Is the display of the roll in this message collapsed (false) or expanded (true)
   * @type {boolean}
   * @private
   */
  _rollExpanded = false;

  /**
   * Is this ChatMessage currently displayed in the sidebar ChatLog?
   * @type {boolean}
   */
  logged = false;

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * Return the recommended String alias for this message.
   * The alias could be a Token name in the case of in-character messages or dice rolls.
   * Alternatively it could be a User name in the case of OOC chat or whispers.
   * @type {string}
   */
  get alias() {
    const speaker = this.data.speaker;
    if ( speaker.alias ) return speaker.alias;
    else if ( game.actors.has(speaker.actor) ) return game.actors.get(speaker.actor).name;
    else return this.user?.name ?? "";
  }

  /* -------------------------------------------- */

  /**
   * Is the current User the author of this message?
   * @type {boolean}
   */
  get isAuthor() {
    return game.user.id === this.data.user;
  }

  /* -------------------------------------------- */

  /**
   * Return whether the content of the message is visible to the current user.
   * For certain dice rolls, for example, the message itself may be visible while the content of that message is not.
   * @type {boolean}
   */
  get isContentVisible() {
    if ( this.isRoll ) {
      const whisper = this.data.whisper || [];
      const isBlind = whisper.length && this.data.blind;
      if ( whisper.length ) return whisper.includes(game.user.id) || (this.isAuthor && !isBlind);
      return true;
    }
    else return this.visible;
  }

  /* -------------------------------------------- */

  /**
   * Test whether the chat message contains a dice roll
   * @type {boolean}
   */
  get isRoll() {
    return this.data.type === CONST.CHAT_MESSAGE_TYPES.ROLL;
  }

  /* -------------------------------------------- */

  /**
   * Return the Roll instance contained in this chat message, if one is present
   * @type {Roll|null}
   */
  get roll() {
    if ( this._roll === null ) {
      try {
        if ( !this.data.roll ) return null;
        this._roll = Roll.fromJSON(this.data.roll);
      } catch(err) {
        this._roll = null;
        Hooks.onError("ChatMessage#roll", err, {roll: this.data.roll, log: "error"});
      }
    }
    return this._roll;
  }

  /* -------------------------------------------- */

  /**
   * Return whether the ChatMessage is visible to the current User.
   * Messages may not be visible if they are private whispers.
   * @type {boolean}
   */
  get visible() {
    if ( this.data.whisper.length ) {
      if ( this.data.type === CONST.CHAT_MESSAGE_TYPES.ROLL ) return true;
      return (this.data.user === game.user.id) || (this.data.whisper.indexOf(game.user.id) !== -1);
    }
    return true;
  }

  /* -------------------------------------------- */

  /**
   * The User who created the chat message.
   * @type {User}
   */
  get user() {
    return game.users.get(this.data.user);
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /** @inheritdoc */
  prepareData() {
    super.prepareData();
    const actor = this.constructor.getSpeakerActor(this.data.speaker) || this.user?.character;
    const rollData = actor ? actor.getRollData() : {};
    this.data.update({"content": TextEditor.enrichHTML(this.data.content, {rollData})});
  }

  /* -------------------------------------------- */

  /**
   * Transform a provided object of ChatMessage data by applying a certain rollMode to the data object.
   * @param {object} chatData     The object of ChatMessage data prior to applying a rollMode preference
   * @param {string} rollMode     The rollMode preference to apply to this message data
   * @returns {object}            The modified ChatMessage data with rollMode preferences applied
   */
  static applyRollMode(chatData, rollMode) {
    if ( rollMode === "roll" ) rollMode = game.settings.get("core", "rollMode");
    if ( ["gmroll", "blindroll"].includes(rollMode) ) {
      chatData.whisper = ChatMessage.getWhisperRecipients("GM").map(u => u.id);
    }
    else if ( rollMode === "selfroll" ) chatData.whisper = [game.user.id];
    else if ( rollMode === "publicroll" ) chatData.whisper = [];
    chatData.blind = rollMode === "blindroll";
    return chatData;
  }

  /* -------------------------------------------- */

  /**
   * Update the data of a ChatMessage instance to apply a requested rollMode
   * @param {string} rollMode     The rollMode preference to apply to this message data
   */
  applyRollMode(rollMode) {
    const updates = {};
    this.constructor.applyRollMode(updates, rollMode);
    this.data.update(updates);
  }

  /* -------------------------------------------- */

  /**
   * Attempt to determine who is the speaking character (and token) for a certain Chat Message
   * First assume that the currently controlled Token is the speaker
   *
   * @param {Scene} [scene]         The Scene in which the speaker resides
   * @param {Actor} [actor]         The Actor whom is speaking
   * @param {TokenDocument} [token] The Token whom is speaking
   * @param {string} [alias]        The name of the speaker to display
   *
   * @returns {Object}  The identified speaker data
   */
  static getSpeaker({scene, actor, token, alias}={}) {

    // CASE 1 - A Token is explicitly provided
    const hasToken = (token instanceof Token) || (token instanceof TokenDocument);
    if ( hasToken ) return this._getSpeakerFromToken({token, alias});
    const hasActor = actor instanceof Actor;
    if ( hasActor && actor.isToken ) return this._getSpeakerFromToken({token: actor.token, alias});

    // CASE 2 - An Actor is explicitly provided
    if ( hasActor ) {
      alias = alias || actor.name;
      const tokens = actor.getActiveTokens();
      if ( !tokens.length ) return this._getSpeakerFromActor({scene, actor, alias});
      const controlled = tokens.filter(t => t._controlled);
      token = controlled.length ? controlled.shift() : tokens.shift();
      return this._getSpeakerFromToken({token: token.document, alias});
    }

    // CASE 3 - Not the viewed Scene
    else if ( ( scene instanceof Scene ) && !scene.isView ) {
      const char = game.user.character;
      if ( char ) return this._getSpeakerFromActor({scene, actor: char, alias});
      return this._getSpeakerFromUser({scene, user: game.user, alias});
    }

    // CASE 4 - Infer from controlled tokens
    if ( canvas.ready ) {
      let controlled = canvas.tokens.controlled;
      if (controlled.length) return this._getSpeakerFromToken({token: controlled.shift().document, alias});
    }

    // CASE 5 - Infer from impersonated Actor
    const char = game.user.character;
    if ( char ) {
      const tokens = char.getActiveTokens(false, true);
      if ( tokens.length ) return this._getSpeakerFromToken({token: tokens.shift(), alias});
      return this._getSpeakerFromActor({actor: char, alias});
    }

    // CASE 6 - From the alias and User
    return this._getSpeakerFromUser({scene, user: game.user, alias});
  }

  /* -------------------------------------------- */

  /**
   * A helper to prepare the speaker object based on a target TokenDocument
   *
   * @param {TokenDocument} [token]     The TokenDocument of the speaker
   * @param {string} [alias]            The name of the speaker to display
   * @returns {Object}                  The identified speaker data
   * @private
   */
  static _getSpeakerFromToken({token, alias}) {
    if ( token instanceof Token ) {
      token = token.document;
      console.warn("You are passing a Token instance to _getSpeakerFromToken which now expects a TokenDocument instance instead");
    }

    return {
      scene: token.parent?.id || null,
      token: token.id,
      actor: token.actor?.id || null,
      alias: alias || token.name
    };
  }

  /* -------------------------------------------- */

  /**
   * A helper to prepare the speaker object based on a target Actor
   *
   * @param {Scene} [scene]             The Scene is which the speaker resides
   * @param {Actor} [actor]             The Actor that is speaking
   * @param {string} [alias]            The name of the speaker to display
   * @returns {Object}                  The identified speaker data
   * @private
   */
  static _getSpeakerFromActor({scene, actor, alias}) {
    return {
      scene: (scene || canvas.scene)?.id || null,
      actor: actor.id,
      token: null,
      alias: alias || actor.name
    }
  }
  /* -------------------------------------------- */

  /**
   * A helper to prepare the speaker object based on a target User
   *
   * @param {Scene} [scene]             The Scene in which the speaker resides
   * @param {User} [user]               The User who is speaking
   * @param {string} [alias]            The name of the speaker to display
   * @returns {Object}                  The identified speaker data
   * @private
   */
  static _getSpeakerFromUser({scene, user, alias}) {
    return {
      scene: (scene || canvas.scene)?.id || null,
      actor: null,
      token: null,
      alias: alias || user.name
    }
  }

  /* -------------------------------------------- */

  /**
   * Obtain an Actor instance which represents the speaker of this message (if any)
   * @param {Object} speaker    The speaker data object
   * @return {Actor|null}
   */
  static getSpeakerActor(speaker) {
    if ( !speaker ) return null;
    let actor = null;

    // Case 1 - Token actor
    if ( speaker.scene && speaker.token ) {
      const scene = game.scenes.get(speaker.scene);
      const token = scene ? scene.tokens.get(speaker.token) : null;
      actor = token?.actor;
    }

    // Case 2 - explicit actor
    if ( speaker.actor && !actor ) {
      actor = game.actors.get(speaker.actor);
    }
    return actor || null;
  }

  /* -------------------------------------------- */

  /**
   * Obtain a data object used to evaluate any dice rolls associated with this particular chat message
   * @return {object}
   */
  getRollData() {
    const actor = this.constructor.getSpeakerActor(this.data.speaker);
    return actor ? actor.getRollData() : {};
  }

  /* -------------------------------------------- */

  /**
   * Given a string whisper target, return an Array of the user IDs which should be targeted for the whisper
   *
   * @param {string} name   The target name of the whisper target
   * @return {User[]}       An array of User instances
   */
  static getWhisperRecipients(name) {

    // Whisper to groups
    if (["GM", "DM"].includes(name.toUpperCase())) {
      return game.users.filter(u => u.isGM);
    }
    else if (name.toLowerCase() === "players") {
      return game.users.players;
    }

    // Whisper to a single person
    const lname = name.toLowerCase();
    let user = game.users.find(u => u.name.toLowerCase() === lname);
    if (user) return [user];
    let actor = game.users.find(a => a.character && a.character.name.toLowerCase() === lname);
    if (actor) return [actor];

    // Otherwise return an empty array
    return [];
  }

  /* -------------------------------------------- */

  /**
   * Render the HTML for the ChatMessage which should be added to the log
   * @return {Promise<jQuery>}
   */
  async getHTML() {

    // Determine some metadata
    const data = this.toObject(false);
    const isWhisper = this.data.whisper.length;

    // Construct message data
    const messageData = {
      message: data,
      user: game.user,
      author: this.user,
      alias: this.alias,
      cssClass: [
        this.data.type === CONST.CHAT_MESSAGE_TYPES.IC ? "ic" : null,
        this.data.type === CONST.CHAT_MESSAGE_TYPES.EMOTE ? "emote" : null,
        isWhisper ? "whisper" : null,
        this.data.blind ? "blind": null
      ].filterJoin(" "),
      isWhisper: this.data.whisper.some(id => id !== game.user.id),
      canDelete: game.user.isGM,  // Only GM users are allowed to have the trash-bin icon in the chat log itself
      whisperTo: this.data.whisper.map(u => {
        let user = game.users.get(u);
        return user ? user.name : null;
      }).filterJoin(", ")
    };

    // Render message data specifically for ROLL type messages
    if ( this.isRoll ) {
      await this._renderRollContent(messageData);
    }

    // Define a border color
    if ( this.data.type === CONST.CHAT_MESSAGE_TYPES.OOC ) {
      messageData.borderColor = this.user.color;
    }

    // Render the chat message
    let html = await renderTemplate(CONFIG.ChatMessage.template, messageData);
    html = $(html);

    // Flag expanded state of dice rolls
    if ( this._rollExpanded ) html.find(".dice-tooltip").addClass("expanded");

    /**
     * A hook event that fires for each ChatMessage which is rendered for addition to the ChatLog.
     * This hook allows for final customization of the message HTML before it is added to the log.
     * @function renderChatMessage
     * @memberof hookEvents
     * @param {ChatMessage} message   The ChatMessage document being rendered
     * @param {jQuery} html           The pending HTML as a jQuery object
     * @param {object} data           The input data provided for template rendering
     */
    Hooks.call("renderChatMessage", this, html, messageData);
    return html;
  }

  /* -------------------------------------------- */

  /**
   * Render the inner HTML content for ROLL type messages.
   * @param {object} messageData      The chat message data used to render the message HTML
   * @returns {Promise}
   * @private
   */
  async _renderRollContent(messageData) {
    const data = messageData.message;

    // Always show a "rolled privately" message if the Roll content is not visible to the current user
    if ( !this.isContentVisible ) {
      data.flavor = game.i18n.format("CHAT.PrivateRollContent", {user: this.user.name});
      data.content =  await this.roll.render({isPrivate: true});
      messageData.isWhisper = false;
      messageData.alias = this.user.name;
    }

    // Determine whether a visible roll message has custom HTML content, otherwise render the Roll to HTML
    else {
      const hasContent = data.content && (Number(data.content) !== this.roll.total);
      if ( !hasContent ) data.content = await this.roll.render({isPrivate: false});
    }
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @override */
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);
    if ( this.isRoll ) {
      const rollMode = options.rollMode || data.rollMode || game.settings.get("core", "rollMode");
      if ( rollMode ) this.applyRollMode(rollMode);
    }
  }

  /* -------------------------------------------- */

  /** @override */
	_onCreate(data, options, userId) {
	  super._onCreate(data, options, userId);
	  if ( options.temporary ) return;
	  ui.chat.postOne(this, true);
    if ( options.chatBubble && canvas.ready ) {
      this.collection.sayBubble(this);
    }
	}

  /* -------------------------------------------- */

  /** @override */
	_onUpdate(data, options, userId) {
	  if ( "roll" in data ) this._roll = null;
	  if ( !this.visible ) ui.chat.deleteMessage(this.id);
    else ui.chat.updateMessage(this);
    super._onUpdate(data, options, userId);
  }

  /* -------------------------------------------- */

  /** @override */
	_onDelete(options, userId) {
    ui.chat.deleteMessage(this.id, options);
	  super._onDelete(options, userId);
  }

  /* -------------------------------------------- */
  /*  Importing and Exporting                     */
  /* -------------------------------------------- */

  /**
   * Export the content of the chat message into a standardized log format
   * @return {string}
   */
  export() {
    let content = [];

    // Handle Roll content
    if ( this.isRoll ) {
      let r = this.roll;
      if ( this.data.content && (this.data.content !== "undefined")) {
        content.push($(`<div>${this.data.content}</div>`).text().trim());
      }
      let flavor = this.data.flavor;
      if ( flavor && flavor !== r.formula ) content.push(flavor);
      content.push(`${r.formula} = ${r.result} = ${r.total}`);
    }

    // Handle HTML content
    else {
      const html = $("<article>").html(this.data["content"].replace(/<\/div>/g, "</div>|n"));
      const text = html.length ? html.text() : this.data["content"];
      const lines = text.replace(/\n/g, "").split("  ").filter(p => p !== "").join(" ");
      content = lines.split("|n").map(l => l.trim());
    }

    // Author and timestamp
    const time = new Date(this.data.timestamp).toLocaleDateString('en-US', {
      hour: "numeric",
      minute: "numeric",
      second: "numeric"
    });

    // Format logged result
    return `[${time}] ${this.alias}\n${content.filterJoin("\n")}`;
  }
}
