/**
 * The client-side Item document which extends the common BaseItem model.
 * Each Item document contains ItemData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseItem
 * @extends ClientDocumentMixin
 *
 * @see {@link data.ItemData}              The Item data schema
 * @see {@link documents.Items}            The world-level collection of Item documents
 * @see {@link applications.ItemSheet}     The Item configuration application
 *
 * @param {data.ItemData} data              Initial data provided to construct the Item document
 * @param {object} [context={}]             The document context, see {@link foundry.abstract.Document}
 */
class Item extends ClientDocumentMixin(foundry.documents.BaseItem) {

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * A convenience alias of Item#parent which is more semantically intuitive
   * @type {Actor|null}
   */
  get actor() {
    return this.parent instanceof Actor ? this.parent : null;
  }

  /* -------------------------------------------- */

  /**
   * A convenience reference to the image path (data.img) used to represent this Item
   * @type {string}
   */
  get img() {
    return this.data.img;
  }

  /* -------------------------------------------- */

  /**
   * Provide a thumbnail image path used to represent this document.
   * @type {string}
   */
  get thumbnail() {
    return this.data.img;
  }

  /* -------------------------------------------- */

  /**
   * A convenience alias of Item#isEmbedded which is preserves legacy support
   * @type {boolean}
   */
  get isOwned() {
    return this.isEmbedded;
  }

  /* -------------------------------------------- */

  /**
   * Return an array of the Active Effect instances which originated from this Item.
   * The returned instances are the ActiveEffect instances which exist on the Item itself.
   * @type {ActiveEffect[]}
   */
  get transferredEffects() {
    return this.effects.filter(e => e.data.transfer === true);
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Prepare a data object which defines the data schema used by dice roll commands against this Item
   * @return {object}
   */
  getRollData() {
    return this.data.data;
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @inheritdoc */
  static async _onCreateDocuments(items, context) {
    if ( !(context.parent instanceof Actor) ) return;
    const toCreate = [];
    for ( let item of items ) {
      for ( let e of item.effects ) {
        if ( !e.data.transfer ) continue;
        const effectData = e.toJSON();
        effectData.origin = item.uuid;
        toCreate.push(effectData);
      }
    }
    if ( !toCreate.length ) return [];
    const cls = getDocumentClass("ActiveEffect");
    return cls.createDocuments(toCreate, context);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static async _onDeleteDocuments(items, context) {
    if ( !(context.parent instanceof Actor) ) return;
    const actor = context.parent;
    const deletedUUIDs = new Set(items.map(i => {
      if ( actor.isToken ) return i.uuid.split(".").slice(-2).join(".");
      return i.uuid;
    }));
    const toDelete = [];
    for ( const e of actor.effects ) {
      let origin = e.data.origin || "";
      if ( actor.isToken ) origin = origin.split(".").slice(-2).join(".");
      if ( deletedUUIDs.has(origin) ) toDelete.push(e.id);
    }
    if ( !toDelete.length ) return [];
    const cls = getDocumentClass("ActiveEffect");
    return cls.deleteDocuments(toDelete, context);
  }
}
