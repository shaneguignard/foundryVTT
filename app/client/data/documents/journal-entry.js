/**
 * The client-side JournalEntry document which extends the common BaseJournalEntry model.
 * Each JournalEntry document contains JournalEntryData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.BaseJournalEntry
 * @extends ClientDocumentMixin
 *
 * @see {@link data.JournalEntryData}              The JournalEntry data schema
 * @see {@link documents.Journal}                  The world-level collection of JournalEntry documents
 * @see {@link applications.JournalSheet}          The JournalEntry configuration application
 *
 * @param {data.JournalEntryData} [data={}]       Initial data provided to construct the JournalEntry document
 */
class JournalEntry extends ClientDocumentMixin(foundry.documents.BaseJournalEntry) {

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * A boolean indicator for whether or not the JournalEntry is visible to the current user in the directory sidebar
   * @type {boolean}
   */
  get visible() {
    return this.testUserPermission(game.user, "OBSERVER");
  }

	/* -------------------------------------------- */

  /**
   * Return a reference to the Note instance for this Journal Entry in the current Scene, if any.
   * If multiple notes are placed for this Journal Entry, only the first will be returned.
   * @type {Note|null}
   */
	get sceneNote() {
	  if ( !canvas.ready ) return null;
	  return canvas.notes.placeables.find(n => n.data.entryId === this.id) || null;
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Show the JournalEntry to connected players.
   * By default the entry will only be shown to players who have permission to observe it.
   * If the parameter force is passed, the entry will be shown to all players regardless of normal permission.
   *
   * @param {string} mode     Which JournalEntry mode to display? Default is text.
   * @param {boolean} force   Display the entry to all players regardless of normal permissions
   * @return {Promise<JournalEntry>}  A Promise that resolves back to the shown entry once the request is processed
   */
  async show(mode="text", force=false) {
    if ( !this.isOwner ) throw new Error("You may only request to show Journal Entries which you own.");
    return new Promise((resolve) => {
      game.socket.emit("showEntry", this.uuid, mode, force, entry => {
        Journal._showEntry(this.uuid, mode, true);
        ui.notifications.info(game.i18n.format("JOURNAL.ActionShowSuccess", {
          mode: mode,
          title: this.name,
          which: force ? "all" : "authorized"
        }));
        return resolve(this);
      });
    });
  }

  /* -------------------------------------------- */

  /**
   * If the JournalEntry has a pinned note on the canvas, this method will animate to that note
   * The note will also be highlighted as if hovered upon by the mouse
   * @param {object} [options={}]         Options which modify the pan operation
   * @param {number} [scale=1.5]          The resulting zoom level
   * @param {number} [duration=250]       The speed of the pan animation in milliseconds
   * @return {Promise<void>}              A Promise which resolves once the pan animation has concluded
   */
  panToNote({scale=1.5, duration=250}={}) {
    const note = this.sceneNote;
    if ( !note ) return;
    if ( note.visible && !canvas.notes._active ) canvas.notes.activate();
    return canvas.animatePan({x: note.x, y: note.y, scale, duration}).then(() => {
      if ( canvas.notes._hover ) canvas.notes._hover._onMouseOut(new Event("mouseout"));
      note._onMouseOver(new Event("mouseover"));
    });
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @override */
	_onUpdate(data, options, userId) {
	  super._onUpdate(data, options, userId);
	  if ( !canvas.ready ) return;
    if ( ["name", "permission"].some(k => k in data) ) {
      canvas.notes.placeables.filter(n => n.data.entryId === this.id).forEach(n => n.draw());
    }
  }

  /* -------------------------------------------- */

  /** @override */
  _onDelete(options, userId) {
    super._onDelete(options, userId);
	  if ( !canvas.ready ) return;
    for ( let n of canvas.notes.placeables ) {
      if ( n.data.entryId === this.id ) {
        n.draw();
      }
    }
  }
}
