/**
 * The client-side Macro document which extends the common BaseMacro model.
 * Each Macro document contains MacroData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.Document
 * @extends abstract.BaseMacro
 * @extends ClientDocumentMixin
 *
 * @see {@link data.MacroData}              The Macro data schema
 * @see {@link documents.Macros}            The world-level collection of Macro documents
 * @see {@link applications.MacroConfig}    The Macro configuration application
 *
 * @param {data.MacroData} [data={}]        Initial data provided to construct the Macro document
 */
class Macro extends ClientDocumentMixin(foundry.documents.BaseMacro) {

  /* -------------------------------------------- */
  /*  Properties                                  */
  /* -------------------------------------------- */

  /**
   * Is the current User the author of this macro?
   * @type {boolean}
   */
  get isAuthor() {
    return game.user.id === this.data.author;
  }

  /* -------------------------------------------- */

  /**
   * Test whether the current user is capable of executing a Macro script
   * @type {boolean}
   */
  get canExecute() {
    if ( !this.testUserPermission(game.user, "LIMITED") ) return false;
    return this.data.type === "script" ? game.user.can("MACRO_SCRIPT") : true;
  }

  /* -------------------------------------------- */

  /**
   * Provide a thumbnail image path used to represent this document.
   * @type {string}
   */
  get thumbnail() {
    return this.data.img;
  }

  /* -------------------------------------------- */
  /*  Methods                                     */
  /* -------------------------------------------- */

  /**
   * Execute the Macro command.
   * @param {object} [scope={}]     Provide some additional scope configuration for the Macro
   * @param {Actor} [scope.actor]   An Actor who is the protagonist of the executed action
   * @param {Token} [scope.token]   A Token which is the protagonist of the executed action
   */
  execute({actor, token}={}) {
    if ( !this.canExecute ) {
      return ui.notifications.warn(`You do not have permission to execute Macro "${this.name}".`);
    }
    switch ( this.data.type ) {
      case "chat":
        return this._executeChat({actor, token});
      case "script":
        return this._executeScript({actor, token});
    }
  }

  /* -------------------------------------------- */

  /**
   * Execute the command as a chat macro.
   * Chat macros simulate the process of the command being entered into the Chat Log input textarea.
   * @private
   */
  _executeChat({actor, token}={}) {
    ui.chat.processMessage(this.data.command).catch(err => {
      Hooks.onError("Macro#_executeChat", err, {
        msg: "There was an error in your chat message syntax.",
        log: "error",
        notify: "error",
        command: this.data.command
      });
    });
  }

  /* -------------------------------------------- */

  /**
   * Execute the command as a script macro.
   * Script Macros are wrapped in an async IIFE to allow the use of asynchronous commands and await statements.
   * @private
   */
  _executeScript({actor, token}={}) {

    // Add variables to the evaluation scope
    const speaker = ChatMessage.implementation.getSpeaker();
    const character = game.user.character;
    actor = actor || game.actors.get(speaker.actor);
    token = token || (canvas.ready ? canvas.tokens.get(speaker.token) : null);

    // Attempt script execution
    const body = `(async () => {
      ${this.data.command}
    })()`;
    const fn = Function("speaker", "actor", "token", "character", body);
    try {
      fn.call(this, speaker, actor, token, character);
    } catch (err) {
      ui.notifications.error(`There was an error in your macro syntax. See the console (F12) for details`);
      console.error(err);
    }
  }
}
