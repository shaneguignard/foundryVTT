/**
 * The client-side Setting document which extends the common BaseSetting model.
 * Each Setting document contains SettingData which defines its data schema.
 *
 * @extends abstract.Document
 * @extends abstract.Document
 * @extends abstract.BaseSetting
 * @extends ClientDocumentMixin
 *
 * @see {@link data.SettingData}              The Setting data schema
 * @see {@link documents.WorldSettings}       The world-level collection of Setting documents
 *
 * @param {data.SettingData} [data={}]        Initial data provided to construct the Setting document
 */
class Setting extends ClientDocumentMixin(foundry.documents.BaseSetting) {

  /**
   * A convenient alias to the key attribute of the setting data
   * @type {string}
   */
  get key() {
    return this.data.key;
  }

  /**
   * A convenient alias to the parsed value attribute of the setting data.
   * @type {*}
   */
  get value() {
    return this.data.value ? JSON.parse(this.data.value) : null;
  }

  /* -------------------------------------------- */
  /*  Event Handlers                              */
  /* -------------------------------------------- */

  /** @override */
  _onCreate(data, options, user) {
    super._onCreate(data, options, user);
    const config = game.settings.settings.get(this.key);
    if ( config.onChange instanceof Function ) config.onChange(this.value);
  }

  /* -------------------------------------------- */

  /** @override */
  _onUpdate(changed, options, user) {
    super._onUpdate(changed, options, user);
    const config = game.settings.settings.get(this.key);
    if ( config.onChange instanceof Function ) config.onChange(this.value);
  }
}
