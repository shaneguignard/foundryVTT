/**
 * A Loader class which helps with loading video and image textures
 */
class TextureLoader {

  /**
   * The cached mapping of textures
   * @type {Map<string,{tex: PIXI.BaseTexture, time: number}>}
   */
  cache = new Map();

  /**
   * The duration in milliseconds for which a texture will remain cached
   * @type {number}
   */
  static CACHE_TTL = 1000 * 60 * 15;

  /* -------------------------------------------- */

  /**
   * Load all the textures which are required for a particular Scene
   * @param {Scene} scene           The Scene to load
   * @param {object} [options={}]   Additional options that configure texture loading
   * @param {boolean} [options.expireCache=true]  Destroy other expired textures
   * @return {Promise<void[]>}
   */
  static loadSceneTextures(scene, {expireCache=true}={}) {
    const sd = scene.data;
    let toLoad = [];

    // Scene background and foreground textures
    if ( sd.img ) toLoad.push(sd.img);
    if ( sd.foreground ) toLoad.push(sd.foreground);

    // Tiles
    toLoad = toLoad.concat(sd.tiles.reduce((arr, t) => {
      if ( t.data.img ) arr.push(t.data.img);
      return arr;
    }, []));

    // Tokens
    toLoad = toLoad.concat(sd.tokens.reduce((arr, t) => {
      if ( t.data.img ) arr.push(t.data.img);
      return arr;
    }, []));

    // Control Icons
    toLoad = toLoad.concat(Object.values(CONFIG.controlIcons)).concat(CONFIG.statusEffects.map(e => e.icon ?? e));

    // Load files
    const showName = scene.active || scene.visible;
    const loadName = showName ? (scene.data.navName || scene.data.name) : "...";
    return this.loader.load(toLoad, {
      message: game.i18n.format("SCENES.Loading", {name: loadName}),
      expireCache: expireCache
    });
  }

  /* -------------------------------------------- */

  /**
   * Load an Array of provided source URL paths
   * @param {string[]} sources      The source URLs to load
   * @param {object} [options={}]   Additional options which modify loading
   * @param {string} [options.message]              The status message to display in the load bar
   * @param {boolean} [options.expireCache=false]   Expire other cached textures?
   * @return {Promise<void[]>}              A Promise which resolves once all textures are loaded
   */
  async load(sources, {message, expireCache=false}={}) {
    const seen = new Set();
    const promises = [];
    const progress = {message: message, loaded: 0, failed: 0, total: 0, pct: 0};
    for ( const src of sources ) {

      // De-dupe load requests
      if ( seen.has(src) ) continue;
      seen.add(src);

      // Load from cache
      const cached = this.getCache(src);
      if ( cached?.baseTexture?.valid ) return cached;

      // Load uncached textures
      const promise = VideoHelper.hasVideoExtension(src) ? this.loadVideoTexture(src) : this.loadImageTexture(src);
      promises.push(promise.then(() => this._onProgress(src, progress)).catch(err => this._onError(src, progress, err)));
    }
    progress.total = promises.length;

    // Expire any cached textures
    if ( expireCache ) this.expireCache();

    // Load all media
    return Promise.all(promises);
  }

  /* -------------------------------------------- */

  /**
   * Load a single texture on-demand from a given source URL path
   * @param {string} src                    The source texture path to load
   * @return {Promise<PIXI.BaseTexture>}    The loaded texture object
   */
  async loadTexture(src) {
    let bt = this.getCache(src);
    if ( bt ) return bt;
    return VideoHelper.hasVideoExtension(src) ? this.loadVideoTexture(src) : this.loadImageTexture(src);
  }

  /* -------------------------------------------- */

  /**
   * Log texture loading progress in the console and in the Scene loading bar
   * @private
   */
  _onProgress(src, progress) {
    progress.loaded++;
    progress.pct = Math.round((progress.loaded + progress.failed) * 100 / progress.total);
    SceneNavigation.displayProgressBar({label: progress.message, pct: progress.pct});
    console.log(`${vtt} | Loaded ${src} (${progress.pct}%)`);
  }

  /* -------------------------------------------- */

  /**
   * Log failed texture loading
   * @private
   */
  _onError(src, progress, error) {
    progress.failed++;
    progress.pct = Math.round((progress.loaded + progress.failed) * 100 / progress.total);
    SceneNavigation.displayProgressBar({label: progress.message, pct: progress.pct});
    console.warn(`${vtt} | Loading failed for ${src} (${progress.pct}%): ${error.message}`);
  }

  /* -------------------------------------------- */

  /**
   * Load an image texture from a provided source url
   * @param {string} src
   * @return {Promise<PIXI.BaseTexture>}
   */
  async loadImageTexture(src) {
    const blob = await this._fetchResource(src);

    // Create the Image element
    const img = new Image();
    img.decoding = "async";
    img.loading = "eager";

    // Wait for the image to load
    return new Promise((resolve, reject) => {

      // Create the texture on successful load
      img.onload = () => {
        URL.revokeObjectURL(img.src);
        img.height = img.naturalHeight;
        img.width = img.naturalWidth;
        const tex = PIXI.BaseTexture.from(img);
        this.setCache(src, tex);
        resolve(tex);
      };

      // Handle errors for valid URLs due to CORS
      img.onerror = err => {
        URL.revokeObjectURL(img.src);
        reject(err);
      }
      img.src = URL.createObjectURL(blob);
    });
  }

  /* --------------------------------------------- */

  /**
   * Use the Fetch API to retrieve a resource and return a Blob instance for it.
   * @param {string} src
   * @param {object} [options]                   Options to configure the loading behaviour.
   * @param {boolean} [options.bustCache=false]  Append a cache-busting query parameter to the request.
   * @return {Promise<Blob>}
   * @private
   */
  async _fetchResource(src, {bustCache=false}={}) {
    const fail = `Failed to load texture ${src}`;
    const req = bustCache ? this._getCacheBustURL(src) : src;
    if ( !req ) throw new Error(`${fail}: Invalid URL`);

    let res;
    try {
      res = await fetch(req, {mode: "cors", credentials: "same-origin"});
    } catch(err) {
      // We may have run into https://bugs.chromium.org/p/chromium/issues/detail?id=409090 so we attempt to bust the
      // cache ourselves.
      if ( !bustCache ) return this._fetchResource(src, {bustCache: true});
      throw new Error(`${fail}: CORS failure`);
    }

    if ( !res.ok ) throw new Error(`${fail}: Server responded with ${res.status}`);
    return res.blob();
  }

  /* -------------------------------------------- */

  /**
   * Return a URL with a cache-busting query parameter appended.
   * @param {string} src        The source URL being attempted
   * @return {string|boolean}   The new URL, or false on a failure.
   * @private
   */
  _getCacheBustURL(src) {
    let url;
    try {
      url = new URL(src);
    } catch(err) {
      return false;
    }

    // Skip same-origin resources
    if ( url.origin === window.location.origin ) return false;

    url.searchParams.append('cors-retry', Date.now().toString());
    return url.href;
  }

  /* -------------------------------------------- */

  /**
   * Load a video texture from a provided source url
   * @param {string} src
   * @return {Promise<PIXI.BaseTexture>}
   */
  async loadVideoTexture(src) {
    if ( !VideoHelper.hasVideoExtension(src) ) {
      throw new Error(`${src} is not a valid video texture`);
    }
    const blob = await this._fetchResource(src);

    // Create a Video element
    const video = document.createElement("VIDEO");
    video.preload = "auto";
    video.autoplay = false;
    video.crossOrigin = "anonymous";
    video.src = URL.createObjectURL(blob);

    // Begin loading and resolve or reject
    return new Promise((resolve, reject) => {
      video.oncanplay = () => {
        video.height = video.videoHeight;
        video.width = video.videoWidth;
        const tex = PIXI.BaseTexture.from(video, {resourceOptions: {autoPlay: false}});
        this.setCache(src, tex);
        video.oncanplay = null;
        resolve(tex);
      };
      video.onerror = err => {
        URL.revokeObjectURL(video.src);
        reject(err);
      };
      video.load();
    });
  }

  /* -------------------------------------------- */
  /*  Cache Controls                              */
  /* -------------------------------------------- */

  /**
   * Add an image url to the texture cache
   * @param {string} src              The source URL
   * @param {PIXI.BaseTexture} tex    The loaded base texture
   */
  setCache(src, tex) {
    this.cache.set(src, {
      tex: tex,
      time: Date.now()
    });
  }

  /* -------------------------------------------- */

  /**
   * Retrieve a texture from the texture cache
   * @param {string} src          The source URL
   * @returns {PIXI.BaseTexture}  The cached texture, or undefined
   */
  getCache(src) {
    const val = this.cache.get(src);
    if ( !val || val?.tex.destroyed ) return undefined;
    val.time = Date.now();
    return val?.tex;
  }

  /* -------------------------------------------- */

  /**
   * Expire (and destroy) textures from the cache which have not been used for more than CACHE_TTL milliseconds.
   */
  expireCache() {
    const t = Date.now();
    for ( let [key, obj] of this.cache.entries() ) {
      if ( (t - obj.time) > TextureLoader.CACHE_TTL ) {
        console.log(`Expiring ${key}`);
        URL.revokeObjectURL(obj.tex.resource.source?.src);
        obj.tex.destroy(true);
        this.cache.delete(key);
      }
    }
  }
}

/**
 * A global reference to the singleton texture loader
 * @type {TextureLoader}
 */
TextureLoader.loader = new TextureLoader();


/* -------------------------------------------- */


/**
 * Test whether a file source exists by performing a HEAD request against it
 * @param {string} src          The source URL or path to test
 * @return {Promise<boolean>}   Does the file exist at the provided url?
 */
async function srcExists(src) {
  return fetchWithTimeout(src, { method: 'HEAD' }).then(resp => {
    return resp.status < 400;
  }).catch(err => false);
}


/* -------------------------------------------- */


/**
 * Get a single texture from the cache
 * @param {string} src
 * @return {PIXI.Texture}
 */
function getTexture(src) {
  let baseTexture = TextureLoader.loader.getCache(src);
  if ( !baseTexture?.valid ) return null;
  return new PIXI.Texture(baseTexture);
}


/* -------------------------------------------- */


/**
 * Load a single texture and return a Promise which resolves once the texture is ready to use
 * @param {string} src                The requested texture source
 * @param {object} [options]          Additional options which modify texture loading
 * @param {string} [fallback]             A fallback texture to use if the requested source is unavailable or invalid
 * @return {PIXI.Texture|null}        The loaded Texture, or null if loading failed with no fallback
 */
async function loadTexture(src, {fallback}={}) {
  let bt = null;
  try {
    bt = await TextureLoader.loader.loadTexture(src);
    if ( !bt?.valid ) throw new Error(`Invalid BaseTexture ${src}`);
  }
  catch(err) {
    err.message = `The requested texture ${src} could not be loaded: ${err.message}`;
    console.error(err);
    if ( fallback ) return loadTexture(fallback);
    return null;
  }
  return new PIXI.Texture(bt);
}
