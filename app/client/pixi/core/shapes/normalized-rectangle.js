/**
 * A PIXI.Rectangle where the width and height are always positive and the x and y are always the top-left
 * @extends {PIXI.Rectangle}
 */
class NormalizedRectangle extends PIXI.Rectangle {
  constructor(x, y, width, height) {
    x = width > 0 ? x : x + width;
    y = height > 0 ? y : y + height;
    super(x, y, Math.abs(width), Math.abs(height));
  }

  /**
   * Determine whether some other Rectangle intersects with this one.
   * @param {Rectangle} other       Some other rectangle against which to compare
   * @returns {boolean}             Do the rectangles intersect?
   */
  intersects(other) {
    return (other.right >= this.left)
      && (other.left <= this.right)
      && (other.bottom >= this.top)
      && (other.top <= this.bottom);
  }

  /**
   * Generate a new rectangle by rotating this one clockwise about its center by a certain number of radians
   * @param {number} radians        The angle of rotation
   * @returns {NormalizedRectangle} A new rotated rectangle
   */
  rotate(radians) {
    return this.constructor.fromRotation(this.x, this.y, this.width, this.height, radians);
  }

  /**
   * Create normalized rectangular bounds given a rectangle shape and an angle of central rotation.
   * @param {number} x              The top-left x-coordinate of the un-rotated rectangle
   * @param {number} y              The top-left y-coordinate of the un-rotated rectangle
   * @param {number} width          The width of the un-rotated rectangle
   * @param {number} height         The height of the un-rotated rectangle
   * @param {number} radians        The angle of rotation about the center
   * @returns {NormalizedRectangle} The constructed rotated rectangle bounds
   */
  static fromRotation(x, y, width, height, radians) {
    const rh = (height * Math.abs(Math.cos(radians))) + (width * Math.abs(Math.sin(radians)));
    const rw = (height * Math.abs(Math.sin(radians))) + (width * Math.abs(Math.cos(radians)));
    const rx = x + ((width - rw) / 2);
    const ry = y + ((height - rh) / 2);
    return new NormalizedRectangle(rx, ry, rw, rh);
  }
}
