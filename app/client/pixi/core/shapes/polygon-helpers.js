/**
 * An internal data structure for polygon vertices
 * @private
 * @ignore
 */
class PolygonVertex {
  constructor(x, y, {distance, index}={}) {
    this.x = Math.round(x);
    this.y = Math.round(y);
    this.key = (this.x << 16) ^ this.y;
    this._distance = distance;
    this._d2 = undefined;
    this._index = index;
    this._inLimitedAngle = false;

    /**
     * The set of edges which connect to this vertex.
     * This set is initially empty and populated later after vertices are de-duplicated.
     * @type {EdgeSet}
     */
    this.edges = new Set();

    /**
     * The subset of edges which continue clockwise from this vertex.
     * @type {EdgeSet}
     */
    this.cwEdges = new Set();

    /**
     * The subset of edges which continue counter-clockwise from this vertex.
     * @type {EdgeSet}
     */
    this.ccwEdges = new Set();

    /**
     * The maximum restriction type of this vertex
     * @type {number|null}
     */
    this.type = null;
  }

  /**
   * Associate an edge with this vertex.
   * @param {PolygonEdge} edge      The edge being attached
   * @param {number} orientation    The orientation of the edge with respect to the origin
   */
  attachEdge(edge, orientation=0) {
    this.edges.add(edge);
    this.type = Math.max(this.type ?? 0, edge.type);
    if ( orientation <= 0 ) this.cwEdges.add(edge);
    if ( orientation >= 0 ) this.ccwEdges.add(edge);
  }

  /**
   * Is this vertex limited in type?
   * @returns {boolean}
   */
  get isLimited() {
    return this.type === CONST.WALL_SENSE_TYPES.LIMITED;
  }

  /**
   * Is this vertex terminal (at the maximum radius)
   * @returns {boolean}
   */
  get isTerminal() {
    return this._distance === 1;
  }

  /**
   * Does this vertex have a limited edge connected to it?
   * @returns {boolean}
   */
  get hasLimitedEdge() {
    for ( let edge of this.edges ) {
      if ( edge.type === CONST.WALL_SENSE_TYPES.LIMITED ) return true;
    }
    return false;
  }

  /**
   * Is this vertex the same point as some other vertex?
   * @param {PolygonVertex} other   Some other vertex
   * @returns {boolean}             Are they the same point?
   */
  equals(other) {
    return this.key === other.key;
  }

  /**
   * Construct a PolygonVertex instance from some other Point structure.
   * @param {Point} point           The point
   * @param {object} [options]      Additional options that apply to this vertex
   * @returns {PolygonVertex}       The constructed vertex
   */
  static fromPoint(point, options) {
    return new this(point.x, point.y, options);
  }
}

/* -------------------------------------------- */

/**
 * An internal data structure for polygon edges
 * @private
 * @ignore
 */
class PolygonEdge {
  constructor(a, b, type=CONST.WALL_SENSE_TYPES.NORMAL, wall) {
    this.A = new PolygonVertex(a.x, a.y);
    this.B = new PolygonVertex(b.x, b.y);
    this.type = type;
    this.wall = wall;
  }

  /**
   * Is this edge limited in type?
   * @returns {boolean}
   */
  get isLimited() {
    return this.type === CONST.WALL_SENSE_TYPES.LIMITED;
  }

  /**
   * Construct a PolygonEdge instance from a Wall placeable object.
   * @param {Wall|WallDocument} wall  The Wall from which to construct an edge
   * @param {string} type             The type of polygon being constructed
   * @returns {PolygonEdge}
   */
  static fromWall(wall, type) {
    const c = wall.data.c;
    return new this({x: c[0], y: c[1]}, {x: c[2], y: c[3]}, wall.data[type], wall);
  }
}

/* -------------------------------------------- */

/**
 * An object containing the result of a collision test.
 * @private
 * @ignore
 */
class CollisionResult {
  constructor({target=null, collisions=[], cwEdges, ccwEdges, isBehind, isLimited, isRequired, wasLimited}={}) {

    /**
     * The vertex that was the target of this result
     * @type {PolygonVertex}
     */
    this.target = target;

    /**
     * The array of collision points which apply to this result
     * @type {PolygonVertex[]}
     */
    this.collisions = collisions;

    /**
     * The set of edges connected to the target vertex that continue clockwise
     * @type {EdgeSet}
     */
    this.cwEdges = cwEdges || new Set();

    /**
     * The set of edges connected to the target vertex that continue counter-clockwise
     * @type {EdgeSet}
     */
    this.ccwEdges = ccwEdges || new Set();

    /**
     * Is the target vertex for this result behind some closer active edge?
     * @type {boolean}
     */
    this.isBehind = isBehind;

    /**
     * Does the target vertex for this result impose a limited collision?
     * @type {boolean}
     */
    this.isLimited = isLimited;

    /**
     * Is this result required due to a limited angle?
     * @type {boolean}
     */
    this.isRequired = isRequired;

    /**
     * Has the set of collisions for this result encountered a limited edge?
     * @type {boolean}
     */
    this.wasLimited = wasLimited;
  }
}

/* -------------------------------------------- */
/*  DEPRECATED                                  */
/* -------------------------------------------- */

/**
 * A special subclass of PIXI.Point which is used for modeling Wall endpoints.
 * A wall endpoint must have integer coordinates.
 *
 * This was used for the RadialSweepPolygon but can now be deleted once that is
 * @deprecated since v9d2
 * @ignore
 *
 * @extends {PIXI.Point}
 * @property {number} x     The integer x-coordinate
 * @property {number} y     The integer y-coordinate
 */
class WallEndpoint extends PIXI.Point {
  constructor(x, y) {
    x = Math.round(x);
    y = Math.round(y);
    super(x, y);

    /**
     * Express the point as a 32-bit integer with 16 bits allocated to x and 16 bits allocated to y
     * @type {number}
     */
    this.key = this.x | (this.y << 16);
  }

  /**
   * The angle between this point and the polygon origin
   * @type {number}
   */
  angle;

  /**
   * Record the set of walls which connect to this Endpoint
   * @type {Set<Wall>}
   */
  walls = new Set();

  /**
   * Record whether this point is the endpoint of any Wall
   * @type {boolean}
   */
  isEndpoint = false;

  /**
   * Record whether this point is a midpoint of any wall?
   * @type {boolean}
   */
  isMidpoint = false;

  /**
   * Record whether this point is the termination of the Ray
   * @type {boolean}
   */
  isTerminal = false;

  /**
   * Aggregate the maximum of each wall restriction type
   * @type {{light: number, move: number, sound: number, sight: number}}
   */
  types = {
    light: 0,
    move: 0,
    sight: 0,
    sound: 0
  }

  /**
   * An intermediate variable used to store the proportional distance of this point from a SightRay origin
   * @type {number}
   */
  _r;

  /**
   * An intermediate variable used to cache the continuation attributes for a certain point
   * @type {{left: boolean, right: boolean}}
   */
  _c;

  attachWall(wall) {
    this.walls.add(wall);
    this.types.move = Math.max(this.types.move, wall.data.move);
    const senses = ["light", "sight", "sound"];
    for ( let s of senses ) {
      const st = wall.data[s] === 1 ? 10 : wall.data[s]; // TODO: this is ugly, need to make it better
      this.types[s] = Math.max(this.types[s], st);
    }
    return this;
  }

  /**
   * Does this endpoint equal some other endpoint?
   * @param {Point} other     Some other point with x and y coordinates
   * @returns {boolean}       Are the points equal?
   */
  equals(other) {
    return (other.x === this.x) && (other.y === this.y);
  }

  /**
   * Is this point one that provides only limited perspective?
   * @param {string} type   The perspective type being tested
   * @returns {boolean}     Is perspective limited?
   */
  isLimited(type) {
    return this.types[type] === CONST.WALL_SENSE_TYPES.LIMITED;
  }

  /**
   * Encode a x/y coordinate as a 32-bit integer
   * @param {number} x    The x-coordinate
   * @param {number} y    The y-coordinate
   * @returns {number}
   */
  static getKey(x, y) {
    return Math.round(x) | (Math.round(y) << 16);
  }
}
