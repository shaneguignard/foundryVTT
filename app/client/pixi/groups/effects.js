/**
 * A container group which contains visual effects rendered above the primary group.
 * @extends {PIXI.Container}
 */
class EffectsCanvasGroup extends PIXI.Container {
  constructor() {
    super();

    // Create group layers
    this._createLayers();

    // Configure group attributes
    this.sortableChildren = true;
  }

  /**
   * The name of this canvas group
   * @type {string}
   */
  static groupName = "effects"

  /* -------------------------------------------- */

  /**
   * Create the member layers of the scene container
   * @private
   */
  _createLayers() {
    for ( let [name, config] of Object.entries(CONFIG.Canvas.layers) ) {
      if ( config.group !== this.constructor.groupName ) continue;
      const layer = new config.layerClass();
      Object.defineProperty(this, name, { value: layer, writable: false });
      this.addChild(layer);
    }
  }
}
