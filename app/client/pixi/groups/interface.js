/**
 * A container group which displays interface elements rendered above other canvas groups.
 * @extends {PIXI.Container}
 */
class InterfaceCanvasGroup extends PIXI.Container {
  constructor() {
    super();

    // Create group layers
    this._createLayers();

    // Configure group attributes
    this.sortableChildren = true;
  }

  /**
   * The name of this canvas group
   * @type {string}
   */
  static groupName = "interface"

  /* -------------------------------------------- */

  /**
   * Create the member layers of the scene container
   * @private
   */
  _createLayers() {
    for ( let [name, config] of Object.entries(CONFIG.Canvas.layers) ) {
      if ( config.group !== this.constructor.groupName ) continue;
      const layer = new config.layerClass();
      Object.defineProperty(this, name, { value: layer, writable: false });
      this.addChild(layer);
    }
  }
}
