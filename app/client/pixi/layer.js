/**
 * @typedef {Object} CanvasLayerOptions   Options which configure the behavior of a Canvas Layer.
 * @property {string} name                The layer name by which the instance is referenced within the Canvas
 * @property {number} zIndex              The zIndex sorting of this layer relative to other layers
 * @property {boolean} sortActiveTop      Should this layer be sorted to the top when it is active?
 */

/**
 * An abstract pattern for primary layers of the game canvas to implement.
 * @type {PIXI.Container}
 * @abstract
 * @interface
 */
class CanvasLayer extends PIXI.Container {
  constructor() {
    super();

    /**
     * Options for this layer instance.
     * @type {CanvasLayerOptions}
     */
    this.options = this.constructor.layerOptions;

    // Default interactivity
    this.interactive = false;
    this.interactiveChildren = false;
  }

  /**
   * Track whether the canvas layer is currently active for interaction
   * @type {boolean}
   */
  _active = false;

  /* -------------------------------------------- */
  /*  Layer Attributes                            */
  /* -------------------------------------------- */

  /**
   * Customize behaviors of this CanvasLayer by modifying some behaviors at a class level.
   * @type {CanvasLayerOptions}
   */
  static get layerOptions() {
    return {
      name: "",
      zIndex: 0,
      sortActiveTop: false
    }
  }

  /* -------------------------------------------- */

  /**
   * Return a reference to the active instance of this canvas layer
   * @type {CanvasLayer}
   */
  static get instance() {
    return canvas[this.layerOptions.name];
  }

  /* -------------------------------------------- */

  /**
   * The canonical name of the CanvasLayer
   * @type {string}
   */
  get name() {
    return this.constructor.name;
  }

  /* -------------------------------------------- */
  /*  Rendering
  /* -------------------------------------------- */

  /**
   * Draw the canvas layer, rendering its internal components and returning a Promise
   * The Promise resolves to the drawn layer once its contents are successfully rendered.
   * @return {Promise<CanvasLayer>}
   */
  async draw() {
    this.removeChildren();
    const d = canvas.dimensions;
    this.width = d.width;
    this.height = d.height;
    this.hitArea = d.rect;
    this.zIndex = this.getZIndex();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Deconstruct data used in the current layer in preparation to re-draw the canvas
   * @return {Promise<CanvasLayer>}
   */
  async tearDown() {
    this.renderable = false;
    this.removeChildren().forEach(c => c.destroy({children: true}));
    this.renderable = true;
    return this;
  }

  /* -------------------------------------------- */
  /*  Methods
  /* -------------------------------------------- */

  /**
   * Activate the CanvasLayer, deactivating other layers and marking this layer's children as interactive.
   * @return {CanvasLayer}    The layer instance, now activated
   */
  activate() {

    // Set this layer as active
    const wasActive = this._active;
    this._active = true;

    // Deactivate other layers
    for (let name of Object.keys(Canvas.layers) ) {
      const layer = canvas[name];
      if ( layer !== this ) layer.deactivate();
    }
    if ( wasActive ) return this;

    // Assign interactivity for the active layer
    this.zIndex = this.getZIndex();
    this.interactive = false;
    this.interactiveChildren = true;

    // Re-render Scene controls
    if ( ui.controls ) ui.controls.initialize({layer: this.constructor.layerOptions.name});
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Deactivate the CanvasLayer, removing interactivity from its children.
   * @return {CanvasLayer}    The layer instance, now inactive
   */
  deactivate() {
    this._active = false;
    this.interactive = false;
    this.interactiveChildren = false;
    this.zIndex = this.getZIndex();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Get the zIndex that should be used for ordering this layer vertically relative to others in the same Container.
   * @returns {number}
   */
  getZIndex() {
    const options = this.constructor.layerOptions;
    if ( this._active && options.sortActiveTop ) {
      return canvas.layers.reduce((max, l) => {
        if ( l.zIndex > max ) max = l.zIndex;
        return max;
      }, 0);
    }
    return options.zIndex;
  }
}
