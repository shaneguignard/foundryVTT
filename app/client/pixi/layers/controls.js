
/**
 * A CanvasLayer for displaying UI controls which are overlayed on top of other layers.
 *
 * We track three types of events:
 * 1) Cursor movement
 * 2) Ruler measurement
 * 3) Map pings
 */
class ControlsLayer extends CanvasLayer {
  constructor() {
    super();

    /**
     * A container of DoorControl instances
     * @type {PIXI.Container}
     */
    this.doors = this.addChild(new PIXI.Container());

    /**
     * A container of HUD interface elements
     * @type {PIXI.Container}
     */
    this.hud = this.addChild(new PIXI.Container());

    /**
     * A container of cursor interaction elements.
     * Contains cursors, rulers, interaction rectangles, and pings
     * @type {PIXI.Container}
     */
    this.cursors = this.addChild(new PIXI.Container());

    /**
     * Ruler tools, one per connected user
     * @type {PIXI.Container}
     */
    this.rulers = this.addChild(new PIXI.Container());

    /**
     * A graphics instance used for drawing debugging visualization
     * @type {PIXI.Graphics}
     */
    this.debug = this.addChild(new PIXI.Graphics());
  }

  /**
   * The Canvas selection rectangle
   * @type {PIXI.Graphics}
   */
  select;

  /**
   * A mapping of user IDs to Cursor instances for quick access
   * @type {Object<string, Cursor>}
   */
  _cursors = {};

  /**
   * A mapping of user IDs to Ruler instances for quick access
   * @type {Object<string, Ruler>}
   * @private
   */
  _rulers = {};


  /* -------------------------------------------- */

  /** @override */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "controls",
      zIndex: 1000
    });
  }

  /* -------------------------------------------- */
  /*  Properties and Public Methods               */
  /* -------------------------------------------- */

  /**
   * A convenience accessor to the Ruler for the active game user
   * @type {Ruler}
   */
  get ruler() {
    return this.getRulerForUser(game.user.id);
  }

  /* -------------------------------------------- */

  /**
   * Get the Ruler display for a specific User ID
   * @param {string} userId
   * @return {Ruler|null}
   */
  getRulerForUser(userId) {
    return this._rulers[userId] || null;
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /** @override */
  async draw() {

    // Create additional elements
    this.drawCursors();
    this.drawRulers();
    this.select = this.cursors.addChild(new PIXI.Graphics());

    // Adjust scale
    const d = canvas.dimensions;
    this.hitArea = d.rect;
    this.zIndex = this.getZIndex();
    this.interactiveChildren = true;
    return this;
  }

  /* -------------------------------------------- */

  /** @override */
  async tearDown() {
    this.doors.removeChildren();
    this.cursors.removeChildren();
    this.rulers.removeChildren();
    this.hud.removeChildren();
    this.debug.clear();
  }

  /* -------------------------------------------- */

  /**
   * Draw the cursors container
   */
  drawCursors() {
    for ( let u of game.users.filter(u => u.active && !u.isSelf ) ) {
      this.drawCursor(u);
    }
  }

  /* -------------------------------------------- */

  /**
   * Draw Ruler tools
   */
  drawRulers() {
    for (let u of game.users) {
      let ruler = new Ruler(u);
      this._rulers[u.id] = this.rulers.addChild(ruler);
    }
  }

  /* -------------------------------------------- */

  /**
   * Draw the select rectangle given an event originated within the base canvas layer
   * @param {Object} coords   The rectangle coordinates of the form {x, y, width, height}
   */
  drawSelect({x, y, width, height}) {
    const s = this.select.clear();
    s.lineStyle(3, 0xFF9829, 0.9).drawRect(x, y, width, height);
  }

  /* -------------------------------------------- */

  /** @override */
  deactivate() {
    super.deactivate();
    this.visible = true;
    this.interactiveChildren = true;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * Handle mousemove events on the game canvas to broadcast activity of the user's cursor position
   * @param {PIXI.InteractionEvent} event
   */
  _onMouseMove(event) {
    const sc = game.user.hasPermission("SHOW_CURSOR");
    const sr = game.user.hasPermission("SHOW_RULER");
    if ( !(sc || sr) ) return;
    const position = event.data.getLocalPosition(this);
    const ruler = sr && (this.ruler._state > 0) ? this.ruler.toJSON() : null;
    game.user.broadcastActivity({
      cursor: position,
      ruler: ruler
    });
  }

  /* -------------------------------------------- */
  /*  Methods
  /* -------------------------------------------- */

  /**
   * Create and draw the Cursor object for a given User
   * @param {User} user   The User document for whom to draw the cursor Container
   */
  drawCursor(user) {
    if ( user.id in this._cursors ) {
      this._cursors[user.id].destroy({children: true});
      delete this._cursors[user.id];
    }
    return this._cursors[user.id] = this.cursors.addChild(new Cursor(user));
  }

  /* -------------------------------------------- */

  /**
   * Update the cursor when the user moves to a new position
   * @param {User} user         The User for whom to update the cursor
   * @param {Point} position    The new cursor position
   */
  updateCursor(user, position) {
    if ( !this.cursors ) return;
    const cursor = this._cursors[user.id] || this.drawCursor(user);

    // Ignore cursors on other Scenes
    if ( ( position === null ) || (user.viewedScene !== canvas.scene.id) ) {
      if ( cursor ) cursor.visible = false;
      return;
    }

    // Ignore cursors for users who are not permitted to share
    if ( (user === game.user) || !user.hasPermission("SHOW_CURSOR") ) {
      if ( cursor ) cursor.visible = false;
        return;
    }

    // Show the cursor in its currently tracked position
    cursor.visible = true;
    cursor.target = {x: position.x || 0, y: position.y || 0};
  }

  /* -------------------------------------------- */

  /**
   * Update display of an active Ruler object for a user given provided data
   * @param {User} user             The User for whom to update the ruler
   * @param {object} rulerData      Data which describes the new ruler measurement to display
   */
  updateRuler(user, rulerData) {

    // Ignore rulers for users who are not permitted to share
    if ( (user === game.user) || !user.hasPermission("SHOW_RULER") ) return;

    // Update the Ruler display for the user
    let ruler = this.getRulerForUser(user.id);
    if ( !ruler ) return;
    if ( rulerData === null ) ruler.clear();
    else ruler.update(rulerData);
  }
}
