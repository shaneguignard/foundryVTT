/**
 * The Ruler - used to measure distances and trigger movements
 * @param {User}  The User for whom to construct the Ruler instance
 * @type {PIXI.Container}
 */
class Ruler extends PIXI.Container {
  constructor(user, {color=null}={}) {
    super();
    user = user || game.user;

    /**
     * Record the User which this Ruler references
     * @type {User}
     */
    this.user = user;

    /**
     * The ruler name - used to differentiate between players
     * @type {string}
     */
    this.name = `Ruler.${user.id}`;

    /**
     * The ruler color - by default the color of the active user
     * @type {number|null}
     */
    this.color = color || foundry.utils.colorStringToHex(this.user.data.color) || 0x42F4E2;

    /**
     * This Array tracks individual waypoints along the ruler's measured path.
     * The first waypoint is always the origin of the route.
     * @type {Array.<PIXI.Point>}
     */
    this.waypoints = [];

    /**
     * The current destination point at the end of the measurement
     * @type {PIXI.Point}
     */
    this.destination = null;

    /**
     * The Ruler element is a Graphics instance which draws the line and points of the measured path
     * @type {PIXI.Graphics}
     */
    this.ruler = this.addChild(new PIXI.Graphics());

    /**
     * The Labels element is a Container of Text elements which label the measured path
     * @type {PIXI.Container}
     */
    this.labels = this.addChild(new PIXI.Container());

    /**
     * Track the current measurement state
     * @type {number}
     */
    this._state = Ruler.STATES.INACTIVE;
  }

  /**
   * An enumeration of the possible Ruler measurement states.
   * @enum {number}
   */
  static STATES = {
    INACTIVE: 0,
    STARTING: 1,
    MEASURING: 2,
    MOVING: 3
  };

  /* -------------------------------------------- */

  /**
   * Is the Ruler being actively used to measure distance?
   * @return {boolean}
   */
  get active() {
    return this.waypoints.length > 0;
  }

  /* -------------------------------------------- */

  /**
   * Measure the distance between two points and render the ruler UI to illustrate it
   * @param {PIXI.Point} destination  The destination point to which to measure
   * @param {boolean} gridSpaces      Restrict measurement only to grid spaces
   */
  measure(destination, {gridSpaces=true}={}) {
    destination = new PIXI.Point(...canvas.grid.getCenter(destination.x, destination.y));
    const waypoints = this.waypoints.concat([destination]);
    const r = this.ruler;
    this.destination = destination;

    // Iterate over waypoints and construct segment rays
    const segments = [];
    for ( let [i, dest] of waypoints.slice(1).entries() ) {
      const origin = waypoints[i];
      const label = this.labels.children[i];
      const ray = new Ray(origin, dest);
      if ( ray.distance < 10 ) {
        if ( label ) label.visible = false;
        continue;
      }
      segments.push({ray, label});
    }

    // Compute measured distance
    const distances = canvas.grid.measureDistances(segments, {gridSpaces});
    let totalDistance = 0;
    for ( let [i, d] of distances.entries() ) {
      totalDistance += d;
      let s = segments[i];
      s.last = i === (segments.length - 1);
      s.distance = d;
      s.text = this._getSegmentLabel(d, totalDistance, s.last);
    }

    // Clear the grid highlight layer
    const hlt = canvas.grid.highlightLayers[this.name] || canvas.grid.addHighlightLayer(this.name);
    hlt.clear();

    // Draw measured path
    r.clear();
    for ( let s of segments ) {
      const {ray, label, text, last} = s;

      // Draw line segment
      r.lineStyle(6, 0x000000, 0.5).moveTo(ray.A.x, ray.A.y).lineTo(ray.B.x, ray.B.y)
       .lineStyle(4, this.color, 0.25).moveTo(ray.A.x, ray.A.y).lineTo(ray.B.x, ray.B.y);

      // Draw the distance label just after the endpoint of the segment
      if ( label ) {
        label.text = text;
        label.alpha = last ? 1.0 : 0.5;
        label.visible = true;
        let labelPosition = ray.project((ray.distance + 50) / ray.distance);
        label.position.set(labelPosition.x, labelPosition.y);
      }

      // Highlight grid positions
      this._highlightMeasurement(ray);
    }

    // Draw endpoints
    for ( let p of waypoints ) {
      r.lineStyle(2, 0x000000, 0.5).beginFill(this.color, 0.25).drawCircle(p.x, p.y, 8);
    }

    // Return the measured segments
    return segments;
  }

  /* -------------------------------------------- */

  /**
   * Get the text label for a segment of the measured path
   * @param {number} segmentDistance
   * @param {number} totalDistance
   * @param {boolean} isTotal
   * @return {string}
   * @private
   */
  _getSegmentLabel(segmentDistance, totalDistance, isTotal) {
    const units = canvas.scene.data.gridUnits;
    let label = `${Math.round(segmentDistance * 100) / 100} ${units}`;
    if ( isTotal ) {
      label += ` [${Math.round(totalDistance * 100) / 100} ${units}]`;
    }
    return label;
  }

  /* -------------------------------------------- */

  /**
   * Highlight the measurement required to complete the move in the minimum number of discrete spaces
   * @param {Ray} ray
   * @private
   */
  _highlightMeasurement(ray) {
    const spacer = canvas.scene.data.gridType === CONST.GRID_TYPES.SQUARE ? 1.41 : 1;
    const nMax = Math.max(Math.floor(ray.distance / (spacer * Math.min(canvas.grid.w, canvas.grid.h))), 1);
    const tMax = Array.fromRange(nMax+1).map(t => t / nMax);

    // Track prior position
    let prior = null;

    // Iterate over ray portions
    for ( let [i, t] of tMax.entries() ) {
      let {x, y} = ray.project(t);

      // Get grid position
      let [r0, c0] = (i === 0) ? [null, null] : prior;
      let [r1, c1] = canvas.grid.grid.getGridPositionFromPixels(x, y);
      if ( r0 === r1 && c0 === c1 ) continue;

      // Highlight the grid position
      let [x1, y1] = canvas.grid.grid.getPixelsFromGridPosition(r1, c1);
      canvas.grid.highlightPosition(this.name, {x: x1, y: y1, color: this.color});

      // Skip the first one
      prior = [r1, c1];
      if ( i === 0 ) continue;

      // If the positions are not neighbors, also highlight their halfway point
      if ( !canvas.grid.isNeighbor(r0, c0, r1, c1) ) {
        let th = tMax[i - 1] + (0.5 / nMax);
        let {x, y} = ray.project(th);
        let [rh, ch] = canvas.grid.grid.getGridPositionFromPixels(x, y);
        let [xh, yh] = canvas.grid.grid.getPixelsFromGridPosition(rh, ch);
        canvas.grid.highlightPosition(this.name, {x: xh, y: yh, color: this.color});
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Determine whether a SPACE keypress event entails a legal token movement along a measured ruler
   *
   * @return {Promise<boolean>}    An indicator for whether a token was successfully moved or not. If True the event should be
   *                               prevented from propagating further, if False it should move on to other handlers.
   */
  async moveToken() {
    let wasPaused = game.paused;
    if ( wasPaused && !game.user.isGM ) {
      ui.notifications.warn("GAME.PausedWarning", {localize: true});
      return false;
    }
    if ( !this.visible || !this.destination ) return false;
    const token = this._getMovementToken();
    if ( !token ) return false;

    // Determine offset relative to the Token top-left.
    // This is important so we can position the token relative to the ruler origin for non-1x1 tokens.
    const origin = canvas.grid.getTopLeft(this.waypoints[0].x, this.waypoints[0].y);
    const s2 = canvas.dimensions.size / 2;
    const dx = Math.round((token.data.x - origin[0]) / s2) * s2;
    const dy = Math.round((token.data.y - origin[1]) / s2) * s2;

    // Get the movement rays and check collision along each Ray
    // These rays are center-to-center for the purposes of collision checking
    let rays = this._getRaysFromWaypoints(this.waypoints, this.destination);
    let hasCollision = rays.some(r => canvas.walls.checkCollision(r));
    if ( hasCollision ) {
      ui.notifications.error("ERROR.TokenCollide", {localize: true});
      return false;
    }

    // Execute the movement path defined by each ray.
    this._state = Ruler.STATES.MOVING;
    let priorDest = undefined;
    for ( let r of rays ) {

      // Break the movement if the game is paused
      if ( !wasPaused && game.paused ) break;

      // Break the movement if Token is no longer located at the prior destination (some other change override this)
      if ( priorDest && ((token.data.x !== priorDest.x) || (token.data.y !== priorDest.y)) ) break;

      // Adjust the ray based on token size
      const dest = canvas.grid.getTopLeft(r.B.x, r.B.y);
      const path = new Ray({x: token.x, y: token.y}, {x: dest[0] + dx, y: dest[1] + dy});

      // Commit the movement and update the final resolved destination coordinates
      await token.document.update(path.B);
      path.B.x = token.data.x;
      path.B.y = token.data.y;
      priorDest = path.B;

      // Retrieve the movement animation and await its completion
      const anim = CanvasAnimation.getAnimation(token.movementAnimationName);
      if ( anim?.promise ) await anim.promise;
    }

    // Once all animations are complete we can clear the ruler
    this._endMeasurement();
  }

  /* -------------------------------------------- */

  /**
   * Acquire a Token, if any, which is eligible to perform a movement based on the starting point of the Ruler
   * @return {Token}
   * @private
   */
  _getMovementToken() {
    let [x0, y0] = Object.values(this.waypoints[0]);
    let tokens = canvas.tokens.controlled;
    if ( !tokens.length && game.user.character ) tokens = game.user.character.getActiveTokens();
    if ( !tokens.length ) return null;
    return tokens.find(t => {
      let pos = new PIXI.Rectangle(t.x - 1, t.y - 1, t.w + 2, t.h + 2);
      return pos.contains(x0, y0);
    });
  }

  /* -------------------------------------------- */

  /**
   * A helper method to return an Array of Ray objects constructed from the waypoints of the measurement
   * @param {PIXI.Point[]} waypoints    An Array of waypoint {x, y} Objects
   * @param {PIXI.Point} destination    An optional destination point to append to the existing waypoints
   * @return {Ray[]}                    An Array of Ray objects which represent the segemnts of the waypoint path
   * @private
   */
  _getRaysFromWaypoints(waypoints, destination) {
    if ( destination ) waypoints = waypoints.concat([destination]);
    return waypoints.slice(1).map((wp, i) => {
      return new Ray(waypoints[i], wp);
    });
  }

  /* -------------------------------------------- */

  /**
   * Clear display of the current Ruler
   */
  clear() {
    this._state = Ruler.STATES.INACTIVE;
    this.waypoints = [];
    if ( this.ruler ) this.ruler.clear();
    this.labels.removeChildren().forEach(c => c.destroy());
    canvas.grid.clearHighlightLayer(this.name);
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * Handle the beginning of a new Ruler measurement workflow
   * @see {Canvas._onDragLeftStart}
   */
  _onDragStart(event) {
    this.clear();
    this._state = Ruler.STATES.STARTING;
    this._addWaypoint(event.data.origin);
  }

  /* -------------------------------------------- */

  /**
   * Handle left-click events on the Canvas during Ruler measurement.
   * @see {Canvas._onClickLeft}
   */
  _onClickLeft(event) {
    const isCtrl = game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.CONTROL);
    if ( (this._state === 2) && isCtrl ) this._addWaypoint(event.data.origin);
  }

  /* -------------------------------------------- */

  /**
   * Handle right-click events on the Canvas during Ruler measurement.
   * @see {Canvas._onClickRight}
   */
  _onClickRight(event) {
    if ( (this._state === 2) && (this.waypoints.length > 1) ) {
      this._removeWaypoint(event.data.origin, {snap: !event.data.originalEvent.shiftKey});
      return canvas.mouseInteractionManager._dragRight = false;
    }
    else return this._endMeasurement();
  }

  /* -------------------------------------------- */

  /**
   * Continue a Ruler measurement workflow for left-mouse movements on the Canvas.
   * @see {Canvas._onDragLeftMove}
   */
  _onMouseMove(event) {
    if ( this._state === Ruler.STATES.MOVING ) return;

    // Extract event data
    const mt = event._measureTime || 0;
    const {origin, destination, originalEvent} = event.data;

    // Do not begin measuring unless we have moved at least 1/4 of a grid space
    const dx = destination.x - origin.x;
    const dy = destination.y - origin.y;
    const distance = Math.hypot(dy, dx);
    if ( !this.waypoints.length && (distance < (canvas.dimensions.size / 4))) return;

    // Hide any existing Token HUD
    canvas.hud.token.clear();
    delete event.data.hudState;

    // Draw measurement updates
    if ( Date.now() - mt > 50 ) {
      this.measure(destination, {gridSpaces: !originalEvent.shiftKey});
      event._measureTime = Date.now();
      this._state = Ruler.STATES.MEASURING;
    }
  }

  /* -------------------------------------------- */

  /**
   * Conclude a Ruler measurement workflow by releasing the left-mouse button.
   * @see {Canvas._onDragLeftDrop}
   */
  _onMouseUp(event) {
    const oe = event.data.originalEvent;
    const isCtrl = oe.ctrlKey || oe.metaKey;
    if ( !isCtrl ) this._endMeasurement();
  }

  /* -------------------------------------------- */

  /**
   * Handle the addition of a new waypoint in the Ruler measurement path
   * @param {PIXI.Point} point
   * @private
   */
  _addWaypoint(point) {
    const center = canvas.grid.getCenter(point.x, point.y);
    this.waypoints.push(new PIXI.Point(center[0], center[1]));
    this.labels.addChild(new PreciseText("", CONFIG.canvasTextStyle));
  }

  /* -------------------------------------------- */

  /**
   * Handle the removal of a waypoint in the Ruler measurement path
   * @param {PIXI.Point} point      The current cursor position to snap to
   * @param {boolean} [snap]        Snap exactly to grid spaces?
   * @private
   */
  _removeWaypoint(point, {snap=true}={}) {
    this.waypoints.pop();
    this.labels.removeChild(this.labels.children.pop());
    this.measure(point, {gridSpaces: snap});
  }

  /* -------------------------------------------- */

  /**
   * Handle the conclusion of a Ruler measurement workflow
   * @private
   */
  _endMeasurement() {
    this.clear();
    game.user.broadcastActivity({ruler: null});
    canvas.mouseInteractionManager.state = MouseInteractionManager.INTERACTION_STATES.HOVER;
  }

  /* -------------------------------------------- */
  /*  Saving and Loading
  /* -------------------------------------------- */

  /**
   * @typedef {object} RulerData
   * @property {number} _state           The ruler measurement state.
   * @property {string} name             A unique name for the ruler containing the owning user's ID.
   * @property {PIXI.Point} destination  The current point the ruler has been extended to.
   * @property {string} class            The class name of this ruler instance.
   * @property {PIXI.Point[]} waypoints  Additional waypoints along the ruler's length, including the starting point.
   */

  /**
   * @returns {RulerData}
   */
  toJSON() {
    return {
      class: "Ruler",
      name: `Ruler.${game.user.id}`,
      waypoints: this.waypoints,
      destination: this.destination,
      _state: this._state
    }
  }

  /* -------------------------------------------- */

  /**
   * Update a Ruler instance using data provided through the cursor activity socket
   * @param {Object} data   Ruler data with which to update the display
   */
  update(data) {
    if ( data.class !== "Ruler" ) throw new Error("Unable to recreate Ruler instance from provided data");

    // Populate data
    this.waypoints = data.waypoints;
    this.destination = data.destination;
    this._state = data._state;

    // Ensure labels are created
    for ( let i=0; i<this.waypoints.length - this.labels.children.length; i++) {
      this.labels.addChild(new PreciseText("", CONFIG.canvasTextStyle));
    }

    // Measure current distance
    if ( data.destination ) this.measure(data.destination);
  }
}
