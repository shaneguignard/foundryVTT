/**
 * The Lighting Layer which displays darkness and light within the rendered Scene.
 * Lighting Layer (Container)
 *   Illumination Container [MULTIPLY]
 *     Background (Graphics)
 *     Light (Container) [LOS Mask]
 *       Source 1, ..., Source N (Container)
 *     Darkness (Container)
 *       Source 1, ..., Source N (Container)
 *   Coloration Container [ADD_NPM]
 * @extends {PlaceablesLayer}
 *
 * @example <caption>The lightingRefresh hook</caption>
 * Hooks.on("lightingRefresh", layer => {});
 */
class LightingLayer extends PlaceablesLayer {
  constructor(...args) {
    super(...args);

    /**
     * A mapping of light sources which are active within the rendered Scene
     * @type {Collection<string,LightSource>}
     */
    this.sources = new foundry.utils.Collection();

    /**
     * Increment this whenever lighting channels are re-configured.
     * This informs lighting and vision sources whether they need to re-render.
     * @type {number}
     */
    this.version = 0;

    /**
     * The currently displayed darkness level, which may override the saved Scene value
     * @type {number}
     */
    this.darknessLevel = 0;

    /**
     * The current client setting for whether global illumination is used or not
     * @type {boolean}
     */
    this.globalLight = false;

    /**
     * The coloration container which visualizes the effect of light sources
     * @type {PIXI.Container}
     */
    this.coloration = null;

    /**
     * The illumination container which visualizes darkness and light
     * @type {PIXI.Container}
     */
    this.illumination = null;

    /**
     * The background container which visualizes the background
     * @type {PIXI.Container}
     */
    this.background = null;

    /**
     * An array of light sources which are currently animated
     * @type {LightSource[]}
     */
    this._animatedSources = [];
  }

  /**
   * A mapping of different light level channels
   * @typedef {{hex: number, rgb: number[]}} LightChannel
   * @type {{black: LightChannel, dark: LightChannel, dim: LightChannel, bright: LightChannel}}
   */
  channels;

  /** @inheritdoc */
  static documentName = "AmbientLight";

  /** @inheritdoc */
  static get layerOptions() {
    return foundry.utils.mergeObject(super.layerOptions, {
      name: "lighting",
      rotatableObjects: true,
      zIndex: 300
    });
  }

  /* -------------------------------------------- */

  /**
   * TODO: Significant portions of this method may no longer be needed
   * Configure the lighting channels which are inputs to the ShadowMap
   * @param {object} [options]
   * @param {number} [options.darkness]         Darkness level override.
   * @param {number} [options.backgroundColor]  Canvas background color override.
   * @returns {{black: object, dark: object, dim: object, bright: object}}
   * @private
   */
  _configureChannels({darkness, backgroundColor}={}) {
    this.version++;
    let {darknessColor, daylightColor, lightLevels, darknessLightPenalty} = CONFIG.Canvas;
    const {dark, dim, bright} = lightLevels;
    darkness = darkness ?? canvas.scene.data.darkness;
    const channels = { canvas: {}, background: {}, black: {}, bright: {}, dark: {}, darkness: {}, dim: {} };

    // The configure darkness level
    channels.darkness.level = darkness;
    channels.darkness.rgb = foundry.utils.hexToRGB(darknessColor);

    // The canvas background is a blend between the Scene background color and the darkness level
    const sceneRGB = foundry.utils.hexToRGB(backgroundColor ?? canvas.backgroundColor);
    channels.canvas.rgb = channels.darkness.rgb.map((c, i) => ((1-darkness) + (darkness*c)) * sceneRGB[i]);
    channels.canvas.hex = foundry.utils.rgbToHex(channels.canvas.rgb);

    // The background is based on the darkness color and the darkness level
    const daylightRGB = canvas.scene.data.tokenVision ? foundry.utils.hexToRGB(daylightColor) : [1.0, 1.0, 1.0];
    channels.background.rgb = channels.darkness.rgb.map((c, i) => (darkness * c) + ((1-darkness) * daylightRGB[i]));
    channels.background.hex = foundry.utils.rgbToHex(channels.background.rgb);

    // Magical darkness and blackness is based on the true darkness color
    channels.dark.rgb = channels.darkness.rgb.map(c => (1 + dark) * c);
    channels.dark.hex = foundry.utils.rgbToHex(channels.dark.rgb);
    channels.black.rgb = channels.dark.rgb.map(c => 0.5 * c);
    channels.black.hex = foundry.utils.rgbToHex(channels.black.rgb);

    // Bright light is penalized by the darkness level
    const penalty = 1 - (darknessLightPenalty * darkness);
    channels.bright.rgb = [1,1,1].map(c => bright * c * penalty);
    channels.bright.hex = foundry.utils.rgbToHex(channels.bright.rgb);

    // Dim light is halfway between darkness and bright
    channels.dim.rgb = channels.bright.rgb.map((c, i) => (dim * c) + ((1 - dim) * channels.background.rgb[i]));
    channels.dim.hex = foundry.utils.rgbToHex(channels.dim.rgb);
    return channels;
  }

  /* -------------------------------------------- */
  /*  Rendering
  /* -------------------------------------------- */

  /** @override */
  async draw() {
    await super.draw();

    // Configure the layer
    this.globalLight = canvas.scene.data.globalLight;
    this.darknessLevel = canvas.scene.data.darkness;

    // Create containers
    this.lighting = this.addChildAt(new PIXI.Container(), 0);
    this.background = this.lighting.addChild(this._drawBackgroundContainer());
    this.illumination = this.lighting.addChild(this._drawIlluminationContainer());
    this.coloration = this.lighting.addChild(this._drawColorationContainer());

    // Create a container of masking polygons which are not rendered
    this.masks = this.lighting.addChild(new PIXI.Container());
    this.masks.renderable = false;

    // Draw the background
    const bgRect = canvas.dimensions.rect.clone().pad(CONFIG.Canvas.blurStrength * 2);
    this.illumination.background.clear().beginFill(0xFFFFFF, 1.0).drawShape(bgRect).endFill();

    // Activate animation
    this.activateAnimation();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Draw the coloration container which is responsible for rendering the visible hue of a light source.
   * Apply an additive blend to the entire container after each individual light source is blended via screen.
   * @return {PIXI.Container}
   * @private
   */
  _drawColorationContainer() {
    const c = new PIXI.Container();
    c.filter = new PIXI.filters.AlphaFilter(1.0);
    c.filter.blendMode = PIXI.BLEND_MODES.ADD;
    c.filters = [c.filter];
    c.sortableChildren = true;
    return c;
  }

  /* -------------------------------------------- */

  /**
   * Draw the illumination container which is responsible for displaying darkness and light.
   * @return {PIXI.Container}
   * @private
   */
  _drawIlluminationContainer() {
    const c = new PIXI.Container();
    c.background = c.addChild(new PIXI.LegacyGraphics());

    // We need an inner container to ensure that the masked lights have background beneath them
    c.primary = c.addChild(new PIXI.Container());
    c.sbackground = c.primary.addChild(c.background.clone());
    c.lights = c.primary.addChild(new PIXI.Container());
    c.lights.sortableChildren = true;

    // Multiply filter for the outer container
    c.filter = canvas.performance.blur.illumination ? canvas.createBlurFilter() : new PIXI.filters.AlphaFilter(1.0);
    c.filter.blendMode = PIXI.BLEND_MODES.MULTIPLY;
    c.filters = [c.filter];
    c.filterArea = canvas.app.renderer.screen;
    return c;
  }

  /* -------------------------------------------- */

  /**
   * Draw the background container which is responsible for displaying altered background.
   * @return {PIXI.Container}
   * @private
   */
  _drawBackgroundContainer() {
    const c = new PIXI.Container();
    c.filter = new PIXI.filters.AlphaFilter(1.0);
    c.filter.blendMode = PIXI.BLEND_MODES.NORMAL;
    c.filters = [c.filter];
    c.sortableChildren = true;
    return c;
  }

  /* -------------------------------------------- */
  /*  Layer Methods                               */
  /* -------------------------------------------- */

  /**
   * Does this scene currently benefit from global illumination?
   * @returns {boolean}
   */
  hasGlobalIllumination() {
    const sd = canvas.scene.data;
    if ( !sd.globalLight ) return false;
    return (sd.globalLightThreshold === null) || (this.darknessLevel <= sd.globalLightThreshold);
  }

  /* -------------------------------------------- */

  /**
   * Initialize all AmbientLight sources which are present on this layer
   */
  initializeSources() {
    this.darknessLevel = canvas.scene.data.darkness;
    this.sources.clear();

    // Ambient Light sources
    for ( let light of this.placeables ) {
      light.updateSource({defer: true});
    }

    // Token light sources
    for ( let token of canvas.tokens.placeables ) {
      token.updateLightSource({defer: true});
    }
  }

  /* -------------------------------------------- */

  /**
   * Refresh the active display of the LightingLayer.
   * Update the scene background color, light sources, and darkness sources
   * @param {object} [options]
   * @param {number} [options.darkness]         An override darkness level to which the layer should be temporarily
   *                                            rendered.
   * @param {string} [options.backgroundColor]  An override canvas background color.
   */
  refresh({darkness, backgroundColor}={}) {
    const priorLevel = this.darknessLevel;
    const darknessChanged = (darkness !== undefined) && (darkness !== priorLevel);
    const bgChanged = backgroundColor !== undefined;
    this.darknessLevel = darkness = Math.clamped(darkness ?? this.darknessLevel, 0, 1);

    // Update lighting channels
    if ( darknessChanged || bgChanged || !this.channels ) {
      this.channels = this._configureChannels({
        backgroundColor: foundry.utils.colorStringToHex(backgroundColor),
        darkness
      });
    }

    // Track global illumination
    let refreshVision = false;
    const globalLight = this.hasGlobalIllumination();
    if ( globalLight !== this.globalLight ) {
      this.globalLight = globalLight;
      canvas.perception.schedule({sight: {initialize: true, refresh: true}});
    }

    // Clear currently rendered sources
    const msk = this.masks;
    msk.removeChildren();
    const bkg = this.background;
    bkg.removeChildren();
    const ilm = this.illumination;
    ilm.lights.removeChildren();
    const col = this.coloration;
    col.removeChildren();
    this._animatedSources = [];

    // Tint the background color
    canvas.app.renderer.backgroundColor = this.channels.canvas.hex;
    ilm.background.tint = ilm.sbackground.tint = this.channels.background.hex;

    // Render light sources
    for ( let source of this.sources ) {

      // Check the active state of the light source
      const isActive = darkness.between(source.data.darkness.min, source.data.darkness.max);
      if ( source.active !== isActive ) refreshVision = true;
      source.active = isActive;
      if ( !source.active ) continue;

      // Add the source mask used by all source meshes
      if ( source.losMask ) msk.addChild(source.losMask);

      // Draw the light update
      const meshes = source.drawMeshes();
      if ( meshes.background ) bkg.addChild(meshes.background);
      if ( meshes.light ) ilm.lights.addChild(meshes.light);
      if ( meshes.color ) col.addChild(meshes.color);
      if ( source.data.animation?.type ) this._animatedSources.push(source);
    }

    // Render sight from vision sources
    for ( let vs of canvas.sight.sources ) {
      if ( vs.radius <= 0 ) continue;
      if ( vs.losMask ) msk.addChild(vs.losMask);
      const sight = vs.drawVision();
      if ( sight ) ilm.lights.addChild(sight);
    }

    // Draw non-occluded roofs that block light
    const displayRoofs = canvas.foreground.displayRoofs;
    for ( let roof of canvas.foreground.roofs ) {
      if ( !displayRoofs || roof.occluded ) continue;

      // Block illumination
      const si = roof.getRoofSprite();
      if ( !si ) continue;
      si.zIndex = 9999; // By convention
      si.tint = this.channels.background.hex;
      this.illumination.lights.addChild(si)

      // Block coloration
      const sc = roof.getRoofSprite();
      sc.tint = 0x000000;
      this.coloration.addChild(sc);

      // Block background
      const sb = roof.getRoofSprite();
      sb.blendMode = PIXI.BLEND_MODES.ERASE;
      this.background.addChild(sb);
    }

    // Refresh vision if necessary
    if ( refreshVision ) canvas.perception.schedule({sight: {refresh: true}});

    // Refresh audio if darkness changed
    if ( darknessChanged ) {
      this._onDarknessChange(darkness, priorLevel);
      canvas.sounds._onDarknessChange(darkness, priorLevel);
    }

    /**
     * A hook event that fires when the LightingLayer is refreshed.
     * @function lightingRefresh
     * @memberof hookEvents
     * @param {LightingLayer} light The LightingLayer
     */
    Hooks.callAll("lightingRefresh", this);
  }

  /* -------------------------------------------- */

  /** @override */
  async tearDown() {
    this.sources.clear();
    this.version = 0;
    this.channels = undefined;
    this.deactivateAnimation();
    return super.tearDown();
  }

  /* -------------------------------------------- */
  /*  Animation                                   */
  /* -------------------------------------------- */

  /**
   * Activate light source animation for AmbientLight objects within this layer
   */
  activateAnimation() {
    this.deactivateAnimation();
    if ( game.settings.get("core", "lightAnimation") === false ) return;
    canvas.app.ticker.add(this._animateSource, this);
  }

  /* -------------------------------------------- */

  /**
   * Deactivate light source animation for AmbientLight objects within this layer
   */
  deactivateAnimation() {
    canvas.app.ticker.remove(this._animateSource, this);
  }

  /* -------------------------------------------- */

  /**
   * The ticker handler which manages animation delegation
   * @param {number} dt   Delta time
   * @private
   */
  _animateSource(dt) {
    for ( let source of this._animatedSources ) {
      source.animate(dt);
    }
  }

  /* -------------------------------------------- */

  /**
   * Animate a smooth transition of the darkness overlay to a target value.
   * Only begin animating if another animation is not already in progress.
   * @param {number} target     The target darkness level between 0 and 1
   * @param {number} duration   The desired animation time in milliseconds. Default is 10 seconds
   * @return {Promise}          A Promise which resolves once the animation is complete
   */
  async animateDarkness(target=1.0, {duration=10000}={}) {
    const animationName = "lighting.animateDarkness";
    CanvasAnimation.terminateAnimation(animationName);
    if ( target === this.darknessLevel ) return false;
    if ( duration <= 0 ) return this.refresh({darkness: target});

    // Prepare the animation data object
    const animationData = [{
      parent: {darkness: this.darknessLevel},
      attribute: "darkness",
      to: Math.clamped(target, 0, 1)
    }];

    // Trigger the animation function
    return CanvasAnimation.animateLinear(animationData, {
      name: animationName,
      duration: duration,
      ontick: (dt, attributes) => {
        this.refresh(attributes[0].parent)
      }
    });
  }

  /* -------------------------------------------- */

  /**
   * Actions to take when the darkness level of the Scene is changed
   * @param {number} darkness   The new darkness level
   * @param {number} prior      The prior darkness level
   * @private
   */
  _onDarknessChange(darkness, prior) {
    if ( !this._active ) return;
    for ( let light of this.placeables ) {
      light.refreshControl();
    }
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  async _onDragLeftStart(event) {
    await super._onDragLeftStart(event);
    const origin = event.data.origin;

    // Create the preview source
    const doc = new AmbientLightDocument({
      x: origin.x,
      y: origin.y,
      type: "l"
    }, {parent: canvas.scene});
    const preview = new AmbientLight(doc);
    preview.source.preview = true;
    event.data.preview = this.preview.addChild(preview);
    this.sources.set(preview.sourceId, preview.source);
    this.deactivateAnimation();
    return preview.draw();
  }

  /* -------------------------------------------- */

  /** @override */
  _onDragLeftMove(event) {
    const { destination, createState, preview, origin } = event.data;
    if ( createState === 0 ) return;

    // Update the light radius
    const radius = Math.hypot(destination.x - origin.x, destination.y - origin.y);

    // Update the preview object data
    preview.data.config.dim = radius * (canvas.dimensions.distance / canvas.dimensions.size);
    preview.data.config.bright = preview.data.config.dim / 2;

    // Refresh the layer display
    preview.updateSource();
    preview.refresh();

    // Confirm the creation state
    event.data.createState = 2;
  }

  /* -------------------------------------------- */

  /** @override */
  _onDragLeftCancel(event) {
    super._onDragLeftCancel(event);
    this.sources.delete(`${this.constructor.documentName}.preview`);
    this.refresh();
    this.activateAnimation();
  }

  /* -------------------------------------------- */

  /** @override */
  _onMouseWheel(event) {

    // Identify the hovered light source
    const light = this._hover;
    if ( !light || (light.data.config.angle === 360) ) return;

    // Determine the incremental angle of rotation from event data
    let snap = event.shiftKey ? 15 : 3;
    let delta = snap * Math.sign(event.deltaY);
    return light.rotate(light.data.rotation + delta, snap);
  }
}
