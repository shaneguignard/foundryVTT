/**
 * Compute a PointSourcePolygon using the "Radial Sweep" algorithm.
 * This algorithm computes collisions by rotating clockwise about the origin, testing against every unique endpoint.
 * This algorithm has been outmoded by ClockwiseSweepPolygon and can be deleted before V9 stable.
 * @extends {PointSourcePolygon}
 *
 * @deprecated since v9d2
 * @ignore
 *
 * @typedef {Map<number,WallEndpoint>} EndpointMap
 */
class RadialSweepPolygon extends PointSourcePolygon {

  /**
   * The limiting radius of the polygon, if applicable
   * @type {object}
   * @property {string} type          The type of polygon being computed
   * @property {number} [angle=360]   The angle of emission, if limited
   * @property {number} [density=6]   The desired density of padding rays, a number per PI
   * @property {number} [radius]      A limited radius of the resulting polygon
   * @property {number} [rotation]    The direction of facing, required if the angle is limited
   * @property {boolean} [debug]      Display debugging visualization and logging for the polygon
   */
  config = {};

  /**
   * The mapping of Wall Endpoint objects which are used to compute this polygon.
   * @type {EndpointMap}
   */
  endpoints = new Map();

  /**
   * The set of Wall objects which can affect this Polygon.
   * @type {object}
   */
  walls = {};

  /** @inheritdoc */
  _compute() {
    const {angle, rotation, type} = this.config;
    this.config.hasRadius = this.config.radius > 0;

    // Record configuration parameters
    this.config.maxR = canvas.dimensions.maxR;
    const isLimited = this.config.isLimited = angle < 360;
    this.config.aMin = isLimited ? Math.normalizeRadians(Math.toRadians(rotation + 90 - (angle / 2))) : -Math.PI;
    this.config.aMax = isLimited ? this.config.aMin + Math.toRadians(angle) : Math.PI;

    // Construct endpoints for each Wall
    this._initializeEndpoints(type);

    // Iterate over endpoints
    this._sweepEndpoints();

    // Create the Polygon geometry
    this._constructPoints();

    // Clean up
    delete this.endpoints;
    delete this.rays;
    delete this.walls;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  initialize(origin, {type="sight", angle=360, density=6, radius, rotation, debug=false}={}) {
    this.origin = origin;
    this.config = {type, angle, density, radius, rotation, debug};
  }

  /* -------------------------------------------- */
  /*  Endpoint Management                         */
  /* -------------------------------------------- */

  /**
   * Initialize the endpoints present for walls within this Scene.
   * @param {string} type       The type of polygon being constructed in WALL_RESTRICTION_TYPES
   * @private
   */
  _initializeEndpoints(type) {
    this.walls = {};
    this.endpoints.clear();
    const norm = a => a < this.config.aMin ? a + (2*Math.PI) : a;

    // Consider all walls in the Scene
    for ( let wall of this._getCandidateWalls() ) {

      // Test whether a wall should be included in the set considered for this polygon
      if ( !this._includeWall(wall, type) ) continue;

      // Register both endpoints for included walls
      let [x0, y0, x1, y1] = wall.data.c;
      let ak = WallEndpoint.getKey(x0, y0);
      let a = this.endpoints.get(ak);
      if ( !a ) {
        a = new WallEndpoint(x0, y0);
        a.angle = norm(Math.atan2(y0 - this.origin.y, x0 - this.origin.x));
        a.isEndpoint = true;
        this.endpoints.set(ak, a);
      }
      a.attachWall(wall);

      let bk = WallEndpoint.getKey(x1, y1);
      let b = this.endpoints.get(bk);
      if ( !b ) {
        b = new WallEndpoint(x1, y1);
        b.angle = norm(Math.atan2(y1 - this.origin.y, x1 - this.origin.x));
        b.isEndpoint = true;
        this.endpoints.set(bk, b);
      }
      b.attachWall(wall);

      // Record the wall
      this.walls[wall.id] = {wall, a, b};
    }
  }

  /* -------------------------------------------- */

  /**
   * Get the super-set of walls which could potentially apply to this polygon.
   * @returns {Wall[]|Set<Wall>}
   * @private
   */
  _getCandidateWalls() {
    const r = this.config.radius;
    if ( !r ) return canvas.walls.placeables;
    const o = this.origin;
    const rect = new NormalizedRectangle(o.x - r, o.y - r, 2*r, 2*r);
    return canvas.walls.quadtree.getObjects(rect);
  }

  /* -------------------------------------------- */

  /**
   * Test whether a Wall object should be included as a candidate for collision from the polygon origin
   * @param {Wall} wall         The Wall being considered
   * @param {string} type       The type of polygon being computed
   * @returns {boolean}         Should the wall be included?
   * @private
   */
  _includeWall(wall, type) {

    // Special case - coerce interior walls to block light and sight
    const isInterior = ( type === "sight" ) && (wall.roof?.occluded === false);
    if ( isInterior ) return true;

    // Ignore non-blocking walls and open doors
    if ( !wall.data[type] || wall.isOpen ) return false;

    // Ignore one-directional walls which are facing away from the origin
    if ( !wall.data.dir ) return true;
    const mp = wall.midpoint;
    const wa = Math.atan2(mp[1] - this.origin.y, mp[0] - this.origin.x);
    const d = Math.normalizeRadians(wa - wall.direction);
    return d.between(-Math.PI/2, Math.PI/2);
  }

  /* -------------------------------------------- */

  /**
   * Convert the set of rays into the polygon points
   * @returns {number[]}        The polygon points
   * @private
   */
  _constructPoints() {
    const points = [];
    const isLimited = this.config.isLimited;

    // Open a limited shape
    if ( isLimited ) points.push(this.origin.x, this.origin.y);

    // Add collision points from every ray
    for ( let ray of this.rays ) {
      for ( let c of ray.collisions ) {
        points.push(c.x, c.y);
      }
    }

    // Close a limited polygon
    if ( isLimited ) points.push(this.origin.x, this.origin.y);
    this.points = points;
  }

  /* -------------------------------------------- */
  /*  Radial Sweep                                */
  /* -------------------------------------------- */

  /**
   * Sweep clockwise around known wall endpoints, constructing the polygon as we go.
   * @private
   */
  _sweepEndpoints() {

    // Configure inputs
    const origin = this.origin;
    const {maxR, isLimited, aMin, aMax} = this.config;
    const radius = this.config.radius ?? maxR;
    const rays = [];
    const angles = new Set();
    const padding = Math.PI / Math.max(this.config.density, 6);

    // Sort endpoints by angle
    const endpoints = Array.from(this.endpoints.values());
    endpoints.sort((a, b) => a.angle - b.angle);

    // Begin with a ray at the lowest angle to establish initial conditions
    let lastRay = SightRay.fromAngle(origin.x, origin.y, aMin, radius);

    // We may need to explicitly include a first ray
    if ( isLimited || (endpoints.length === 0) ) {
      const pFirst = new WallEndpoint(lastRay.B.x, lastRay.B.y);
      pFirst.angle = aMin;
      endpoints.unshift(pFirst);
    }

    // We may need to explicitly include a final ray
    if ( isLimited || (endpoints.length === 1) ) {
      const aFinal = isLimited ? aMax : endpoints[0].angle + Math.PI;
      const rFinal = SightRay.fromAngle(origin.x, origin.y, aFinal, radius);
      const pFinal = new WallEndpoint(rFinal.B.x, rFinal.B.y);
      pFinal.angle = aFinal;
      endpoints.push(pFinal);
    }

    // Sweep each endpoint
    for ( let endpoint of endpoints ) {

      // De-dupe repeated angles
      if ( angles.has(endpoint.angle) ) continue;
      angles.add(endpoint.angle);

      // Skip endpoints which are not within our limited angle
      if ( isLimited && !endpoint.angle.between(aMin, aMax) ) continue;

      // Create a Ray targeting this endpoint
      const ray = SightRay.fromAngle(origin.x, origin.y, endpoint.angle, radius);
      ray.endpoint = endpoint;
      endpoint._r = ray.dx !== 0 ? (endpoint.x - origin.x) / ray.dx : (endpoint.y - origin.y) / ray.dy;
      if ( (ray.dx === 0) && (ray.dy === 0) ) endpoint._r = 0;

      // Test the ray
      this._testRay(ray, lastRay);
      if ( ray.result.superfluous ) continue;

      // Pad supplementary rays if an adjacent ray reached a terminal point
      if ( lastRay.result.terminal || (this.config.hasRadius && ray.result.terminal) ) {
        this._padRays(lastRay, ray, padding, rays, this.config.hasRadius);
      }

      // Push the ray
      rays.push(ray);
      lastRay = ray;
    }

    // For complete circles, pad gaps between the final ray and the initial one
    if ( !isLimited && lastRay.result.terminal ) {
      this._padRays(lastRay, rays[0], padding, rays, this.config.hasRadius);
    }
    this.rays = rays;
  }

  /* -------------------------------------------- */

  /**
   * Create additional rays to fill gaps with a desired padding size
   * @param {SightRay} r0       The prior SightRay that was tested
   * @param {SightRay} r1       The next SightRay that will be tested
   * @param {number} padding    The size of padding in radians to fill between r0 and r1
   * @param {SightRay[]} rays   The accumulating array of Ray objects
   * @param {boolean} requireTest   Require padding rays to be tested, instead of assuming their reach their endpoint
   * @private
   */
  _padRays(r0, r1, padding, rays, requireTest) {

    // Determine padding delta
    let d = r1.angle - r0.angle;
    if ( d < 0 ) d += (2*Math.PI); // Handle cycling past pi
    const nPad = Math.floor(d / padding);
    if ( nPad === 0 ) return [];

    // Construct padding rays
    const delta = d / nPad;
    let lr = r0;
    for ( let i=1; i<nPad; i++ ) {
      let r = r0.shiftAngle(i*delta);

      // If may be required to test the padded ray
      if ( requireTest ) {
        this._testRay(r, lr);
        lr = r;
        if ( r.result.superfluous ) continue;
      }

      // Otherwise we can assume it reaches the endpoint
      else {
        const pt = new WallEndpoint(r.B.x, r.B.y);
        pt.isTerminal = r.result.terminal = true;
        r.collisions = [pt];
      }
      rays.push(r);
    }
  }

  /* -------------------------------------------- */
  /*  Single Ray Testing                          */
  /* -------------------------------------------- */

  /**
   * Find the collisions for a provided Ray against the set of Wall objects which exists within the Scene.
   * @param {SightRay} ray            The Ray being tested
   * @param {SightRay} lastRay        The prior Ray that was fired
   * @private
   */
  _testRay(ray, lastRay) {

    // Set up variables and storages
    const testedWalls = new Set();
    const result = ray.result;
    result.activeWalls = lastRay.result.activeWalls || new Set();

    // Test walls in batches
    for ( let walls of this._getWalls(ray, ray.endpoint) ) {
      if ( !walls.size ) continue;

      // Test all walls within the batch
      const points = [];
      const keys = new Set();
      for (let wall of walls) {

        // Skip walls which have already been tested or those which are not included in this polygon
        if ( testedWalls.has(wall) ) continue;
        testedWalls.add(wall);
        const w = this.walls[wall.id];
        if ( !w ) continue;

        // Identify the collision point
        const c = this._testWall(ray, w);
        if ( !c || keys.has(c.key) ) continue;
        keys.add(c.key);
        points.push(c);
      }

      // Sort and filter the resulting points
      points.sort((a, b) => a._r - b._r);
      for ( let p of points ) {
        this._processCollision(ray, lastRay, result, p);
        if ( result.stopped ) break; // We only need two collisions
      }
      if ( result.stopped ) break; // We only need two collisions
    }

    // Maybe ignore the ray entirely
    if ( result.superfluous ) return;

    // If we only produced one collision, add the ray termination
    if ( !result.stopped && (result.collisions.length < 2) ) {
      const t = new WallEndpoint(ray.B.x, ray.B.y);
      t.isTerminal = true;
      result.collisions.push(t);
    }

    // Register the first collision point
    const [p0, p1] = result.collisions;
    ray.collisions = [p0];

    // Reference wall continuation data to determine which penetration points are necessary
    if ( p1 ) {
      if (!result.continuation) result.continuation = this._checkWallContinuation(ray, p0);
      if (result.continuation.left === false) ray.collisions.unshift(p1);  // counter-clockwise, penetrate before
      if (result.continuation.right === false) ray.collisions.push(p1);    // clockwise, penetrate after
    }

    // Record additional result metadata
    result.activeWalls = ray.lastCollision.walls;
    result.terminal = ray.lastCollision.isTerminal;
  }

  /* -------------------------------------------- */

  /**
   * Return the set of walls that should be considered as collision targets
   * @param {SightRay} ray                The ray being tested
   * @param {WallEndpoint} knownPoint     The known collision point
   * @returns {Generator<Set<Wall>>}
   * @private
   */
  * _getWalls(ray, knownPoint) {
    const steps0 = knownPoint ? Math.ceil(knownPoint._r / 0.125) : 8;
    const steps1 = 8 - steps0;
    if ( !knownPoint ) knownPoint = ray.B;
    let pt = ray.A;

    // First half, up to the known point
    let dx0 = (knownPoint.x - this.origin.x) / steps0;
    dx0 = dx0 < 0 ? Math.floor(dx0) : Math.ceil(dx0);
    let dy0 = (knownPoint.y - this.origin.y) / steps0;
    dy0 = dy0 < 0 ? Math.floor(dy0) : Math.ceil(dy0);
    for ( let s=0; s<steps0; s++ ) {
      const rect = new NormalizedRectangle(pt.x, pt.y, dx0, dy0);
      const walls = canvas.walls.quadtree.getObjects(rect);
      pt = {x: pt.x + dx0, y: pt.y + dy0};
      yield walls;
    }

    // Second half, after the known point
    let dx1 = (ray.B.x - pt.x) / steps1;
    dx1 = dx1 < 0 ? Math.floor(dx1) : Math.ceil(dx1);
    let dy1 = (ray.B.y - pt.y) / steps1;
    dy1 = dy1 < 0 ? Math.floor(dy1) : Math.ceil(dy1);
    for ( let s=0; s<steps1; s++ ) {
      const rect = new NormalizedRectangle(pt.x, pt.y, dx1, dy1);
      const walls = canvas.walls.quadtree.getObjects(rect);
      pt = {x: pt.x + dx1, y: pt.y + dy1};
      yield walls;
    }
  }

  /* -------------------------------------------- */

  /**
   * Test the collision of a single ray against a single wall, using a cached mapping of existing collisions.
   * @param {SightRay} ray            The Ray being tested
   * @param {object} w                The Wall object
   * @returns {WallEndpoint?}         The collision point, if any
   * @private
   */
  _testWall(ray, w) {
    const {wall, a, b} = w;

    // Test whether an intersection occurs
    const i = ray.intersectSegment(wall.data.c);
    if ( !i || (i.t0 <= 0) ) return;

    // Update collision data for the intersection
    const ck = WallEndpoint.getKey(i.x, i.y);
    let c = this.endpoints.get(ck);
    if ( !c ) {
      c = new WallEndpoint(i.x, i.y);
      c.isMidpoint = true;
      this.endpoints.set(ck, c);
    }

    // Record that an existing endpoint may also be a midpoint
    else if ( !(c.equals(a) || c.equals(b)) ) c.isMidpoint = true;

    // Record the collision
    c._r = i.t0;
    c.attachWall(wall);
    return c;
  }

  /* -------------------------------------------- */

  /**
   * Process each recorded collision to decide which are necessary and which can be ignored
   * @param {SightRay} ray                  The ray being tested
   * @param {SightRay} lastRay              The prior ray that was emitted
   * @param {object} result                 The result for the ray currently being evaluated
   * @param {WallEndpoint} point            The collision point being processed
   * @private
   */
  _processCollision(ray, lastRay, result, point) {

    // Determine whether a limited point can safely be ignored
    if ( point.isLimited(this.config.type) ) {
      let {limit, enforce, penetrate} = this._enforceLimitedPoint(ray, lastRay, result, point);
      if (!penetrate) result.stopped = true;    // This collision point stops further progression
      if (limit) result.limitation++;           // This point increments the limitation condition
      if (!enforce) return;                     // Non-enforced points are omitted from the polygon
    }

    // Handle midpoint collisions, which prevent further progress
    if ( point.isMidpoint ) {
      if ( !point.isEndpoint && !this.config.hasRadius ) {  // Midpoint collisions may be superfluous
        const sameWalls = result.activeWalls.intersects(point.walls);
        const isRequired = this.config.isLimited && [this.config.aMin, this.config.aMax].includes(ray.endpoint.angle);
        if (sameWalls && !isRequired) result.superfluous = true;
      }
      result.collisions.push(point);
      result.activeWalls = point.walls;
      result.stopped = true;
      return;
    }

    // Handle endpoint collisions, which allow further penetration
    if ( !result.collisions.length) {           // Always include the first endpoint
      result.collisions.push(point);
      result.activeWalls = point.walls;
      result.continuation = this._checkWallContinuation(ray, point);
    } else {                                    // Include additional endpoints if their continuation differs
      const c = this._checkWallContinuation(ray, point);
      if (( result.continuation.left === c.left ) && (result.continuation.right === c.right)) return;
      result.collisions.push(point);
      result.activeWalls = point.walls;
      result.stopped = true;
    }
  }

  /* -------------------------------------------- */

  /**
   * Determine whether to enforce a limited point as a binding collision or not?
   * @param {SightRay} ray            The current ray being tested
   * @param {SightRay} lastRay        The previous ray that was tested
   * @param {object} result           The ongoing result for the ray
   * @param {WallEndpoint} point      The collision point being considered
   * @returns {{enforce: boolean, penetrate: boolean}}  Enforce the limited point, or allow it to penetrate further?
   * @private
   */
  _enforceLimitedPoint(ray, lastRay, result, point) {

    // If the point is a midpoint, we simply ignore the first one
    const isLimited = result.limitation >= 1;
    if ( point.isMidpoint ) return {
      limit: true,
      enforce: isLimited,
      penetrate: !isLimited
    };

    // Check the continuation of the walls
    const continuation = this._checkWallContinuation(ray, point);

    // Special Case #1: if we are striking an identically aligned wall we need to only count the 2nd collision
    if ( continuation.identical ) {
      if ( result.collisions.length === 0 ) {
        if ( isLimited ) { // Second point
          result.limitation = 0;
          return {limit: false, enforce: true, penetrate: true};
        } else {           // First point
          return {limit: true, enforce: false, penetrate: true};
        }
      }
      return {enforce: false, penetrate: true, limit: false};
    }

    // Special Case #2: we are striking an endpoint which is not part of a closed figure
    const nWalls = point.walls.size;
    if ( nWalls < 2 ) return {
      limit: true,              // Striking the edge of a line counts as limitation, but does not enforce
      enforce: false,
      penetrate: true
    };

    // We may be opening a new figure
    const openNew = !continuation.left && continuation.right
    if ( openNew ) return {
      limit: true,              // Since we are opening into a new figure, this should limit future collisions
      enforce: true,
      penetrate: true
    };

    // We may be closing a figure
    const closeOld = !continuation.right && continuation.left;
    if ( closeOld ) return {
      limit: false,             // Since we are closing out of a figure, this should not limit future collisions
      enforce: lastRay.result.limitation > 0,
      penetrate: true
    };

    // Safety net: never ignore a point with 3+ walls that continues in both directions
    if ( (nWalls >= 3) && continuation.left && continuation.right ) return {
      limit: true,
      enforce: true,
      penetrate: false
    };

    // Otherwise ignore the first point (this should never be reached)
    return {limit: true, enforce: isLimited, penetrate: !isLimited};
  }

  /* -------------------------------------------- */

  /**
   * Test whether walls which join the collision point continue clockwise relative to the tested Ray.
   * @param {SightRay} ray                        The Ray being tested
   * @param {WallEndpoint} point                  The collision point
   * @returns {{left: boolean, right: boolean}}   In which directions do the adjoining walls continue?
   * @private
   */
  _checkWallContinuation(ray, point) {
    if ( point._c ) return point._c;
    const continuation = { left: false, right: false, identical: false };
    for ( let wall of point.walls ) {
      const w = this.walls[wall.id];

      // Create rays for each wall originating from the target point
      const r = point.equals(w.a) ? new Ray(w.a, w.b) : new Ray(w.b, w.a);
      let d = r.angle - ray.angle;
      if ( d < -Math.PI ) d += (2 * Math.PI);

      // The wall is the same angle as the ray
      if ( (d === 0) || (Math.abs(d) === Math.PI) ) continuation.identical = true;

      // Does a wall continue to the right (clockwise)
      else if ( d.between(0, Math.PI) ) continuation.right = true;

      // Does a wall continue to the left (counter-clockwise)
      else continuation.left = true;

      // No further testing is needed
      if ( continuation.left + continuation.right === 2 ) break;
    }
    point._c = continuation;
    return continuation;
  }

  /* -------------------------------------------- */
  /*  Collision Testing                           */
  /* -------------------------------------------- */

  /**
   * Check whether a given ray intersects with walls.
   * @param {Ray} ray                   The Ray being tested
   * @param {object} [options={}]       Options which customize how collision is tested
   * @param {string} [options.type=movement]        Which collision type to check: movement, sight, sound
   * @param {string} [options.mode=any]             Which type of collisions are returned: any, closest, all
   * @return {object[]|object|boolean}  An array of collisions, if mode is "all"
   *                                    The closest collision, if mode is "closest"
   *                                    Whether any collision occurred if mode is "any"
   */
  static getRayCollisions(ray, {type="move", mode="all", steps=8}={}) {

    // Record collision points and tested walls
    const angleBounds = [ray.angle - (Math.PI / 2), ray.angle + (Math.PI / 2)];
    const collisionPoints = new Map();
    const testedWalls = new Set();

    // Progressively test walls along ray segments
    let dx = ray.dx / steps;
    let dy = ray.dy / steps;
    let pt = ray.A;
    let step = 0;
    while (step < steps) {
      step++;
      const testRect = new NormalizedRectangle(pt.x, pt.y, dx, dy);
      let walls = canvas.walls.quadtree.getObjects(testRect);
      pt = {x: pt.x + dx, y: pt.y + dy};
      for (let wall of walls) {

        // Don't repeat tests
        if (testedWalls.has(wall)) continue;
        testedWalls.add(wall);

        // Ignore walls of the wrong type or open doors
        if (!wall.data[type] || wall.isOpen) continue;

        // Ignore one-way walls which are facing the wrong direction
        if ((wall.direction !== null) && !wall.isDirectionBetweenAngles(...angleBounds)) continue;

        // Test whether an intersection occurs
        const i = ray.intersectSegment(wall.data.c);
        if (!i || (i.t0 <= 0)) continue;

        // We may only need one
        if ( mode === "any" ) return true;

        // Record the collision point if an intersection occurred
        const c = new WallEndpoint(i.x, i.y);
        collisionPoints.set(c.key, c);
      }
      if (collisionPoints.size && (mode === "closest")) break;
    }

    // Return the result based on the test type
    switch (mode) {
      case "all":
        return Array.from(collisionPoints.values());
      case "any":
        return collisionPoints.size > 0;
      case "closest":
        if (!collisionPoints.size) return null;
        const sortedPoints = Array.from(collisionPoints.values()).sort((a, b) => a.t0 - b.t0);
        if (sortedPoints[0].isLimited(type)) sortedPoints.shift();
        return sortedPoints[0] || null;
    }
  }
}
