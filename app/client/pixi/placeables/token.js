/**
 * A Token is an implementation of PlaceableObject which represents an Actor within a viewed Scene on the game canvas.
 * @extends  {PlaceableObject}
 * @see {TokenDocument}
 * @see {TokenLayer}
 */
class Token extends PlaceableObject {
  constructor(...args) {
    super(...args);

    /**
     * A Ray which represents the Token's current movement path
     * @type {Ray}
     * @private
     */
    this._movement = null;

    /**
     * An Object which records the Token's prior velocity dx and dy
     * This can be used to determine which direction a Token was previously moving
     * @type {{dx: number, dy: number, sx: number, sy:number}}
     * @private
     */
    this._velocity = this._getInitialVelocity();

    /**
     * The Token's most recent valid position
     * @type {Object}
     * @private
     */
    this._validPosition = {x: this.data.x, y: this.data.y};

    /**
     * Track the set of User documents which are currently targeting this Token
     * @type {Set.<User>}
     */
    this.targeted = new Set([]);

    /**
     * A reference to the VisionSource object which defines this vision source area of effect
     * @type {VisionSource}
     */
    this.vision = new VisionSource(this);

    /**
     * A reference to the LightSource object which defines this light source area of effect
     * @type {LightSource}
     */
    this.light = new LightSource(this);

    /**
     * A linked ObjectHUD element which is synchronized with the location and visibility of this Token
     * @type {ObjectHUD}
     */
    this.hud = new ObjectHUD(this);
  }

  /**
   * The UI frame container which depicts Token metadata and status, displayed in the ControlsLayer.
   * @type {ObjectHUD}
   * @property {PIXI.Container} bars        Token health bars
   * @property {PreciseText} nameplate      Token nameplate
   * @property {PreciseText} tooltip        Token elevation tooltip
   * @property {PIXI.Container} effects     Token status effects
   * @property {PIXI.Graphics} target       Token target marker
   */
  hud;

  /** @inheritdoc */
  static embeddedName = "Token";

  /* -------------------------------------------- */

  /**
   * Establish an initial velocity of the token based on it's direction of facing.
   * Assume the Token made some prior movement towards the direction that it is currently facing.
   * @returns {{dx: number, sx: number, dy: number, sy: number}}
   * @private
   */
  _getInitialVelocity() {
    this._velocity = {dx: 0, dy: 0, sx: 0.1, sy: 0.1}; // Assume top-left towards bottom-right by default
    const r = Ray.fromAngle(this.data.x, this.data.y, Math.toRadians(this.data.rotation + 90), canvas.dimensions.size);
    return this._updateVelocity(r);
  }

  /* -------------------------------------------- */
  /*  Permission Attributes
  /* -------------------------------------------- */

  /**
   * A convenient reference to the Actor object associated with the Token embedded document.
   * @returns {Actor|null}
   */
  get actor() {
    return this.document.actor;
  }

  /* -------------------------------------------- */

  /**
   * A convenient reference for whether the current User has full control over the Token document.
   * @type {boolean}
   */
  get owner() {
    return this.document.isOwner;
  }

  get isOwner() {
    return this.document.isOwner;
  }

  /* -------------------------------------------- */

  /**
   * A boolean flag for whether the current game User has observer permission for the Token
   * @type {boolean}
   */
  get observer() {
    return game.user.isGM || !!this.actor?.testUserPermission(game.user, "OBSERVER");
  }

  /* -------------------------------------------- */

  /**
   * Is the HUD display active for this token?
   * @return {boolean}
   */
  get hasActiveHUD() {
    return this.layer.hud.object === this;
  }

  /* -------------------------------------------- */

  /**
   * Convenience access to the token's nameplate string
   * @type {string}
   */
  get name() {
    return this.document.name;
  }

  /* -------------------------------------------- */
  /*  Rendering Attributes
  /* -------------------------------------------- */

  /** @override */
  get bounds() {
    return new NormalizedRectangle(this.data.x, this.data.y, this.w, this.h);
  }

  /* -------------------------------------------- */

  /**
   * Translate the token's grid width into a pixel width based on the canvas size
   * @type {number}
   */
  get w() {
    return this.data.width * canvas.grid.w;
  }

  /* -------------------------------------------- */

  /**
   * Translate the token's grid height into a pixel height based on the canvas size
   * @type {number}
   */
  get h() {
    return this.data.height * canvas.grid.h;
  }

  /* -------------------------------------------- */

  /**
   * The Token's current central position
   * @property x The central x-coordinate
   * @property y The central y-coordinate
   * @type {Object}
   */
  get center() {
    return this.getCenter(this.data.x, this.data.y);
  }

  /* -------------------------------------------- */

  /**
   * The HTML source element for the primary Tile texture
   * @type {HTMLImageElement|HTMLVideoElement}
   */
  get sourceElement() {
    return this.texture?.baseTexture.resource.source;
  }

  /* -------------------------------------------- */

  /**
   * Does this Tile depict an animated video texture?
   * @type {boolean}
   */
  get isVideo() {
    const source = this.sourceElement;
    return source?.tagName === "VIDEO";
  }

  /* -------------------------------------------- */
  /*  State Attributes
  /* -------------------------------------------- */

  /**
   * An indicator for whether or not this token is currently involved in the active combat encounter.
   * @type {boolean}
   */
  get inCombat() {
    return this.document.inCombat;
  }

  /* -------------------------------------------- */

  /**
   * Return a reference to a Combatant that represents this Token, if one is present in the current encounter.
   * @type {Combatant|null}
   */
  get combatant() {
    return this.document.combatant;
  }

  /* -------------------------------------------- */

  /**
   * An indicator for whether the Token is currently targeted by the active game User
   * @type {boolean}
   */
  get isTargeted() {
    return this.targeted.has(game.user);
  }

  /* -------------------------------------------- */

  /**
   * Determine whether the Token is visible to the calling user's perspective.
   * Hidden Tokens are only displayed to GM Users.
   * Non-hidden Tokens are always visible if Token Vision is not required.
   * Controlled tokens are always visible.
   * All Tokens are visible to a GM user if no Token is controlled.
   *
   * @see {SightLayer#testVisibility}
   * @type {boolean}
   */
  get isVisible() {

    // Only GM users can see hidden tokens
    const gm = game.user.isGM;
    if ( this.data.hidden && !gm ) return false;

    // Some tokens are always visible
    if ( !canvas.sight.tokenVision ) return true;
    if ( this._controlled ) return true;

    // Otherwise test visibility against current sight polygons
    if ( canvas.sight.sources.has(this.sourceId) ) return true;
    const tolerance = Math.min(this.w, this.h) / 4;
    return canvas.sight.testVisibility(this.center, {tolerance, object: this});
  }

  /* -------------------------------------------- */

  /**
   * The animation name used for Token movement
   * @type {string}
   */
  get movementAnimationName() {
    return `Token.${this.id}.animateMovement`;
  }

  /* -------------------------------------------- */
  /*  Lighting and Vision Attributes
  /* -------------------------------------------- */

  /**
   * Test whether the Token has sight (or blindness) at any radius
   * @type {boolean}
   */
  get hasSight() {
    return this.data.vision;
  }

  /* -------------------------------------------- */

  /**
   * Test whether the Token emits light (or darkness) at any radius
   * @type {boolean}
   */
  get emitsLight() {
    return !!(this.data.light.dim || this.data.light.bright);
  }

  /* -------------------------------------------- */

  /**
   * Test whether the Token has a limited angle of vision or light emission which would require sight to update on Token rotation
   * @type {boolean}
   */
  get hasLimitedVisionAngle() {
    return (this.hasSight && (this.data.sightAngle !== 360)) || (this.emitsLight && (this.data.light.angle !== 360));
  }

  /* -------------------------------------------- */

  /**
   * Translate the token's sight distance in units into a radius in pixels.
   * @return {number}     The sight radius in pixels
   */
  get dimRadius() {
    let r = Math.abs(this.data.dimLight) > Math.abs(this.data.dimSight) ? this.data.dimLight : this.data.dimSight;
    return this.getLightRadius(r);
  }

  /* -------------------------------------------- */

  /**
   * Translate the token's bright light distance in units into a radius in pixels.
   * @return {number}       The bright radius in pixels
   */
  get brightRadius() {
    let r = Math.abs(this.data.brightLight) > Math.abs(this.data.brightSight) ? this.data.brightLight :
      this.data.brightSight;
    return this.getLightRadius(r);
  }

  /* -------------------------------------------- */

  /**
   * The named identified for the source object associated with this Token
   * @return {string}
   */
  get sourceId() {
    return `${this.document.documentName}.${this._original?.id ?? this.document.id ?? "preview"}`;
  }

  /* -------------------------------------------- */

  /**
   * Update the light and vision source objects associated with this Token
   * @param {boolean} [defer]           Defer refreshing the SightLayer to manually call that refresh later.
   * @param {boolean} [deleted]         Indicate that this light source has been deleted.
   * @param {boolean} [skipUpdateFog]   Never update the Fog exploration progress for this update.
   */
  updateSource({defer=false, deleted=false, skipUpdateFog=false}={}) {
    this.updateLightSource({defer, deleted});
    this.updateVisionSource({defer, deleted, skipUpdateFog});
  }

  /* -------------------------------------------- */

  /**
   * Update an emitted light source associated with this Token.
   * @param {boolean} [defer]           Defer refreshing the LightingLayer to manually call that refresh later.
   * @param {boolean} [deleted]         Indicate that this light source has been deleted.
   */
  updateLightSource({defer=false, deleted=false}={}) {

    // Prepare data
    const origin = this.getSightOrigin();
    const sourceId = this.sourceId;
    const d = canvas.dimensions;
    const isLightSource = this.emitsLight && !this.data.hidden;

    // Initialize a light source
    if ( isLightSource && !deleted ) {
      const lightConfig = foundry.utils.mergeObject(this.data.light.toObject(false), {
        x: origin.x,
        y: origin.y,
        dim: Math.clamped(this.getLightRadius(this.data.light.dim), 0, d.maxR),
        bright: Math.clamped(this.getLightRadius(this.data.light.bright), 0, d.maxR),
        z: this.document.getFlag("core", "priority"),
        seed: this.document.getFlag("core", "animationSeed"),
        rotation: this.data.rotation
      });
      this.light.initialize(lightConfig);
      canvas.lighting.sources.set(sourceId, this.light);
    }

    // Remove a light source
    else canvas.lighting.sources.delete(sourceId);

    // Schedule a perception update
    if ( !defer && (isLightSource || deleted ) ) {
      canvas.perception.schedule({
        lighting: {refresh: true},
        sight: {refresh: true}
      });
    }
  }

  /* -------------------------------------------- */

  /**
   * Update an Token vision source associated for this token.
   * @param {boolean} [defer]           Defer refreshing the LightingLayer to manually call that refresh later.
   * @param {boolean} [deleted]         Indicate that this vision source has been deleted.
   * @param {boolean} [skipUpdateFog]   Never update the Fog exploration progress for this update.
   */
  updateVisionSource({defer=false, deleted=false, skipUpdateFog=false}={}) {

    // Prepare data
    const origin = this.getSightOrigin();
    const sourceId = this.sourceId;
    const d = canvas.dimensions;
    const isVisionSource = this._isVisionSource();

    // Initialize vision source
    if ( isVisionSource && !deleted ) {
      this.vision.initialize({
        x: origin.x,
        y: origin.y,
        dim: Math.clamped(this.getLightRadius(this.data.dimSight), 0, d.maxR),
        bright: Math.clamped(this.getLightRadius(this.data.brightSight), 0, d.maxR),
        angle: this.data.sightAngle,
        rotation: this.data.rotation
      });
      canvas.sight.sources.set(sourceId, this.vision);
    }

    // Remove vision source
    else canvas.sight.sources.delete(sourceId);

    // Schedule a perception update
    if ( !defer && (isVisionSource || deleted) ) canvas.perception.schedule({
      sight: {refresh: true, skipUpdateFog}
    });
  }

  /* -------------------------------------------- */

  /**
   * Test whether this Token is a viable vision source for the current User
   * @return {boolean}
   * @private
   */
  _isVisionSource() {
    if ( !canvas.sight.tokenVision || !this.hasSight ) return false;

    // Only display hidden tokens for the GM
    const isGM = game.user.isGM;
    if (this.data.hidden && !isGM) return false;

    // Always display controlled tokens which have vision
    if ( this._controlled ) return true;

    // Otherwise vision is ignored for GM users
    if ( isGM ) return false;

    // If a non-GM user controls no other tokens with sight, display sight anyways
    const canObserve = this.actor?.testUserPermission(game.user, "OBSERVER") ?? false;
    if ( !canObserve ) return false;
    const others = this.layer.controlled.filter(t => !t.data.hidden && t.hasSight);
    return !others.length;
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /** @override */
  clear() {
    if ( this.hasActiveHUD ) canvas.tokens.hud.clear();
    return super.clear();
  }

  /* -------------------------------------------- */

  /** @override */
  async draw() {
    this.clear();

    // Clean initial data
    this._cleanData();

    // Draw the token as invisible so it will be safely revealed later
    this.visible = false;

    // Load token texture
    this.texture = await loadTexture(this.data.img, {fallback: CONST.DEFAULT_TOKEN});

    // Draw Token components
    this.border = this.addChild(new PIXI.Graphics());
    this.icon = this.addChild(await this._drawIcon());

    // Draw the HUD interface
    this._drawHUD();

    // Define initial interactivity and visibility state
    this.hitArea = new PIXI.Rectangle(0, 0, this.w, this.h);
    this.buttonMode = true;

    // Begin video playback
    if ( this.isVideo ) {
      await this._unlinkVideoPlayback(this.sourceElement);
      this.play(true);
    }

    // Draw the initial position
    this.refresh();
    await this.drawEffects();
    this.drawBars();

    // Enable interactivity, only if the Tile has a true ID
    if ( this.id ) this.activateListeners();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Draw the HUD container which provides an interface for managing this Token
   * @returns {ObjectHUD}
   * @private
   */
  _drawHUD() {
    if ( !this.hud.parent ) canvas.controls.hud.addChild(this.hud);
    this.hud.removeChildren();
    this.hud.bars = this.hud.addChild(this._drawAttributeBars());
    this.hud.tooltip = this.hud.addChild(this._drawTooltip());
    this.hud.effects = this.hud.addChild(new PIXI.Container());
    this.hud.target = this.hud.addChild(new PIXI.Graphics());
    this.hud.nameplate = this.hud.addChild(this._drawNameplate());
    return this.hud;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  destroy(options) {
    this.light.destroy();
    this.vision.destroy();
    this.hud.destroy({children: true});
    if ( this.isVideo ) this.texture.baseTexture?.destroy();
    return super.destroy(options);
  }

  /* -------------------------------------------- */

  /**
   * Apply initial sanitizations to the provided input data to ensure that a Token has valid required attributes.
   * @private
   */
  _cleanData() {
    // Constrain canvas coordinates
    if ( !canvas || !this.scene?.active ) return;
    const d = canvas.dimensions;
    this.data.x = Math.clamped(Math.round(this.data.x), 0, d.width - this.w);
    this.data.y = Math.clamped(Math.round(this.data.y), 0, d.height - this.h);
  }

  /* -------------------------------------------- */

  /**
   * Draw resource bars for the Token
   * @private
   */
  _drawAttributeBars() {
    const bars = new PIXI.Container();
    bars.bar1 = bars.addChild(new PIXI.Graphics());
    bars.bar2 = bars.addChild(new PIXI.Graphics());
    return bars;
  }

  /* -------------------------------------------- */

  /**
   * Draw the Sprite icon for the Token
   * @return {Promise<PIXI.Sprite>}
   * @private
   */
  async _drawIcon() {
    let icon = new PIXI.Sprite(this.texture);
    icon.anchor.set(0.5, 0.5);
    if ( !this.texture ) return icon;
    icon.tint = this.data.tint ? foundry.utils.colorStringToHex(this.data.tint) : 0xFFFFFF;
    icon.visible = false;
    return icon;
  }

  /* -------------------------------------------- */

  /**
   * Play video for this Token (if applicable).
   * @param {boolean} [playing]     Should the Token video be playing?
   * @param {object} [options={}]   Additional options for modifying video playback
   * @param {boolean} [options.loop]    Should the video loop?
   * @param {number} [options.offset]   A specific timestamp between 0 and the video duration to begin playback
   * @param {number} [options.volume]   Desired volume level of the video's audio channel (if any)
   */
  play(playing=true, {loop=true, offset=0, volume=0}={}) {
    const el = this.sourceElement;
    if ( el?.tagName !== "VIDEO" ) return;
    el.loop = loop;
    el.volume = volume;
    el.muted = el.volume === 0;
    el.currentTime = typeof offset === "number" ? Math.clamped(offset, 0, el.duration) : 0;
    if ( playing ) game.video.play(el);
    else el.pause();
  }

  /* -------------------------------------------- */

  /**
   * Unlink the playback of this video token from the playback of other tokens which are using the same base texture.
   * @param {HTMLVideoElement} source     The video element source
   * @returns {Promise<void>}
   * @private
   */
  async _unlinkVideoPlayback(source) {
    const s = source.cloneNode();
    s.onplay = () => s.currentTime = Math.random() * source.duration;
    await new Promise(resolve => s.oncanplay = resolve);
    this.texture = this.icon.texture = PIXI.Texture.from(s, {resourceOptions: {autoPlay: false}});
  }

  /* -------------------------------------------- */

  /**
   * Update display of the Token, pulling latest data and re-rendering the display of Token components
   */
  refresh() {
    // Token position and visibility
    if ( !this._movement ) this.position.set(this.data.x, this.data.y);

    // Refresh Token components
    if ( this.icon ) this._refreshIcon();
    if ( this.border ) this._refreshBorder();
    if ( this.hud ) this.refreshHUD();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Size and display the Token Icon
   * @private
   */
  _refreshIcon() {
    // Size the texture aspect ratio within the token frame
    const tex = this.texture;
    let aspect = tex.width / tex.height;
    const scale = this.icon.scale;
    if (aspect >= 1) {
      this.icon.width = this.w * this.data.scale;
      scale.y = Number(scale.x);
    } else {
      this.icon.height = this.h * this.data.scale;
      scale.x = Number(scale.y);
    }

    // Mirror horizontally or vertically
    this.icon.scale.x = Math.abs(this.icon.scale.x) * (this.data.mirrorX ? -1 : 1);
    this.icon.scale.y = Math.abs(this.icon.scale.y) * (this.data.mirrorY ? -1 : 1);

    // Set rotation, position, and opacity
    this.icon.rotation = this.data.lockRotation ? 0 : Math.toRadians(this.data.rotation);
    this.icon.position.set(this.w / 2, this.h / 2);
    this.icon.alpha = this.data.hidden ? Math.min(this.data.alpha, 0.5) : this.data.alpha;
    this.icon.visible = true;
  }

  /* -------------------------------------------- */

  /**
   * Draw the Token border, taking into consideration the grid type and border color
   * @private
   */
  _refreshBorder() {
    this.border.clear();
    const borderColor = this._getBorderColor();
    if( !borderColor ) return;
    const t = CONFIG.Canvas.objectBorderThickness;

    // Draw Hex border for size 1 tokens on a hex grid
    const gt = CONST.GRID_TYPES;
    const hexTypes = [gt.HEXEVENQ, gt.HEXEVENR, gt.HEXODDQ, gt.HEXODDR];
    if ( hexTypes.includes(canvas.grid.type) && (this.data.width === 1) && (this.data.height === 1) ) {
      const polygon = canvas.grid.grid.getPolygon(-1, -1, this.w+2, this.h+2);
      this.border.lineStyle(t, 0x000000, 0.8).drawPolygon(polygon);
      this.border.lineStyle(t/2, borderColor, 1.0).drawPolygon(polygon);
    }

    // Otherwise Draw Square border
    else {
      const h = Math.round(t/2);
      const o = Math.round(h/2);
      this.border.lineStyle(t, 0x000000, 0.8).drawRoundedRect(-o, -o, this.w+h, this.h+h, 3);
      this.border.lineStyle(h, borderColor, 1.0).drawRoundedRect(-o, -o, this.w+h, this.h+h, 3);
    }
  }

  /* -------------------------------------------- */

  /**
   * Get the hex color that should be used to render the Token border
   * @return {number|null}   The hex color used to depict the border color
   * @private
   */
  _getBorderColor() {
    const colors = CONFIG.Canvas.dispositionColors;
    if ( this._controlled ) return colors.CONTROLLED;
    else if ( this._hover ) {
      let d = parseInt(this.data.disposition);
      if (!game.user.isGM && this.isOwner) return colors.CONTROLLED;
      else if (this.actor?.hasPlayerOwner) return colors.PARTY;
      else if (d === CONST.TOKEN_DISPOSITIONS.FRIENDLY) return colors.FRIENDLY;
      else if (d === CONST.TOKEN_DISPOSITIONS.NEUTRAL) return colors.NEUTRAL;
      else return colors.HOSTILE;
    }
    else return null;
  }

  /* -------------------------------------------- */

  /**
   * Refresh the display of the Token HUD interface.
   */
  refreshHUD() {
    this._refreshTarget();
    this.hud.nameplate.visible = this._canViewMode(this.data.displayName);
    this.hud.bars.visible = this._canViewMode(this.data.displayBars);
  }

  /* -------------------------------------------- */

  /**
   * Refresh the target indicators for the Token.
   * Draw both target arrows for the primary User as well as indicator pips for other Users targeting the same Token.
   * @private
   */
  _refreshTarget() {
    this.hud.target.clear();
    if ( !this.targeted.size ) return;

    // Determine whether the current user has target and any other users
    const [others, user] = Array.from(this.targeted).partition(u => u === game.user);
    const userTarget = user.length;

    // For the current user, draw the target arrows
    if ( userTarget ) {
      let p = 4;
      let aw = 12;
      let h = this.h;
      let hh = h / 2;
      let w = this.w;
      let hw = w / 2;
      let ah = canvas.dimensions.size / 3;
      this.hud.target.beginFill(0xFF9829, 1.0).lineStyle(1, 0x000000)
        .drawPolygon([-p,hh, -p-aw,hh-ah, -p-aw,hh+ah])
        .drawPolygon([w+p,hh, w+p+aw,hh-ah, w+p+aw,hh+ah])
        .drawPolygon([hw,-p, hw-ah,-p-aw, hw+ah,-p-aw])
        .drawPolygon([hw,h+p, hw-ah,h+p+aw, hw+ah,h+p+aw]);
    }

    // For other users, draw offset pips
    for ( let [i, u] of others.entries() ) {
      let color = foundry.utils.colorStringToHex(u.data.color);
      this.hud.target.beginFill(color, 1.0).lineStyle(2, 0x0000000).drawCircle(2 + (i * 8), 0, 6);
    }
  }

  /* -------------------------------------------- */

  /**
   * Refresh the display of Token attribute bars, rendering latest resource data
   * If the bar attribute is valid (has a value and max), draw the bar. Otherwise hide it.
   * @private
   */
  drawBars() {
    if ( !this.actor || (this.data.displayBars === CONST.TOKEN_DISPLAY_MODES.NONE) ) return;
    ["bar1", "bar2"].forEach((b, i) => {
      const bar = this.hud.bars[b];
      const attr = this.document.getBarAttribute(b);
      if ( !attr || (attr.type !== "bar") ) return bar.visible = false;
      this._drawBar(i, bar, attr);
      bar.visible = true;
    });
  }

  /* -------------------------------------------- */

  /**
   * Draw a single resource bar, given provided data
   * @param {number} number       The Bar number
   * @param {PIXI.Graphics} bar   The Bar container
   * @param {Object} data         Resource data for this bar
   * @protected
   */
  _drawBar(number, bar, data) {
    const val = Number(data.value);
    const pct = Math.clamped(val, 0, data.max) / data.max;

    // Determine sizing
    let h = Math.max((canvas.dimensions.size / 12), 8);
    const w = this.w;
    const bs = Math.clamped(h / 8, 1, 2);
    if ( this.data.height >= 2 ) h *= 1.6;  // Enlarge the bar for large tokens

    // Determine the color to use
    const blk = 0x000000;
    let color;
    if ( number === 0 ) color = PIXI.utils.rgb2hex([(1-(pct/2)), pct, 0]);
    else color = PIXI.utils.rgb2hex([(0.5 * pct), (0.7 * pct), 0.5 + (pct / 2)]);

    // Draw the bar
    bar.clear()
    bar.beginFill(blk, 0.5).lineStyle(bs, blk, 1.0).drawRoundedRect(0, 0, this.w, h, 3)
    bar.beginFill(color, 1.0).lineStyle(bs, blk, 1.0).drawRoundedRect(0, 0, pct*w, h, 2)

    // Set position
    let posY = number === 0 ? this.h - h : 0;
    bar.position.set(0, posY);
  }

  /* -------------------------------------------- */

  /**
   * Draw the token's nameplate as a text object
   * @return {PIXI.Text}  The Text object for the Token nameplate
   */
  _drawNameplate() {
    const style = this._getTextStyle();
    const name = new PreciseText(this.data.name, style);
    name.anchor.set(0.5, 0);
    name.position.set(this.w / 2, this.h + 2);
    return name;
  }

  /* -------------------------------------------- */

  /**
   * Draw a text tooltip for the token which can be used to display Elevation or a resource value
   * @returns {PreciseText}     The text object used to render the tooltip
   * @private
   */
  _drawTooltip() {
    let text = this._getTooltipText();
    const style = this._getTextStyle();
    const tip = new PreciseText(text, style);
    tip.anchor.set(0.5, 1);
    tip.position.set(this.w / 2, -2);
    return tip;
  }

  /* -------------------------------------------- */

  /**
   * Return the text which should be displayed in a token's tooltip field
   * @return {string}
   * @private
   */
  _getTooltipText() {
    let el = this.data.elevation;
    if (!Number.isFinite(el) || el === 0) return "";
    let units = canvas.scene.data.gridUnits;
    return el > 0 ? `+${el} ${units}` : `${el} ${units}`;
  }

  /* -------------------------------------------- */

  _getTextStyle() {
    const style = CONFIG.canvasTextStyle.clone();
    style.fontSize = 24;
    if (canvas.dimensions.size >= 200) style.fontSize = 28;
    else if (canvas.dimensions.size < 50) style.fontSize = 20;
    style.wordWrapWidth = this.w * 2.5;
    return style
  }

  /* -------------------------------------------- */

  /**
   * Draw the active effects and overlay effect icons which are present upon the Token
   */
  async drawEffects() {
    this.hud.effects.removeChildren().forEach(c => c.destroy());
    const tokenEffects = this.data.effects;
    const actorEffects = this.actor?.temporaryEffects || [];
    let overlay = {
      src: this.data.overlayEffect,
      tint: null
    };

    // Draw status effects
    if ( tokenEffects.length || actorEffects.length ) {
      const promises = [];
      let w = Math.round(canvas.dimensions.size / 2 / 5) * 2;
      let bg = this.hud.effects.addChild(new PIXI.Graphics()).beginFill(0x000000, 0.40).lineStyle(1.0, 0x000000);
      let i = 0;

      // Draw actor effects first
      for ( let f of actorEffects ) {
        if ( !f.data.icon ) continue;
        const tint = f.data.tint ? foundry.utils.colorStringToHex(f.data.tint) : null;
        if ( f.getFlag("core", "overlay") ) {
          overlay = {src: f.data.icon, tint};
          continue;
        }
        promises.push(this._drawEffect(f.data.icon, i, bg, w, tint));
        i++;
      }

      // Next draw token effects
      for ( let f of tokenEffects ) {
        promises.push(this._drawEffect(f, i, bg, w, null));
        i++;
      }
      await Promise.all(promises);
    }

    // Draw overlay effect
    return this._drawOverlay(overlay)
  }

  /* -------------------------------------------- */

  /**
   * Draw the overlay effect icon
   * @return {Promise<void>}
   * @private
   */
  async _drawOverlay({src, tint}={}) {
    if ( !src ) return;
    const tex = await loadTexture(src, {fallback: 'icons/svg/hazard.svg'});
    const icon = new PIXI.Sprite(tex);
    const size = Math.min(this.w * 0.6, this.h * 0.6);
    icon.width = icon.height = size;
    icon.position.set((this.w - size) / 2, (this.h - size) / 2);
    icon.alpha = 0.80;
    if ( tint ) icon.tint = tint;
    this.hud.effects.addChild(icon);
  }

  /* -------------------------------------------- */

  /**
   * Draw a status effect icon
   * @return {Promise<void>}
   * @private
   */
  async _drawEffect(src, i, bg, w, tint) {
    let tex = await loadTexture(src, {fallback: 'icons/svg/hazard.svg'});
    let icon = this.hud.effects.addChild(new PIXI.Sprite(tex));
    icon.width = icon.height = w;
    const nr = Math.floor(this.data.height * 5);
    icon.x = Math.floor(i / nr) * w;
    icon.y = (i % nr) * w;
    if ( tint ) icon.tint = tint;
    bg.drawRoundedRect(icon.x + 1, icon.y + 1, w - 2, w - 2, 2);
  }

  /* -------------------------------------------- */

  /**
   * Helper method to determine whether a token attribute is viewable under a certain mode
   * @param {number} mode   The mode from CONST.TOKEN_DISPLAY_MODES
   * @return {boolean}      Is the attribute viewable?
   * @private
   */
  _canViewMode(mode) {
    if ( mode === CONST.TOKEN_DISPLAY_MODES.NONE ) return false;
    else if ( mode === CONST.TOKEN_DISPLAY_MODES.ALWAYS ) return true;
    else if ( mode === CONST.TOKEN_DISPLAY_MODES.CONTROL ) return this._controlled;
    else if ( mode === CONST.TOKEN_DISPLAY_MODES.HOVER ) return this._hover;
    else if ( mode === CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER ) return this.isOwner && this._hover;
    else if ( mode === CONST.TOKEN_DISPLAY_MODES.OWNER ) return this.isOwner;
    return false;
  }

  /* -------------------------------------------- */

  /**
   * Animate Token movement along a certain path which is defined by a Ray object
   * @param {Ray} ray   The path along which to animate Token movement
   */
  async animateMovement(ray) {

    // Move distance is 10 spaces per second
    const s = canvas.dimensions.size;
    this._movement = ray;
    const speed = s * 10;
    const duration = (ray.distance * 1000) / speed;

    // Define attributes
    const attributes = [
      { parent: this, attribute: 'x', to: ray.B.x },
      { parent: this, attribute: 'y', to: ray.B.y }
    ];

    // Determine what type of updates should be animated
    const emits = this.emitsLight;
    const config = {
      animate: game.settings.get("core", "visionAnimation"),
      source: this._isVisionSource() || emits,
      sound: this._controlled || this.observer,
      forceUpdateFog: emits && !this._controlled && (canvas.sight.sources.size > 0)
    }

    // Dispatch the animation function
    await CanvasAnimation.animateLinear(attributes, {
      name: this.movementAnimationName,
      context: this,
      duration: duration,
      ontick: (dt, anim) => this._onMovementFrame(dt, anim, config)
    });

    // Once animation is complete perform a final refresh
    if ( !config.animate ) this._animatePerceptionFrame({source: config.source, sound: config.sound});
    this._movement = null;
  }

  /* -------------------------------------------- */

  /**
   * Animate the continual revealing of Token vision during a movement animation
   * @private
   */
  _onMovementFrame(dt, anim, config) {
    this.data.x = this.x;
    this.data.y = this.y;
    if ( !config.animate || !anim.length ) return;
    this._animatePerceptionFrame(config);
  }

  /* -------------------------------------------- */

  /**
   * Update perception each frame depending on the animation configuration
   * @private
   */
  _animatePerceptionFrame({source=false, sound=false, forceUpdateFog=false}={}) {
    if ( source ) this.updateSource({defer: true});
    canvas.perception.update({  // run immediately
      lighting: {refresh: source},
      sight: {refresh: source, forceUpdateFog},
      sounds: {refresh: sound},
      foreground: {refresh: true}
    });
  }

  /* -------------------------------------------- */

  /**
   * Terminate animation of this particular Token
   */
  stopAnimation() {
    return CanvasAnimation.terminateAnimation(`Token.${this.id}.animateMovement`);
  }

  /* -------------------------------------------- */
  /*  Methods
  /* -------------------------------------------- */

  /**
   * Check for collision when attempting a move to a new position
   * @param {Point} destination   The destination point of the attempted movement
   * @return {boolean}            A true/false indicator for whether the attempted movement caused a collision
   */
  checkCollision(destination) {

    // Create a Ray for the attempted move
    let origin = this.getCenter(...Object.values(this._validPosition));
    let ray = new Ray({x: origin.x, y: origin.y}, {x: destination.x, y: destination.y});

    // Shift the origin point by the prior velocity
    ray.A.x -= this._velocity.sx;
    ray.A.y -= this._velocity.sy;

    // Shift the destination point by the requested velocity
    ray.B.x -= Math.sign(ray.dx);
    ray.B.y -= Math.sign(ray.dy);

    // Check for a wall collision
    return canvas.walls.checkCollision(ray);
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onControl({releaseOthers=true, pan=false}={}) {
    _token = this;
    this.zIndex = 1;
    this.refresh();
    if ( pan ) canvas.addPendingOperation("Canvas.animatePan", canvas.animatePan, canvas, [{x: this.x, y: this.y}]);
    canvas.perception.schedule({
      sight: {
        initialize: true,
        refresh: true,
        forceUpdateFog: true // Update exploration even if the token hasn't moved
      },
      lighting: {refresh: true},
      sounds: {refresh: true},
      foreground: {refresh: true}
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onRelease(options) {
    super._onRelease(options);
    this.zIndex = 0;
    canvas.perception.schedule({
      sight: {initialize: true, refresh: true},
      lighting: {refresh: true},
      sounds: {refresh: true},
      foreground: {refresh: true}
    });
  }

  /* -------------------------------------------- */

  /**
   * Get the center-point coordinate for a given grid position
   * @param {number} x    The grid x-coordinate that represents the top-left of the Token
   * @param {number} y    The grid y-coordinate that represents the top-left of the Token
   * @return {Object}     The coordinate pair which represents the Token's center at position (x, y)
   */
  getCenter(x, y) {
    return {
      x: x + (this.w / 2),
      y: y + (this.h / 2)
    };
  }

  /* -------------------------------------------- */

  /**
   * Set the token's position by comparing its center position vs the nearest grid vertex
   * Return a Promise that resolves to the Token once the animation for the movement has been completed
   * @param {number} x            The x-coordinate of the token center
   * @param {number} y            The y-coordinate of the token center
   * @param {object} [options={}] Additional options which configure the token movement
   * @param {boolean} [options.animate=true]    Animate the movement path
   * @param {boolean} [options.recenter=true]   Automatically re-center the view if token movement goes off-screen
   * @return {Promise<Token>}     The Token after animation has completed
   */
  async setPosition(x, y, {animate=true, recenter=true}={}) {

    // Create a Ray for the requested movement
    let origin = this._movement ? this.position : this._validPosition,
        target = {x: x, y: y},
        isVisible = this.isVisible;

    // Create the movement ray
    let ray = new Ray(origin, target);

    // Update the new valid position
    this._validPosition = target;

    // Record the Token's new velocity
    this._velocity = this._updateVelocity(ray);

    // Update visibility for a non-controlled token which may have moved into the controlled tokens FOV
    this.visible = isVisible;

    // Conceal the HUD if it targets this Token
    if ( this.hasActiveHUD ) this.layer.hud.clear();

    // Either animate movement to the destination position, or set it directly if animation is disabled
    if ( animate ) await this.animateMovement(new Ray(this.position, ray.B));
    else this.position.set(x, y);

    // If the movement took a controlled token off-screen, re-center the view
    if ( this._controlled && isVisible && recenter ) {
      const pad = 50;
      const sidebarPad = $("#sidebar").width() + pad;
      const rect = new PIXI.Rectangle(pad, pad, window.innerWidth - sidebarPad, window.innerHeight - pad);
      let gp = this.getGlobalPosition();
      if ( !rect.contains(gp.x, gp.y) ) {
        // noinspection ES6MissingAwait
        canvas.animatePan(this.center);
      }
    }
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Update the Token velocity auto-regressively, shifting increasing weight towards more recent movement
   * Employ a magic constant chosen to minimize (effectively zero) the likelihood of trigonometric edge cases
   * @param {Ray} ray     The proposed movement ray
   * @return {Object}     An updated velocity with directional memory
   * @private
   */
  _updateVelocity(ray) {
    const v = this._velocity;
    const m = 0.89734721;
    return {
      dx: ray.dx,
      sx: ray.dx ? (m * Math.sign(ray.dx)) : (0.5 * m * Math.sign(v.sx)),
      dy: ray.dy,
      sy: ray.dy ? (m * Math.sign(ray.dy)) : (0.5 * m * Math.sign(v.sy))
    }
  }

  /* -------------------------------------------- */

  /**
   * Set this Token as an active target for the current game User
   * @param {boolean} targeted                        Is the Token now targeted?
   * @param {object} [context={}]                     Additional context options
   * @param {User|null} [context.user=null]           Assign the token as a target for a specific User
   * @param {boolean} [context.releaseOthers=true]    Release other active targets for the same player?
   * @param {boolean} [context.groupSelection=false]  Is this target being set as part of a group selection workflow?
   */
  setTarget(targeted=true, {user=null, releaseOthers=true, groupSelection=false}={}) {
    user = user || game.user;

    // Release other targets
    if ( user.targets.size && releaseOthers ) {
      user.targets.forEach(t => {
        if ( t !== this ) t.setTarget(false, {releaseOthers: false});
      });
      user.targets.clear();
    }

    // Acquire target
    if ( targeted ) {
      this.targeted.add(user);
      user.targets.add(this);
    }

    // Release target
    else {
      this.targeted.delete(user);
      user.targets.delete(this);
    }

    // Refresh Token display
    this.refresh();

    // Refresh the Token HUD
    if ( this.hasActiveHUD ) this.layer.hud.render();

    // Broadcast the target change
    if ( !groupSelection ) user.broadcastActivity({targets: user.targets.ids});
  }

  /* -------------------------------------------- */

  /**
   * Add or remove the currently controlled Tokens from the active combat encounter
   * @param {Combat} [combat]    A specific combat encounter to which this Token should be added
   * @return {Promise<Token>} The Token which initiated the toggle
   */
  async toggleCombat(combat) {
    await this.layer.toggleCombat(!this.inCombat, combat, {token: this});
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Toggle an active effect by its texture path.
   * Copy the existing Array in order to ensure the update method detects the data as changed.
   *
   * @param {string|object} effect  The texture file-path of the effect icon to toggle on the Token.
   * @param {object} [options]      Additional optional arguments which configure how the effect is handled.
   * @param {boolean} [options.active]    Force a certain active state for the effect
   * @param {boolean} [options.overlay]   Whether to set the effect as the overlay effect?
   * @return {Promise<boolean>}   Was the texture applied (true) or removed (false)
   */
  async toggleEffect(effect, {active, overlay=false}={}) {
    const fx = this.data.effects;
    const texture = effect.icon ?? effect;

    // Case 1 - handle an active effect object
    if ( effect.icon ) await this.document.toggleActiveEffect(effect, {active, overlay});

    // Case 2 - overlay effect
    else if ( overlay ) await this._toggleOverlayEffect(texture, {active});

    // Case 3 - add or remove a standard effect icon
    else {
      const idx = fx.findIndex(e => e === texture);
      if ((idx !== -1) && (active !== true)) fx.splice(idx, 1);
      else if ((idx === -1) && (active !== false)) fx.push(texture);
      await this.document.update({effects: fx}, {diff: false});
    }

    // Update the Token HUD
    if ( this.hasActiveHUD ) canvas.tokens.hud.refreshStatusIcons();
    return active;
  }

  /* -------------------------------------------- */

  /**
   * A helper function to toggle the overlay status icon on the Token
   * @return {Promise<*>}
   * @private
   */
  async _toggleOverlayEffect(texture, {active}) {

    // Assign the overlay effect
    active = active ?? this.data.overlayEffect !== texture;
    let effect = active ? texture : null;
    await this.document.update({overlayEffect: effect});

    // Set the defeated status in the combat tracker
    // TODO - deprecate this and require that active effects be used instead
    if ( (texture === CONFIG.controlIcons.defeated) && game.combat ) {
      const combatant = game.combat.getCombatantByToken(this.id);
      if ( combatant ) await combatant.update({defeated: active});
    }
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Toggle the visibility state of any Tokens in the currently selected set
   * @return {Promise<TokenDocument[]>}     A Promise which resolves to the updated Token documents
   */
  async toggleVisibility() {
    let isHidden = this.data.hidden;
    const tokens = this._controlled ? canvas.tokens.controlled : [this];
    const updates = tokens.map(t => { return {_id: t.id, hidden: !isHidden}});
    return canvas.scene.updateEmbeddedDocuments("Token", updates);
  }

  /* -------------------------------------------- */

  /**
   * Return the token's sight origin, tailored for the direction of their movement velocity to break ties with walls
   * @return {Point}
   */
  getSightOrigin() {
    let p = this.center;
    return {
      x: p.x - this._velocity.sx,
      y: p.y - this._velocity.sy
    };
  }

  /* -------------------------------------------- */

  /**
   * A generic transformation to turn a certain number of grid units into a radius in canvas pixels.
   * This function adds additional padding to the light radius equal to half the token width.
   * This causes light to be measured from the outer token edge, rather than from the center-point.
   * @param {number} units  The radius in grid units
   * @return {number}       The radius in canvas units
   */
  getLightRadius(units) {
    if (units === 0) return 0;
    const u = Math.abs(units);
    const hw = (this.w / 2);
    return (((u / canvas.dimensions.distance) * canvas.dimensions.size) + hw) * Math.sign(units);
  }

  /* -------------------------------------------- */

  /** @override */
  _getShiftedPosition(dx, dy) {
    let [x, y] = canvas.grid.grid.shiftPosition(this.x, this.y, dx, dy);
    let targetCenter = this.getCenter(x, y);
    let collide = this.checkCollision(targetCenter);
    return collide ? {x: this.data.x, y: this.data.y} : {x, y};
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  rotate(...args) {
    if ( this._movement ) return;
    return super.rotate(...args);
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  _onCreate(options, userId) {

    // Initialize Tokens on the Sight Layer if the Token could be a vision source or emits light
    const refreshVision = this.data.vision && this.observer;
    const refreshLighting = this.emitsLight;
    if ( refreshVision || refreshLighting ) {
      this.updateSource({defer: true});
      canvas.perception.schedule({
        sight: {refresh: refreshVision},
        lighting: {refresh: refreshLighting}
      });
    }

    // Draw the object and display the new Token
    this.draw().then(() => {
      if ( !game.user.isGM && this.isOwner ) this.control({pan: true});
      this.visible = this.isVisible;
    });
  }

  /* -------------------------------------------- */

  /** @override */
  _onUpdate(data, options, userId) {
    const keys = Object.keys(data);
    const changed = new Set(keys);
    const actorData = data.actorData || {};
    const fullRedraw = ["img", "width", "height", "tint", "actorId", "actorLink"].some(r => changed.has(r));
    const positionChange = ["x", "y"].some(c => changed.has(c));

    // Full Token re-draw
    if ( fullRedraw ) {
      const visible = this.visible;
      this.draw().then(() => this.visible = visible);
    }

    // Partial token refresh
    else {
      if ( positionChange ) this.setPosition(this.data.x, this.data.y, options);
      if ( ["effects", "overlayEffect"].some(k => changed.has(k)) || ("effects" in actorData) ) this.drawEffects();
      if ( changed.has("name") ) this.hud.nameplate.text = data.name;
      if ( changed.has("elevation") ) this.hud.tooltip.text = this._getTooltipText();
      const refreshBars = ["actorData", "displayBars"].some(k => k in data) || keys.some(k => k.startsWith("bar"));
      if ( refreshBars ) this.drawBars();
      this.refresh();
    }

    // Handle changes to the visibility state of the token
    const visibilityChange = changed.has("hidden");
    if ( visibilityChange ) {
      if ( !game.user.isGM ) {
        if ( this._controlled && data.hidden ) this.release();
        else if ( !data.hidden && !canvas.tokens.controlled.length ) this.control({pan: true});
      }
      this.visible = this.isVisible;
    }

    // Determine whether the token's perspective has changed
    const rotationChange = changed.has("rotation") && this.hasLimitedVisionAngle;
    const perspectiveChange = visibilityChange || positionChange || rotationChange;
    const visionChange = ["vision", "dimSight", "brightSight"].some(k => changed.has(k)) || (this.data.vision && perspectiveChange);
    const lightChange = changed.has("light") ||  (this.emitsLight && perspectiveChange)
    if ( visionChange || lightChange ) {
      const animating = positionChange && (options.animate !== false);
      if ( !animating ) {  // Immediately update perception if not animating
        this.updateSource({defer: true});
        canvas.perception.schedule({
          lighting: {refresh: true},
          sight: {initialize: changed.has("vision"), refresh: true, forceUpdateFog: this.hasLimitedVisionAngle},
          sounds: {refresh: true},
          foreground: {refresh: true}
        });
      }
    }

    // Process Combat Tracker changes
    if ( this.inCombat ) {
      if ( changed.has("name") ) {
        canvas.addPendingOperation(`Combat.setupTurns`, game.combat.setupTurns, game.combat);
      }
      if ( ["effects", "name", "overlayEffect"].some(k => changed.has(k)) ) {
        canvas.addPendingOperation(`CombatTracker.render`, ui.combat.render, ui.combat);
      }
    }
  }

  /* -------------------------------------------- */

  /** @override */
  _onDelete(options, userId) {

    // Cancel movement animations
    this.stopAnimation();

    // Remove target (if applicable)
    game.user.targets.delete(this);

    // Process changes to perception
    const refreshVision = this.data.vision && this.observer;
    const refreshLighting = this.emitsLight;
    if ( refreshVision || refreshLighting ) {
      this.updateSource({deleted: true, defer: true});
      canvas.perception.schedule({
        sight: {refresh: refreshVision},
        lighting: {refresh: refreshLighting}
      });
    }

    // Remove Combatants
    if (userId === game.user.id) {
      game.combats._onDeleteToken(this.scene.id, this.id);
    }

    // Parent class deletion handlers
    return super._onDelete(options, userId);
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /** @override */
  _canControl(user, event) {
    if ( canvas.controls.ruler.active ) return false;
    const tool = game.activeTool;
    if ( tool === "target" ) return true;
    return game.user.isGM || (this.actor?.testUserPermission(user, "OWNER") ?? false);
  }

  /* -------------------------------------------- */

  /** @override */
  _canHUD(user, event) {
    if ( canvas.controls.ruler.active ) return false;
    return user.isGM || (this.actor?.testUserPermission(user, "OWNER") ?? false);
  }

  /* -------------------------------------------- */

  /** @override */
  _canConfigure(user, event) {
    return true;
  }

  /* -------------------------------------------- */

  /** @override */
  _canHover(user, event) {
    return true;
  }

  /* -------------------------------------------- */

  /** @override */
  _canView(user, event) {
    if ( !this.actor ) ui.notifications.warn("TOKEN.WarningNoActor", {localize: true});
    return this.actor?.testUserPermission(user, "LIMITED");
  }

  /* -------------------------------------------- */

  /** @override */
  _canDrag(user, event) {
    if ( !this._controlled ) return false;
    const tool = game.activeTool;
    if ( ( tool !== "select" ) || game.keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.CONTROL) ) return false;
    const blockMove = game.paused && !game.user.isGM;
    return !this._movement && !blockMove;
  }

  /* -------------------------------------------- */

  /** @override */
  _onHoverIn(event, options) {
    const combatant = this.combatant;
    if ( combatant ) ui.combat.hoverCombatant(combatant, true);
    return super._onHoverIn(event, options);
  }

  /* -------------------------------------------- */

  /** @override */
  _onHoverOut(event) {
    const combatant = this.combatant;
    if ( combatant ) ui.combat.hoverCombatant(combatant, false);
    return super._onHoverOut(event);
  }

  /* -------------------------------------------- */

  /** @override */
  _onClickLeft(event) {
    const tool = game.activeTool;
    const oe = event.data.originalEvent;
    let isRuler = (tool === "ruler") || ( oe.ctrlKey || oe.metaKey );
    if ( isRuler ) canvas.mouseInteractionManager._handleClickLeft(event);
    if ( tool === "target" ) return this.setTarget(!this.isTargeted, {releaseOthers: !oe.shiftKey});
    super._onClickLeft(event);
  }

  /* -------------------------------------------- */

  /** @override */
  _onClickLeft2(event) {
    const sheet = this.actor.sheet;
    if ( sheet.rendered ) {
      sheet.maximize();
      sheet.bringToTop();
    }
    else sheet.render(true, {token: this.document});
  }

  /* -------------------------------------------- */

  /** @override */
  _onClickRight2(event) {
    if ( this.isOwner ) {
      if ( game.user.can("TOKEN_CONFIGURE") ) return super._onClickRight2(event);
    }
    else return this.setTarget(!this.targeted.has(game.user), {releaseOthers: !event.data.originalEvent.shiftKey});
  }

  /* -------------------------------------------- */

  /** @override */
  _onDragLeftDrop(event) {
    const clones = event.data.clones || [];
    const {originalEvent, destination} = event.data;

    // Ensure the cursor destination is within bounds
    if ( !canvas.dimensions.rect.contains(destination.x, destination.y) ) return false;

    // Compute the final dropped positions
    const updates = clones.reduce((updates, c) => {

      // Get the snapped top-left coordinate
      let dest = {x: c.data.x, y: c.data.y};
      if ( !originalEvent.shiftKey && (canvas.grid.type !== CONST.GRID_TYPES.GRIDLESS) ) {
        const isTiny = (c.data.width < 1) && (c.data.height < 1);
        dest = canvas.grid.getSnappedPosition(dest.x, dest.y, isTiny ? 2 : 1);
      }

      // Test collision for each moved token vs the central point of it's destination space
      const target = c.getCenter(dest.x, dest.y);
      if ( !game.user.isGM ) {
        c._velocity = c._original._velocity;
        let collides = c.checkCollision(target);
        if ( collides ) {
          ui.notifications.error("ERROR.TokenCollide", {localize: true});
          return updates
        }
      }

      // Otherwise ensure the final token center is in-bounds
      else if ( !canvas.dimensions.rect.contains(target.x, target.y) ) return updates;

      // Perform updates where no collision occurs
      updates.push({_id: c._original.id, x: dest.x, y: dest.y});
      return updates;
    }, []);

    // Submit the data update
    return canvas.scene.updateEmbeddedDocuments("Token", updates);
  }

  /* -------------------------------------------- */

  /** @override */
  _onDragLeftMove(event) {
    const {clones, destination, origin, originalEvent} = event.data;
    const preview = game.settings.get("core", "tokenDragPreview");

    // Pan the canvas if the drag event approaches the edge
    canvas._onDragCanvasPan(originalEvent);

    // Determine dragged distance
    const dx = destination.x - origin.x;
    const dy = destination.y - origin.y;

    // Update the position of each clone
    for ( let c of clones || [] ) {
      const o = c._original;
      const x = o.data.x + dx;
      const y = o.data.y + dy;
      if ( preview && !game.user.isGM ) {
        const collision = o.checkCollision(o.getCenter(x, y));
        if ( collision ) continue;
      }
      c.data.x = x;
      c.data.y = y;
      c.refresh();
      if ( preview ) c.updateSource({defer: true});
    }

    // Batch update perception for all dragged clones
    if ( preview ) canvas.perception.schedule({
      lighting: {refresh: true},
      sight: {refresh: true, skipUpdateFog: true}
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _onDragLeftCancel(event) {
    const preview = game.settings.get("core", "tokenDragPreview");
    if ( preview ) {
      for ( let token of this.layer.controlled ) {
        token.updateSource({defer: true});
      }
      canvas.perception.schedule({
        lighting: {refresh: true},
        sight: {refresh: true, skipUpdateFog: true}
      });
    }
    return super._onDragLeftCancel(event);
  }
}

/**
 * A "secret" global to help debug attributes of the currently controlled Token.
 * This is only for debugging, and may be removed in the future, so it's not safe to use.
 * @type {Token}
 * @ignore
 */
let _token = null;
