/**
 * @typedef {Object}                      VisionSourceData
 * @property {number} x                   The x-coordinate of the source location
 * @property {number} y                   The y-coordinate of the source location
 * @property {number} z                   An optional z-index sorting for the source
 * @property {number} rotation            The angle of rotation for this point source
 * @property {number} angle               The angle of emission for this point source
 * @property {number} bright              The allowed radius of bright vision or illumination
 * @property {number} dim                 The allowed radius of dim vision or illumination
 */

/**
 * A specialized subclass of the PointSource abstraction which is used to control the rendering of vision sources.
 * @extends {PointSource}
 * @param {Token} object                  The Token object that generates this vision source
 */
class VisionSource extends PointSource {
  constructor(object) {
    super(object);

    /**
     * The current vision mesh for this source
     * @type {PIXI.Mesh}
     */
    this.illumination = this._createMesh(AdaptiveIlluminationShader);
  }

  /** @inheritdoc */
  static sourceType = "vision";

  /**
   * Keys in the VisionSourceData structure which, when modified, change the appearance of the source
   * @type {string[]}
   * @private
   */
  static _appearanceKeys = ["dim", "bright"];

  /* -------------------------------------------- */
  /*  Vision Source Attributes                    */
  /* -------------------------------------------- */

  /**
   * The object of data which configures how the source is rendered
   * @type {VisionSourceData}
   */
  data = {};

  /**
   * The ratio of dim:bright as part of the source radius
   * @type {number}
   */
  ratio;

  /**
   * The rendered field-of-vision texture for the source for use within shaders.
   * @type {PIXI.RenderTexture}
   */
  fovTexture;

  /**
   * Track which uniforms need to be reset
   * @type {{illumination: boolean}}
   * @private
   */
  _resetUniforms = {
    illumination: true
  }

  /**
 * To track if a source is temporarily shutdown to avoid glitches
 * @type {{illumination: boolean}}
 * @private
 */
  _shutdown = {
    illumination: false
  }

  /* -------------------------------------------- */
  /*  Vision Source Initialization                */
  /* -------------------------------------------- */

  /**
   * Initialize the source with provided object data.
   * @param {object} data             Initial data provided to the point source
   * @return {VisionSource}           A reference to the initialized source
   */
  initialize(data={}) {

    // Initialize new input data
    const changes = this._initializeData(data);

    // Compute derived data attributes
    this.radius = Math.max(Math.abs(this.data.dim), Math.abs(this.data.bright));
    this.ratio = Math.clamped(Math.abs(this.data.bright) / this.radius, 0, 1);
    this.limited = this.data.angle !== 360;

    // Compute the source polygon
    const origin = {x: this.data.x, y: this.data.y};
    this.los = CONFIG.Canvas.losBackend.create(origin, {
      type: "sight",
      angle: this.data.angle,
      rotation: this.data.rotation,
      source: this
    });

    // Store the FOV circle
    this.fov = new PIXI.Circle(origin.x, origin.y, this.radius);

    // Record status flags
    this._flags.useFov = canvas.performance.textures.enabled;
    this._flags.renderFOV = true;
    if ( this.constructor._appearanceKeys.some(k => k in changes) ) {
      for ( let k of Object.keys(this._resetUniforms) ) {
        this._resetUniforms[k] = true;
      }
    }

    // Set the correct blend mode
    this._initializeBlending();
    return this;
  }

  /* -------------------------------------------- */

  /**
   * Initialize the blend mode and vertical sorting of this source relative to others in the container.
   * @private
   */
  _initializeBlending() {
    const defaultZ = this.isDarkness ? 10 : 0;
    let blend = PIXI.BLEND_MODES[this.isDarkness ? "MIN_COLOR" : "MAX_COLOR"];
    if (this._resetUniforms.illumination && (this.illumination.blendMode !== blend)) {
      this._shutdown.illumination = !(this.illumination.renderable = false);
    }
    this.illumination.blendMode = blend;
    this.illumination.zIndex = this.data.z ?? defaultZ;
  }

  /* -------------------------------------------- */

  /**
   * Process new input data provided to the LightSource.
   * @param {object} data             Initial data provided to the vision source
   * @returns {object}                The changes compared to the prior data
   * @private
   */
  _initializeData(data) {

    // Clean input data
    data.angle = data.angle ?? 360;
    data.bright = data.bright ?? 0;
    data.dim = data.dim ?? 0;
    data.rotation = data.rotation ?? 0;
    data.x = data.x ?? 0;
    data.y = data.y ?? 0;

    // Identify changes compared to the current object
    const changes = foundry.utils.flattenObject(foundry.utils.diffObject(this.data, data));
    this.data = data;
    return changes;
  }

  /* -------------------------------------------- */
  /*  Vision Source Rendering                     */
  /* -------------------------------------------- */

  /**
   * Draw the display of this source to remove darkness from the LightingLayer illumination container.
   * @see {LightSource#drawLight}
   * @return {PIXI.Container|null}         The rendered light container
   */
  drawVision() {
    if ( this._flags.renderFOV ) {
      this.losMask.clear().beginFill(0xFFFFFF).drawShape(this.los).endFill();
      if ( this._flags.useFov ) this._renderTexture();
    }
    return LightSource.prototype.drawLight.call(this);
  }

  /* -------------------------------------------- */

  /**
   * Draw a Container used for exploring the FOV area of Token sight in the SightLayer
   * @returns {PIXI.Container}
   */
  drawSight() {
    const c = new PIXI.Container();
    const fov = c.addChild(new PIXI.LegacyGraphics());
    fov.beginFill(0xFFFFFF).drawCircle(this.x, this.y, this.radius).endFill();
    const los = c.addChild(new PIXI.LegacyGraphics());
    los.beginFill(0xFFFFFF).drawShape(this.los).endFill();
    c.mask = los;
    return c;
  }

  /* -------------------------------------------- */

  /**
   * Update shader uniforms by providing data from this PointSource
   * @param {AdaptiveIlluminationShader} shader        The shader being updated
   * @private
   */
  _updateIlluminationUniforms(shader) {
    const u = shader.uniforms;
    const c = canvas.lighting.channels;
    const s = canvas.app.renderer.screen;

    // Determine light colors
    const ll = CONFIG.Canvas.lightLevels;
    const penalty = shader.getDarknessPenalty(c.darkness.level, 0.5);
    u.colorBright = [1,1,1].map(x => ll.bright * x * (1-penalty));
    u.colorDim = u.colorBright.map((x, i) => (ll.dim * x) + ((1 - ll.dim) * c.background.rgb[i]));

    // Apply standard uniforms for this PointSource
    u.ratio = this.ratio;
    u.screenDimensions = canvas.screenDimensions;
    u.uBkgSampler = canvas.primary.renderTexture;
    u.fovTexture = this._flags.useFov ? this.fovTexture : null;
    u.useFov = this._flags.useFov;
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  _drawRenderTextureContainer() {
    const c = new PIXI.Container();
    const fov = c.addChild(new PIXI.LegacyGraphics());
    fov.beginFill(0x0000FF).drawCircle(this.x, this.y, this.radius).endFill();
    const los = c.addChild(new PIXI.LegacyGraphics());
    los.beginFill(0xFFFFFF).drawShape(this.los).endFill();
    c.mask = los;
    return c;
  }
}
