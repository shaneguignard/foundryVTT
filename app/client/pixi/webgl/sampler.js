/**
 * A simple shader to emulate a PIXI.Sprite with a PIXI.Mesh (but faster!)
 * @extends {AbstractBaseShader}
 */
class BaseSamplerShader extends AbstractBaseShader {

  /**
   * @inheritdoc
   * @author SecretFire
   */
  static vertexShader = `
    attribute vec2 aVertexPosition;
    attribute vec2 aUvs;
    uniform mat3 translationMatrix;
    uniform mat3 projectionMatrix;
    uniform vec2 screenDimensions;
    varying vec2 vUvs;
    varying vec2 vSamplerUvs;
  
    void main() {
      vec3 tPos = translationMatrix * vec3(aVertexPosition, 1.0);
      vUvs = aUvs;
      vSamplerUvs = tPos.xy / screenDimensions;
      gl_Position = vec4((projectionMatrix * tPos).xy, 0.0, 1.0);
    }`;

  /**
   * @inheritdoc
   * @author SecretFire
   */
  static fragmentShader = `
    uniform sampler2D sampler;
    varying vec2 vUvs;
    varying vec2 vSamplerUvs;
  
    void main() {
      gl_FragColor = texture2D(sampler, vSamplerUvs);
    }`;

  /** @inheritdoc */
  static defaultUniforms = {
    screenDimensions: [1, 1],
    sampler: 0
  };
}
