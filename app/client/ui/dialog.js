/**
 * @typedef {ApplicationOptions} DialogOptions
 * @property {boolean} [jQuery=true]  Whether to provide jQuery objects to callback functions (if true) or plain
 *                                    HTMLElement instances (if false). This is currently true by default but in the
 *                                    future will become false by default.
 */

/**
 * @typedef {Object} DialogButton
 * @property {string} icon            A Font Awesome icon for the button
 * @property {string} label           The label for the button
 * @property {Function} [callback]    A callback function that fires when the button is clicked
 */

/**
 * Create a dialog window displaying a title, a message, and a set of buttons which trigger callback functions.
 * @implements {Application}
 *
 * @param {Object} data               An object of dialog data which configures how the modal window is rendered
 * @param {string} data.title         The window title displayed in the dialog header
 * @param {string} data.content       HTML content for the dialog form
 * @param {Object<string, DialogButton>} data.buttons The buttons which are displayed as action choices for the dialog
 * @param {string} [data.default]     The name of the default button which should be triggered on Enter keypress
 * @param {Function} [data.render]    A callback function invoked when the dialog is rendered
 * @param {Function} [data.close]     Common callback operations to perform when the dialog is closed
 *
 * @param {DialogOptions} [options]   Dialog rendering options, see {@link Application}.
 *
 * @example <caption>Constructing a custom dialog instance</caption>
 * let d = new Dialog({
 *  title: "Test Dialog",
 *  content: "<p>You must choose either Option 1, or Option 2</p>",
 *  buttons: {
 *   one: {
 *    icon: '<i class="fas fa-check"></i>',
 *    label: "Option One",
 *    callback: () => console.log("Chose One")
 *   },
 *   two: {
 *    icon: '<i class="fas fa-times"></i>',
 *    label: "Option Two",
 *    callback: () => console.log("Chose Two")
 *   }
 *  },
 *  default: "two",
 *  render: html => console.log("Register interactivity in the rendered dialog"),
 *  close: html => console.log("This always is logged no matter which option is chosen")
 * });
 * d.render(true);
 */
class Dialog extends Application {
  constructor(data, options) {
    super(options);
    this.data = data;
  }

	/* -------------------------------------------- */

  /**
   * @override
   * @returns {DialogOptions}
   */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
	    template: "templates/hud/dialog.html",
      classes: ["dialog"],
      width: 400,
      jQuery: true
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  get title() {
    return this.data.title || "Dialog";
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  getData(options) {
    let buttons = Object.keys(this.data.buttons).reduce((obj, key) => {
      let b = this.data.buttons[key];
      b.cssClass = [key, this.data.default === key ? "default" : ""].filterJoin(" ");
      if ( b.condition !== false ) obj[key] = b;
      return obj;
    }, {});
    return {
      content: this.data.content,
      buttons: buttons
    }
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  activateListeners(html) {
    html.find(".dialog-button").click(this._onClickButton.bind(this));
    // Prevent the default form submission action if any forms are present in this dialog.
    html.find("form").each((i, el) => el.onsubmit = evt => evt.preventDefault());
    $(document).on('keydown.chooseDefault', this._onKeyDown.bind(this));
    if ( this.data.render instanceof Function ) this.data.render(this.options.jQuery ? html : html[0]);
  }

  /* -------------------------------------------- */

  /**
   * Handle a left-mouse click on one of the dialog choice buttons
   * @param {MouseEvent} event    The left-mouse click event
   * @private
   */
  _onClickButton(event) {
    const id = event.currentTarget.dataset.button;
    const button = this.data.buttons[id];
    this.submit(button);
  }

  /* -------------------------------------------- */

  /**
   * Handle a keydown event while the dialog is active
   * @param {KeyboardEvent} event   The keydown event
   * @private
   */
  _onKeyDown(event) {

    // Close dialog
    if ( event.key === "Escape" ) {
      event.preventDefault();
      event.stopPropagation();
      return this.close();
    }

    // Confirm default choice
    if ( (event.key === "Enter") && this.data.default ) {
      event.preventDefault();
      event.stopPropagation();
      const defaultChoice = this.data.buttons[this.data.default];
      return this.submit(defaultChoice);
    }
  }

  /* -------------------------------------------- */

  /**
   * Submit the Dialog by selecting one of its buttons
   * @param {Object} button     The configuration of the chosen button
   * @private
   */
  submit(button) {
    try {
      if (button.callback) button.callback(this.options.jQuery ? this.element : this.element[0]);
      this.close();
    } catch(err) {
      ui.notifications.error(err);
      throw new Error(err);
    }
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  async close(options) {
    if ( this.data.close ) this.data.close(this.options.jQuery ? this.element : this.element[0]);
    $(document).off('keydown.chooseDefault');
    return super.close(options);
  }

  /* -------------------------------------------- */
  /*  Factory Methods                             */
  /* -------------------------------------------- */

  /**
   * A helper factory method to create simple confirmation dialog windows which consist of simple yes/no prompts.
   * If you require more flexibility, a custom Dialog instance is preferred.
   *
   * @param {object} config             Confirmation dialog configuration
   * @param {string} config.title          The confirmation window title
   * @param {string} config.content        The confirmation message
   * @param {Function} [config.yes]        Callback function upon yes
   * @param {Function} [config.no]         Callback function upon no
   * @param {Function} [config.render]     A function to call when the dialog is rendered
   * @param {boolean} [config.defaultYes=true]  Make "yes" the default choice?
   * @param {boolean} [config.rejectClose=false] Reject the Promise if the Dialog is closed without making a choice.
   * @param {Object} [config.options={}]   Additional rendering options passed to the Dialog
   *
   * @return {Promise<*>}               A promise which resolves once the user makes a choice or closes the window
   *
   * @example
   * let d = Dialog.confirm({
   *  title: "A Yes or No Question",
   *  content: "<p>Choose wisely.</p>",
   *  yes: () => console.log("You chose ... wisely"),
   *  no: () => console.log("You chose ... poorly"),
   *  defaultYes: false
   * });
   */
  static async confirm({title, content, yes, no, render, defaultYes=true, rejectClose=false, options={}}={}) {
    return new Promise((resolve, reject) => {
      const dialog = new this({
        title: title,
        content: content,
        buttons: {
          yes: {
            icon: '<i class="fas fa-check"></i>',
            label: game.i18n.localize("Yes"),
            callback: html => {
              const result = yes ? yes(html) : true;
              resolve(result);
            }
          },
          no: {
            icon: '<i class="fas fa-times"></i>',
            label: game.i18n.localize("No"),
            callback: html => {
              const result = no ? no(html) : false;
              resolve(result);
            }
          }
        },
        default: defaultYes ? "yes" : "no",
        render: render,
        close: () => {
          if ( rejectClose ) reject("The confirmation Dialog was closed without a choice being made");
          else resolve(null);
        },
      }, options);
      dialog.render(true);
    });
  }

  /* -------------------------------------------- */

  /**
   * A helper factory method to display a basic "prompt" style Dialog with a single button
   * @param {object} config        Dialog configuration options
   * @param {string} [config.title]          The confirmation window title
   * @param {string} [config.content]        The confirmation message
   * @param {string} [config.label]          The confirmation button text
   * @param {Function} [config.callback]     A callback function to fire when the button is clicked
   * @param {Function} [config.render]       A function that fires after the dialog is rendered
   * @param {boolean} [config.rejectClose=true] Reject the promise if the dialog is closed without confirming the
   *                                         choice, otherwise resolve as null
   * @param {object} [config.options]        Additional rendering options
   * @return {Promise<*>}           The returned value from the provided callback function, if any
   */
  static async prompt({title, content, label, callback, render, rejectClose=true, options={}}={}) {
    return new Promise((resolve, reject) => {
      const dialog = new this({
        title: title,
        content: content,
        buttons: {
          ok: {
            icon: '<i class="fas fa-check"></i>',
            label: label,
            callback: html => {
              const result = callback(html);
              resolve(result);
            }
          },
        },
        default: "ok",
        render: render,
        close: () => {
          if ( rejectClose ) {
            reject(new Error("The Dialog prompt was closed without being accepted."));
          }
          else resolve(null);
        },
      }, options);
      dialog.render(true);
    });
  }
}
